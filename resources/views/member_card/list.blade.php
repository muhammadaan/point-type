@extends('layouts.app')

@section('_title')
<title>Member_Card | List</title>
@endsection

@push('styles')
	<link type="text/css" rel="stylesheet" href="{{ asset('template/assets/plugins/jquery-datatable/media/css/jquery.dataTables.css') }}">
	<link type="text/css" rel="stylesheet" href="{{ asset('template/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css') }}">
	<link media="screen" type="text/css" rel="stylesheet" href="{{ asset('template/assets/plugins/datatables-responsive/css/datatables.responsive.css') }}">
	<style>
		.form-group-attached {
			padding-left: 15px
		}
	</style>
@endpush

@section('content')
	<div class="col-md-12">
		<div data-pages="card" class="card card-default">
			<div class="card-header">
				<div class="card-title">
					<h4>Member Card List</h4>
				</div>
				<div class="card-controls">
					<ul>
						<li>
							<a data-toggle="collapse" class="card-collapse" href="#"><i class="card-icon card-icon-collapse"></i></a>
						</li>	
					</ul>
				</div>
				@if (session('message'))
				    <div class="alert alert-success" role="alert">
				    	<button class="close" data-dismiss="alert"></button>
				        {{ session('message') }}
				    </div>
				@endif	
			</div>
			<div class="card-block">
				<button onclick="goBack()" class="btn btn-complete m-b-15">Back</button>
				<button onclick="relocateAddMemberCard()" class="btn btn-primary m-b-15">Add Member Card</button>
				<table id="membercardTable" class="table table-hover" cellspacing="0" width="100%">
				    <thead>
				        <tr>
				            <th>Member Card Name</th>
				            <th>Member Card Image</th>
				            <th>Action</th>
				        </tr>
				    </thead>
				    <tfoot style="display: table-header-group;">
				        <tr>
				            <th>Member Card Name</th>
				            <th>Member Card Image</th>
				            <th>Action</th>
				        </tr>
				    </tfoot>
				    <tbody>
				    	@foreach($member_card as $show)
				        <tr>
				            <td>{{ $show->card_name }}</td>
				            <td><img src="{{ asset('assets/images/') }}/{{$show->background_image}}" style="max-width:100px;max-height:100px;float:left;"></td>
				            <td>
				            	<a onclick="showModal({{ $show->id }})" href="#" data-toggle="modal" title="Show Detail"><i class="fa fa-search"></i></a>
				            	<a href="{{url('/membercard/edit')}}/{{$show->id}}" title="Edit"><i class="fa fa-edit"></i></a>
			                    <a href="{{url('/membercard/delete')}}/{{$show->id}}" onclick="return ConfirmDelete()" title="Delete"><i class="pg-trash"></i></a>
			                </td>
				        </tr>
				        @endforeach
				    </tbody>
				</table>		
			</div>
		</div>
	</div>
	@include('member_card.modals.detail')
@endsection

@push('scripts')
	<script src="{{ asset('template/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('template/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('template/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js') }}"></script>
	<script src="{{ asset('template/assets/plugins/datatables-responsive/js/datatables.responsive.js') }}" type="text/javascript"></script>
	<script src="{{ asset('template/assets/plugins/datatables-responsive/js/lodash.min.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		$('#form-generate-card').submit(function(e)
		{
			e.preventDefault()
			var member = $('#member-selection option:selected').val()
			var card = $('#member-card-id').val()

			$http('{{ route("membercard.generate") }}').post({'member_id': member, 'card_id': card, _token: '{{ csrf_token() }}'}).then(function(resp) {
				resp = JSON.parse(resp)
				$('#member-card-container').empty()
				$('#member-card-container').append('<img width="448px" height="250px" src="'+resp.image_url+'">')
			}).catch(function(err) {
				console.log(err)
			})

		})
		function showModal(id)
		{
			$('#membercard-detail').modal()
			$('#form-generate-card #card-id').empty()
			$('#member-card-container').empty()
			$('#form-generate-card #card-id').append('<input id="member-card-id" type="hidden" value="'+id+'">')
		}
		function ConfirmDelete()
		{
		  var x = confirm("Are you sure you want to delete ?");
		  
		  if(x)
		  	return true;
		  else
		    return false;
			
		}   
	</script>
	<script>
		$(document).ready(function() {
		    // Setup - add a text input to each footer cell
		    $('#membercardTable tfoot th').each( function () {
		        var title = $(this).text();
		        $(this).html( '<input type="text" />' );
		    } );

		    var table = $('#membercardTable').DataTable();

		    // Apply the search
		    table.columns().every( function () {
		        var that = this;
		 
		        $( 'input', this.footer() ).on( 'keyup change', function () {
		            if ( that.search() !== this.value ) {
		                that
		                    .search( this.value )
		                    .draw();
		            }
		        } );
		    } );
		} );

		function relocateAddMemberCard() {
			window.location.href = "{{ route('membercard.create') }}"
		}

	</script>
@endpush