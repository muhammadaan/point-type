@extends('layouts.app')

@section('_title')
<title>Member Card Card | Edit</title>
@endsection

@section('content')
	<div class="col-md-12">
		<div data-pages="card" class="card card-default">
			<div class="card-header">
				<div class="card-title">
					<h4>Edit Member Card</h4>
				</div>
				<div class="card-controls">
					<ul>
						<li>
							<a data-toggle="collapse" class="card-collapse" href="#"><i class="card-icon card-icon-collapse"></i></a>
						</li>	
					</ul>
				</div>
			</div>
			<div class="card-block">
				<button onclick="goBack()" class="btn btn-complete m-b-15">Back</button>
				<form id="form1" action="{{url('/membercard/update')}}/{{$membercard->id}}" method="POST" enctype="multipart/form-data" role="form" autocomplete="off">
				{{csrf_field()}}
				<input type="hidden" name="id" value="{{$membercard->id}}">		
					<div class="form-group form-group-default">
						<label>Card Name</label>
						<input type="text" name="card_name" class="form-control" value="{{$membercard->card_name}}">
					</div>
					<div class="form-group form-group-default">
						<label>Image</label>
													
						<img src="{{ asset('assets/images/') }}/{{$membercard->background_image}}" style="max-width:100px;max-height:100px; border-width:2px; margin-bottom: auto;" id="prev">
						<br>
						<input type="file" name="background_image" id="background_image" style="padding-top: 10px;">
					</div>
					<button class="btn btn-success" type="submit">Submit</button>
				</form>
			</div>
		</div>
	</div>
@endsection