@extends('layouts.app')

@section('_title')
<title>Member Card | Add</title>
@endsection

@section('content')
<?php $acl_btn = App\Models\Usergroup::where('name', \Auth::user()->roles)->where('route_access_list', 'LIKE', '%'.'member.create'.'%')->count(); ?>		 
@if($acl_btn != 0 || \Auth::user()->roles == 'superadmin')

	<div class="col-md-12">
		<div data-pages="card" class="card card-default">
			<div class="card-header">
				<div class="card-title">
					<h4>Add Card</h4>
				</div>
				<div class="card-controls">
					<ul>
						<li>
							<a data-toggle="collapse" class="card-collapse" href="#"><i class="card-icon card-icon-collapse"></i></a>
						</li>	
					</ul>
				</div>
			</div>
			<div class="card-block">
				<button onclick="goBack()" class="btn btn-complete m-b-15">Back</button>
				<form id="form1" action="{{ url('/membercard/insert') }}" method="POST" enctype="multipart/form-data" role="form" autocomplete="off">
				{{csrf_field()}}	
					<div class="form-group form-group-default">
						<label>Card Name</label>
						<input type="text" name="card_name" class="form-control" required="">
					</div>
					<div class="form-group form-group-default">
						<label>Image</label>
						<input type="file" name="background_image" required="">
					</div>
					<!-- <input type="hidden" name="remember" value="{{csrf_token()}}"> -->
					<button class="btn btn-success" type="submit">Submit</button>
				</form>
			</div>
		</div>
		@else
		<div class="card-block"> 
    	 	<h3 align="center">Sorry, You don't have access permission to this page !</h3>
		</div>
		@endif
	</div>
@endsection