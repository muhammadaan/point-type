<div class="modal fade slide-up disable-scroll" id="membercard-detail" tabindex="-1" role="dialog" aria-labelledby="membercard-detail" aria-hidden="false">
    <div class="modal-dialog ">
        <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                  <i class="pg-close fs-14"></i>
                </button>
                <h5 style="padding-left: 8.5px">Detail</h5>
            </div>
            <div class="modal-body">
                <form id="form-generate-card">
                	<div class="form-group-attached">
                		<div class="form-group row">
                            <span id="card-id"></span>
                			<label class="col-md-3 offset-md-1 control-label">Select Member</label>
                			<div class="col-md-7">
                                <select id="member-selection" class="form-control">
                                    @if(count($members) > 1)
                                        @foreach($members as $member)
                                            <option value="{{ $member->id }}">{{ $member->member_name }}</option>
                                        @endforeach
                                    @endif
                                </select>         
                            </div>
                		</div>
                	</div>
                    <button id="generate-card" type="submit" class="btn btn-primary float-right" style="margin-top: 10px; margin-bottom: 10px;">Generate Card</button>
                    <div id="member-card-container"></div>
                </form>
            </div>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>