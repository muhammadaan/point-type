@extends('layouts.app')

@section('_title')
<title>Wallet Level | Add</title>
@endsection

@section('content')

<?php $acl_btn = App\Models\Usergroup::where('name', \Auth::user()->roles)->where('route_access_list', 'LIKE', '%'.'wallet_level.create'.'%')->count(); ?>		 
@if($acl_btn != 0 || \Auth::user()->roles == 'superadmin')

	<div class="col-md-12">
		<div data-pages="card" class="card card-default">
			<div class="card-header">
				<div class="card-title">
					<h4>Add Wallet Level</h4>
				</div>
				<div class="card-controls">
					<ul>
						<li>
							<a data-toggle="collapse" class="card-collapse" href="#"><i class="card-icon card-icon-collapse"></i></a>
						</li>	
					</ul>
				</div>
			</div>
			<div class="card-block">
				<button onclick="goBack()" class="btn btn-complete m-b-15">Back</button>
				<form action="{{ url('/wallet_level/insert') }}" method="POST" enctype="multipart/form-data" id="formpoint" role="form" autocomplete="off">
				{{csrf_field()}}

					<div class="form-group form-group-default col-md-6">
						<label>Point Level Name</label>
						<input type="text" name="level" class="form-control" required="">
					</div>
					<div class="form-group form-group-default col-md-6">
						<label>Point Type</label>
						<select name="type" class="form-control" data-init-plugin="select2" tabindex="-1">
							<option value="">Select Point Type</option>
							@foreach($point as $show)
							<option value="{{$show->id}}">{{$show->point_type}}</option>
							@endforeach	
						</select>
					</div>	
					<div class="form-group form-group-default col-md-6">
						<label>Minimum Point</label>
						<input type="text" name="minimum" class="col-lg-11" required="" id="min">
					</div>
					<div class="form-group form-group-default col-md-6">
						<label>Maximum Point</label>
						<input type="text" name="maximum" class="col-lg-11" required="" id="max">
					</div>
					<div class="form-group form-group-default col-md-6">
						<label>Image</label>
						<input type="file" name="image" class="form-control" required="">
					</div>
					<button class="btn btn-success" type="submit">Submit</button>
				</form>
			</div>
		</div>
		@else
		<div class="card-block"> 
    	 	<h3 align="center">Sorry, You don't have access permission to this page !</h3>
		</div>
		@endif
	</div>	
@endsection

@push('scripts')
	<script src="{{ asset('template/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('template/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('template/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js') }}"></script>
	<script src="{{ asset('template/assets/plugins/datatables-responsive/js/datatables.responsive.js') }}" type="text/javascript"></script>
	<script src="{{ asset('template/assets/plugins/datatables-responsive/js/lodash.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('template/assets/plugins/jquery-ui/jquery-ui_back.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('template/assets/js_validation/jquery.validate.js') }}" type="text/javascript"></script>
	<script>
		$(document).ready(function() {
	    	
			 $( "#start" ).spinner({min: 0, max: 500});
			 $( "#min" ).spinner({min: 0, max: 500});
			 $( "#max" ).spinner({min: 0, max: 500});
			 
 			 $('#formpoint').validate();			
		});
	</script>
@endpush