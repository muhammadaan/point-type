@extends('layouts.app')

@section('_title')
<title>User Group | Edit</title>
@endsection

@section('content')
<?php $acl_btn = App\Models\Usergroup::where('name', \Auth::user()->roles)->where('route_access_list', 'LIKE', '%'.'usergroup.edit'.'%')->count(); ?> 
  @if($acl_btn != 0 || \Auth::user()->roles == 'superadmin') 

<div class="col-md-12">
   <div data-pages="card" class="card card-default">
	 <div class="card-header">
        <form action="{{url('/usergroup/update')}}/{{$data->id}}" method="POST" id="addgroup" enctype="multipart/form-data">
         {{ csrf_field() }}
         {{ method_field('PUT') }}
         <input type="hidden" name="id" value="{{$data->id}}">
        <div class="card-header separator" style="display: flex">
            <h3><strong>Edit User Group</strong><span id="date-title" style="margin-left: 100px"></span> </h3>
 
            <div style="margin-left: 650px;" class="pull-right">                    
              <button class="btn btn-primary pull-right" style="margin-left: 5px;" type="submit">Submit</button>
               <button class="btn btn-danger pull-right" onclick="goBack()" style="margin-left: 5px;">Cancel</button>
            </div>
        
        </div>
        <div class="col-md-12">
          <div class="card-default card-block col-md-12 pull-left">    
            <!-- START card -->
          <div class="card-block">
          <div class="col-md-12">
            <div class="card card-transparent ">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs nav-tabs-fillup" data-init-reponsive-tabs="dropdownfx">
                <li class="nav-item">
                  <a href="#" class="active" data-toggle="tab" data-target="#slide1"><span>Group Information</span></a>
                </li>
                <li class="nav-item">
                  <a href="#" data-toggle="tab" data-target="#slide2"><span>Access Information</span></a>
                </li>
              </ul>
            	 <!-- Tab panes -->
              <div class="tab-content">
                <div class="tab-pane slide-left active" id="slide1">
                  <div class="row column-seperation">
                     <form class="" role="form">
                    <div class="col-lg-6">
                      <div class="form-group form-group-default required ">
                        <label>Group Name</label>
                        <input type="text" name="name" class="form-control" value="{{$data->name}}" required>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group form-group-default required">
                        <label>Group Desc</label>
                        <input type="text" name="description" class="form-control" value="{{$data->description}}" required>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane slide-left" id="slide2">
                  <div class="row">
                    <div class="col-lg-12">
                       <?php 
                          $routeCollection = Route::getRoutes();
                          $new             = [];
                          $groups          = [];
                          foreach ($routeCollection as $value) {

                              if(($value->getName() != '') or (!$value)){

                                  if(strpos($value->getName(), 'front-user') === false){
                                      
                                      if (in_array(explode(".",$value->getName())[0], $groups)) {
                                          
                                      } else {

                                          array_push($groups, explode(".",$value->getName())[0]);
                                      }                            
                                      array_push($new, $value->getName());
                                  }
                              }                            
                          } 
                          asort($groups);
                          foreach ($groups as $key => $group) {
                              # code...
                              echo '<label class="switch"><input type="checkbox" class="skip" onclick="checkclass(\''.$group.'\')" id="'.$group.'"/><span></span><em> '.$group.'</em> </label></br>';
                              foreach ($routeCollection as $row) {
                                  if(explode(".",$row->getName())[0] == $group){

                                      echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class="switch"><input type="checkbox" id="as'.str_replace(".","bb",$row->getName()).'" name="routelist[]" class="'.$group.' skip" value="'.$row->getName().'"/><span></span> '.$row->getName().'</label></br>';
                                  }
                              }
                              echo '</br>';
                          }
                          
                        ?>
                  	   </p>
                 	  </form>
                  </div>
               </div>
            </div>
         </div>
     </div>
  </div>
</div>  
</div>
</div>
</form>
</div>
</div>
@else
    <div class="card-block"> 
        <h3 align="center">Sorry, You don't have access permission to this page !</h3>
    </div>
@endif        
</div>
@push('scripts')
    <script src="{{ asset('template/assets/plugins/jquery/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('template/assets/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('template/assets/plugins/jquery/jquery-easy.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
    function checkclass(data)
    {   
        if($( "#"+data ).is(':checked'))
        {                    
            $( "."+data ).not(this).prop( "checked", true );        
        } 
        else 
        {                    
            $( "."+data ).not(this).prop( "checked", false );        
        }        

    }

    @foreach (json_decode($data->route_access_list)->routelist as $listcheck)
        $( '#as{{str_replace(".","bb",$listcheck)}}' ).not(this).prop( "checked", true );                
    @endforeach
</script>
@endpush
@endsection
