@extends('layouts.app')

@section('_title')
<title>Member | Add</title>
@endsection

@section('content')
<?php $acl_btn = App\Models\Usergroup::where('name', \Auth::user()->roles)->where('route_access_list', 'LIKE', '%'.'member.create'.'%')->count(); ?>		 
@if($acl_btn != 0 || \Auth::user()->roles == 'superadmin')

	<div class="col-md-12">
		<div data-pages="card" class="card card-default">
			<div class="card-header">
				<div class="card-title">
					<h4>Add Member</h4>
				</div>
				<div class="card-controls">
					<ul>
						<li>
							<a data-toggle="collapse" class="card-collapse" href="#"><i class="card-icon card-icon-collapse"></i></a>
						</li>	
					</ul>
				</div>
			</div>
			<div class="card-block">
				<button onclick="goBack()" class="btn btn-complete m-b-15">Back</button>
				<form id="form1" action="{{ url('/member/insert') }}" method="POST" enctype="multipart/form-data" role="form" autocomplete="off">
				{{csrf_field()}}	
					<div class="form-group form-group-default">
						<label>Member Code</label>
						<input type="text" name="member_code" class="form-control" required="">
					</div>
					<div class="form-group form-group-default">
						<label>Member Name</label>
						<input type="text" name="member_name" class="form-control" required="">
					</div>
					<div class="form-group form-group-default">
						<label>Email</label>
						<input type="email" name="email" class="form-control" required="">
					</div>
					<div class="form-group form-group-default">
						<label>Password</label>
						<input type="password" name="password" class="form-control" required="">
					</div>
					<div class="form-group form-group-default">
						<label>Avatar</label>
						<input type="file" name="avatar" required="">
					</div>
					<div class="form-group form-group-default">
						<label>Division</label>
						<select name="division" class="full-width select2-hidden-accessible" data-init-plugin="select2" tabindex="-1" aria-hidden="true" required="">
							<option>Select Division</option>
							@foreach($division as $row)
								<option value="{{$row->id}}">{{$row->division_name}}</option>
							@endforeach		
						</select>
					</div>
					<div class="form-group form-group-default">
						<label>Company</label>
						<select name="company" class="full-width select2-hidden-accessible" data-init-plugin="select2" tabindex="-1" aria-hidden="true" required="">
							<option>Select Company</option>
							@if(count($company) > 1)
								@foreach($company as $row)
									<option value="{{$row->id}}">{{$row->company_name}}</option>
								@endforeach
							@else
								<option value="{{$company->id}}">{{$company->company_name}}</option>
							@endif
						</select>
					</div>
					<div class="form-group form-group-default">
						<label>Status</label>
						<select name="status" class="full-width select2-hidden-accessible" data-init-plugin="select2" tabindex="-1" aria-hidden="true" required="">
							<option>Select Status</option>
							<option value="{{ 1 }}">Active</option>
							<option value="{{ 0 }}">Not Active</option>
						</select>
					</div>
					<div class="form-group form-group-default">
						<label>Role</label>
						<select name="role" class="full-width select2-hidden-accessible" data-init-plugin="select2" tabindex="-1" aria-hidden="true" required="">
							<option>Select Role</option>
							@foreach($role as $row)
								<option value="{{$row->name}}">{{$row->name}}</option>
							@endforeach
						</select>
					</div>
					<input type="hidden" name="remember" value="{{csrf_token()}}">
					<button class="btn btn-success" type="submit">Submit</button>
				</form>
			</div>
		</div>
		@else
		<div class="card-block"> 
    	 	<h3 align="center">Sorry, You don't have access permission to this page !</h3>
		</div>
		@endif
	</div>
@endsection