@extends('layouts.app')

@section('_title')
<title>Member | Edit</title>
@endsection

@section('content')
	<div class="col-md-12">
		<div data-pages="card" class="card card-default">
			<div class="card-header">
				<div class="card-title">
					<h4>Edit Member</h4>
				</div>
				<div class="card-controls">
					<ul>
						<li>
							<a data-toggle="collapse" class="card-collapse" href="#"><i class="card-icon card-icon-collapse"></i></a>
						</li>	
					</ul>
				</div>
			</div>
			<div class="card-block">
				<button onclick="goBack()" class="btn btn-complete m-b-15">Back</button>
				<form id="form1" action="{{url('/member/update')}}/{{$member->id}}" method="POST" enctype="multipart/form-data" role="form" autocomplete="off">
				{{csrf_field()}}
				<input type="hidden" name="id" value="{{$member->id}}">		
					<div class="form-group form-group-default">
						<label>Member Code</label>
						<input type="text" name="member_code" class="form-control" value="{{$member->member_code}}">
					</div>
					<div class="form-group form-group-default">
						<label>Member Name</label>
						<input type="text" name="member_name" class="form-control" value="{{$member->member_name}}">
					</div>
					<div class="form-group form-group-default">
						<label>Email</label>
						<input type="text" name="email" class="form-control" value="{{$member->email}}">
					</div>
					<div class="form-group form-group-default">
						<label>Avatar</label>
													
						<img src="{{ asset('assets/images/') }}/{{$member->avatar}}" style="max-width:100px;max-height:100px; border-width:2px; margin-bottom: auto;" id="prev">
						<br>
						<input type="file" name="avatar" id="avatar" style="padding-top: 10px;">
					</div>
					<div class="form-group form-group-default">
						<label>Division</label>
						<select name="division" class="full-width select2-hidden-accessible" data-init-plugin="select2" tabindex="-1" aria-hidden="true" required="">
							@foreach($division as $show)
								<option value="{{$show->id}}" {{ $member->id_division == $show->id ? 'selected="selected"' : '' }} >{{ $show->division_name}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group form-group-default">
						<label>Role</label>
						<select name="role" class="full-width select2-hidden-accessible" data-init-plugin="select2" tabindex="-1" aria-hidden="true" required="">
							@foreach($role as $show)
								<option value="{{$show->id}}" {{ $member->id_role == $show->id ? 'selected="selected"' : '' }} >{{ $show->role_name}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group form-group-default">
						<label>Company</label>
						<select name="company" class="full-width select2-hidden-accessible" data-init-plugin="select2" tabindex="-1" aria-hidden="true" required="">
							@foreach($company as $show)
								<option value="{{$show->id}}" {{ $member->id_company == $show->id ? 'selected="selected"' : '' }} >{{ $show->company_name}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group form-group-default">
						<label>Status</label>
						<select name="status" class="full-width select2-hidden-accessible" data-init-plugin="select2" tabindex="-1" aria-hidden="true" required="">
							<option value="{{ 1 }}">Active</option>
							<option value="{{ 0 }}">Not Active</option>
						</select>
					</div>
					<button class="btn btn-success" type="submit">Submit</button>
				</form>
			</div>
		</div>
	</div>
@endsection