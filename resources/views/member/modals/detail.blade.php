<div class="modal fade slide-up disable-scroll" id="member-detail" tabindex="-1" role="dialog" aria-labelledby="member-detail" aria-hidden="false">
    <div class="modal-dialog ">
        <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                  <i class="pg-close fs-14"></i>
                </button>
                <h5 style="padding-left: 8.5px">Member Detail</h5>
            </div>
            <div class="modal-body">
                <form>
                	<div class="form-group-attached">
                		<div class="form-group row">
                			<label class="col-md-4 control-label">Member Code</label>
                			<div class="col-md-8" id="member-code"></div>
                		</div>
                		<div class="form-group row">
                			<label class="col-md-4">Member Name</label>
                			<div class="col-md-8" id="member-name"></div>
                		</div>
                        <div class="form-group row">
                            <label class="col-md-4">Avatar</label>
                            <div class="col-md-8" id="avatar"><img style="max-height: 100px; max-width: 100px;" src=""></div>
                        </div>
                		<div class="form-group row">
                			<label class="col-md-4">Email</label>
                			<div class="col-md-8" id="email"></div>
                		</div>
                		<div class="form-group row">
                			<label class="col-md-4">Company</label>
                			<div class="col-md-8" id="company"></div>
                		</div>
                		<div class="form-group row">
                			<label class="col-md-4">Division</label>
                			<div class="col-md-8" id="division"></div>
                		</div>
                        <div class="form-group row">
                            <label class="col-md-4">Role</label>
                            <div class="col-md-8" id="role"></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4">Status</label>
                            <div class="col-md-8" id="status"></div>
                        </div>
                		<div class="form-group row">
                			<label class="col-md-4">Created At</label>
                			<div class="col-md-8" id="created-at"></div>
                		</div>
                		<div class="form-group row">
                			<label class="col-md-4">Created By</label>
                			<div class="col-md-8" id="created-by"></div>
                		</div>
                	</div>
                </form>
            </div>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>