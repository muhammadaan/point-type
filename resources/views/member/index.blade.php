@extends('layouts.app')

@section('_title')
<title>Member | List</title>
@endsection

@push('styles')
	<link type="text/css" rel="stylesheet" href="{{ asset('template/assets/plugins/jquery-datatable/media/css/jquery.dataTables.css') }}">
	<link type="text/css" rel="stylesheet" href="{{ asset('template/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css') }}">
	<link media="screen" type="text/css" rel="stylesheet" href="{{ asset('template/assets/plugins/datatables-responsive/css/datatables.responsive.css') }}">
	<style>
		.form-group-attached {
			padding-left: 15px
		}
	</style>
@endpush

@section('content')
	<div class="col-md-12">
		<div data-pages="card" class="card card-default">
			<div class="card-header">
				<div class="card-title">
					<h4>Member List</h4>
				</div>
				<div class="card-controls">
					<ul>
						<li>
							<a data-toggle="collapse" class="card-collapse" href="#"><i class="card-icon card-icon-collapse"></i></a>
						</li>	
					</ul>
				</div>
				@if (session('message'))
				    <div class="alert alert-success" role="alert">
				    	<button class="close" data-dismiss="alert"></button>
				        {{ session('message') }}
				    </div>
				@endif	
			</div>
			<div class="card-block">
				<button onclick="goBack()" class="btn btn-complete m-b-15">Back</button>
				<button onclick="relocateAddMember()" class="btn btn-primary m-b-15">Add Member</button>
				<table id="memberTable" class="table table-hover" cellspacing="0" width="100%">
				    <thead>
				        <tr>
				            <th>Member Code</th>
				            <th>Member Name</th>
				            <th>Email</th>
				            <th>Avatar</th>
				            <th>Company</th>
				            <th>Division</th>
				            <th>Status</th>
				            <th>Action</th>
				        </tr>
				    </thead>
				    <tfoot style="display: table-header-group;">
				        <tr>
				            <th>Member Code</th>
				            <th>Member Name</th>
				            <th>Email</th>
				            <th>Avatar</th>
				            <th>Company</th>
				            <th>Division</th>
				            <th>Status</th>
				            <th>Action</th>
				        </tr>
				    </tfoot>
				    <tbody>
				    	@foreach($member as $show)
				        <tr>
				            <td>{{ $show->member_code }}</td>
				            <td>{{ $show->member_name }}</td>
				            <td>{{ $show->email }}</td>
				            <td><img src="{{ asset('assets/images/') }}/{{$show->avatar}}" style="max-width:100px;max-height:100px;float:left;"></td>
				            <td>{{ $show->company->company_name }}</td>
				            <td>{{ $show->division->division_name }}</td>
				            @if($show->status == 1)
				            <td>Active</td>
				            @else
				            <td>Not Active</td>
				            @endif
				            <td>
				            	<a href="{{url('/member/edit')}}/{{$show->id}}" title="Edit"><i class="fa fa-edit"></i></a>
			                    <a href="{{url('/member/delete')}}/{{$show->id}}" onclick="return ConfirmDelete()" title="Delete"><i class="pg-trash"></i></a>
			                    <a onclick="showModal({{ $show->id }})" href="#" data-toggle="modal" title="Show Detail"><i class="fa fa-search"></i></a>
			                </td>
				        </tr>
				        @endforeach
				    </tbody>
				</table>		
			</div>
		</div>
	</div>
	@include('member.modals.detail')
@endsection

@push('scripts')
	<script src="{{ asset('template/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('template/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('template/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js') }}"></script>
	<script src="{{ asset('template/assets/plugins/datatables-responsive/js/datatables.responsive.js') }}" type="text/javascript"></script>
	<script src="{{ asset('template/assets/plugins/datatables-responsive/js/lodash.min.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		function ConfirmDelete()
		{
		  var x = confirm("Are you sure you want to delete ?");
		  
		  if(x)
		  	return true;
		  else
		    return false;
			
		}   
	</script>
	<script>
		$(document).ready(function() {
		    // Setup - add a text input to each footer cell
		    $('#memberTable tfoot th').each( function () {
		        var title = $(this).text();
		        $(this).html( '<input type="text" />' );
		    } );

		    var table = $('#memberTable').DataTable();

		    // Apply the search
		    table.columns().every( function () {
		        var that = this;
		 
		        $( 'input', this.footer() ).on( 'keyup change', function () {
		            if ( that.search() !== this.value ) {
		                that
		                    .search( this.value )
		                    .draw();
		            }
		        } );
		    } );
		} );

		function relocateAddMember() {
			window.location.href = "{{ route('member.create') }}"
		}

		function showModal(id) 
		{
			$('#member-detail').modal('show')
			$.ajax({
				url: '{{ route("member.show") }}',
				data: {
					id: id
				},
				type: "GET",
				success: function(response) {
					console.log(response)
					$('#member-code').empty()
					$('#member-name').empty()
					$('#avatar img').attr('src', '')
					$('#email').empty()
					$('#company').empty()
					$('#division').empty()
					$('#role').empty()
					$('#status').empty()
					$('#created-at').empty()
					$('#created-by').empty()
					$('#member-code').append(response.member.member_code)
					$('#member-name').append(response.member.member_name)
					$('#avatar img').attr('src', '{{ asset("assets/images") }}' + '/' +response.member.avatar)
					$('#email').append(response.member.email)
					$('#company').append(response.company.company_name)
					$('#division').append(response.division.division_name)
					$('#role').append(response.role.role_name)
					$('#status').append(response.member.status ? 'Active' : 'Non Active')
					$('#created-at').append(response.member.created_at)
					$('#created-by').append(response.member.created_by)
				}
			})
		}
	</script>
@endpush