<div class="modal fade slide-up" id="point-wallet-detail" tabindex="-1" role="dialog" aria-labelledby="point-wallet-detail" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                  <i class="pg-close fs-14"></i>
                </button>
                <h5 style="padding-left: 8.5px">Detail</h5>
            </div>
            <div class="modal-body">
                <form id="transaction-form">
                    {{ csrf_field() }}
                    <span id="point-container"></span>
                    <input type="hidden" name="member_id">
                    <input type="hidden" name="point_wallet_id">
                	<div class="form-group-attached">
                		<div class="form-group row">
                			<label class="col-md-3 offset-md-1 control-label">Point Type</label>
                			<div class="col-md-7">
                                <select id="wallet-type-select" name="point_type_id" class="form-control">
                                </select>         
                            </div>
                		</div>
                	</div>
                    <br>
                    <div class="form-group-attached">
                        <div class="form-group row">
                            <label class="col-md-3 offset-md-1 control-label">Action</label>
                            <div class="col-md-7">
                                <select id="wallet-type-action" name="action_wallet_type" class="form-control">
                                    <option selected disabled>Select Action</option>
                                    <option value="add">Add Point</option>
                                    <option value="reduce">Reduce Point</option>
                                </select>         
                            </div>
                            <label class="col-md-3 offset-md-1 control-label" style="margin-top: 20px;">Point Amount</label>
                            <div class="col-md-7">
                                <input id="point-amount" type="text" name="point_in" class="form-control" style="margin-top: 20px;">
                            </div>
                            <div class="col-md-3 offset-md-1"><button class="btn btn-primary" type="submit">Submit</button></div>
                        </div>
                    </div>
                    <div class="form-group-attached">
                        <div class="form-group-row">
                            
                        </div>
                    </div>
                </form>
                <table width="100%" class="table table-hover" id="transaction-datatable">
                    <thead>
                        <th>Point In</th>
                        <th>Point Out</th>
                        <th>Point Type</th>
                        <th>Date</th>
                    </thead>
                    <tbody id="transaction-table"></tbody>
                </table>
            </div>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>