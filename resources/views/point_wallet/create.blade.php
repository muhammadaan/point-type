@extends('layouts.app')

@section('_title')
<title>Point Wallet | Add</title>
@endsection

@section('content')

<?php $acl_btn = App\Models\Usergroup::where('name', \Auth::user()->roles)->where('route_access_list', 'LIKE', '%'.'point_wallet.create'.'%')->count(); ?>		 
@if($acl_btn != 0 || \Auth::user()->roles == 'superadmin')

	<div class="col-md-12">
		<div data-pages="card" class="card card-default">
			<div class="card-header">
				<div class="card-title">
					<h4>Add Point Wallet</h4>
				</div>
				<div class="card-controls">
					<ul>
						<li>
							<a data-toggle="collapse" class="card-collapse" href="#"><i class="card-icon card-icon-collapse"></i></a>
						</li>	
					</ul>
				</div>
			</div>
			<div class="card-block">
				<button onclick="goBack()" class="btn btn-complete m-b-15">Back</button>
				<form action="{{ url('/point_wallet/insert') }}" method="POST" enctype="multipart/form-data" id="formpoint" role="form" autocomplete="off">
				{{csrf_field()}}	
					<div class="form-group form-group-default">
						<label>Member</label>
						<select name="member" class="form-control" data-init-plugin="select2" tabindex="-1">
							<option value="">Select Member</option>
							@foreach($member as $show)
							<option value="{{$show->id}}">{{$show->member_name}}</option>
							@endforeach	
						</select>
					</div>
					<div class="form-group form-group-default">
						<label>Company</label>
						<select name="company" class="form-control" data-init-plugin="select2" tabindex="-1">
							<option value="">Select Company</option>
							@if(count($company) > 1)
								@foreach($company as $show)
								<option value="{{$show->id}}">{{$show->company_name}}</option>
								@endforeach
							@else
								<option value="{{$company->id}}">{{$company->company_name}}</option>
							@endif
						</select>
					</div>
					<div class="form-group form-group-default">
						<label>Select Point Type</label>
						<select class="form-control" name="point_type_id" data-init-plugin="select2" tabindex="-1">
							<option value="" selected>Select Point Type</option>
							@foreach($point_type as $type)
								<option value="{{ $type->id }}">{{ $type->point_type }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group form-group-default col-md-6">
						<label>Input Starting Point</label>
						<input type="text" name="current" class="col-lg-11" required="" id="start">
					</div>
					<button class="btn btn-success" type="submit">Submit</button>
				</form>
			</div>
		</div>
		@else
		<div class="card-block"> 
    	 	<h3 align="center">Sorry, You don't have access permission to this page !</h3>
		</div>
		@endif
	</div>	
@endsection

@push('scripts')
	<script src="{{ asset('template/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('template/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('template/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js') }}"></script>
	<script src="{{ asset('template/assets/plugins/datatables-responsive/js/datatables.responsive.js') }}" type="text/javascript"></script>
	<script src="{{ asset('template/assets/plugins/datatables-responsive/js/lodash.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('template/assets/plugins/jquery-ui/jquery-ui_back.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('template/assets/js_validation/jquery.validate.js') }}" type="text/javascript"></script>	
	<script>
		$(document).ready(function() {
	    	
			 $( "#start" ).spinner({min: 0, max: 500});
 			 $('#formpoint').validate({

 			 	  ignore: [],
 			 	  rules: {
 			 	    member: {
 			 	      required: true,
 			 	      remote: '{{route('point_wallet.check-member')}}'
 			 	    },
 			 	    current: {
 			 	      required: true
 			 	    } 
 			 	  },
 			 	  messages: {
 			 	      
 			 	      member: {  
 			 	        remote: "Member already exist"
 			 	      }

 			 	  }

 			 });			
		});
	</script>
@endpush