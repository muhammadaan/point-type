@extends('layouts.app')

@section('_title')
<title>Point Wallet | List</title>
@endsection

@push('styles')
	<link type="text/css" rel="stylesheet" href="{{ asset('template/assets/plugins/jquery-datatable/media/css/jquery.dataTables.css') }}">
	<link type="text/css" rel="stylesheet" href="{{ asset('template/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css') }}">
	<link media="screen" type="text/css" rel="stylesheet" href="{{ asset('template/assets/plugins/datatables-responsive/css/datatables.responsive.css') }}">
	<style>
		
	</style>
@endpush

@section('content')

<?php $acl_btn = App\Models\Usergroup::where('name', \Auth::user()->roles)->where('route_access_list', 'LIKE', '%'.'point_wallet.index'.'%')->count(); ?>		 
@if($acl_btn != 0 || \Auth::user()->roles == 'superadmin')

	<div class="col-md-12">
		<div data-pages="card" class="card card-default">
			<div class="card-header">
				<div class="card-title">
					<h4>Point Wallet List</h4>
				</div>
				<div class="card-controls">
					<ul>
						<li>
							<a data-toggle="collapse" class="card-collapse" href="#"><i class="card-icon card-icon-collapse"></i></a>
						</li>	
					</ul>
				</div>
			@if(session('message'))
			    <div class="alert alert-success" role="alert">
			    	<button class="close" data-dismiss="alert"></button>
			        {{ session('message') }}
			    </div>
			@endif
			</div>
			<div class="card-block">
				<button onclick="goBack()" class="btn btn-complete m-b-15">Back</button>
				<button onclick="relocateAddPointWallet()" class="btn btn-primary m-b-15">Add Point Wallet</button>
				<table id="pointWalletTable" class="table table-hover" cellspacing="0" width="100%">
				    <thead>
				        <tr>
				            <th>Member Name</th>
				            <th>Balance</th>
				            <th>Wallet Level</th>
				            <th>Balance KPI</th>
				            <th>Wallet Level KPI</th>
				            <th>Created At</th>
				            <th>Updated By</th>
				            <th>Created By</th>
				            <th>Action</th>
				        </tr>
				    </thead>
				    <tfoot style="display: table-header-group;">
				        <tr>
				            <th>Member Name</th>
				            <th>Balance</th>
				            <th>Wallet Level</th>
				            <th>Balance KPI</th>
				            <th>Wallet Level KPI</th>
				            <th>Created At</th>
				            <th>Updated By</th>
				            <th>Created By</th>
				            <th>Action</th>
				        </tr>
				    </tfoot>
				    <tbody>
				    	@foreach($point_wallet as $show)
				    	<tr>
				            <td>{{ $show->member->member_name }}</td>
				            <td>{{ $show->current_point }}</td>
				            <td>{{ $show->wallet_level != null ? $show->wallet_level->level_name : 'Wallet Not Found'  }}</td>
				            <td>{{ !empty($show->current_point_kpi) ? $show->current_point_kpi : 0 }}</td>
				            <td>
				            	<?php 
				            	$current_point_kpi = $show->current_point_kpi != null ? $show->current_point_kpi : 0;
				            	$wallet_level_id = \App\Http\Controllers\point_wallet\helper\PointWalletHelper::getWalletLevelKpiId($current_point_kpi) != null ? \App\Http\Controllers\point_wallet\helper\PointWalletHelper::getWalletLevelKpiId($current_point_kpi) : 0 ;
				            	$wallet_level = \App\Models\WalletLevel::find($wallet_level_id);
				            	echo !empty($wallet_level) ? $wallet_name : 'Wallet Not Found';
				             	?>
				             	
				             </td>
				            <td>{{ $show->created_at }}</td>
				            <td>{{ $show->updated_by }}</td>
				            <td>{{ $show->created_by }}</td>
				            <td>
				            	<a onclick="showModal({{ $show->id }}, {{ $show->member_id }})" href="#" data-toggle="modal" title="Show Detail"><i class="fa fa-search"></i></a>
			                    <a href="{{url('/point_wallet/delete')}}/{{$show->id}}" onclick="return ConfirmDelete()" title="Delete"><i class="pg-trash"></i></a>
				            </td>
				        </tr>
				        @endforeach
				    </tbody>
				</table>		
			</div>
		</div>
		@else
		<div class="card-block"> 
    	 	<h3 align="center">Sorry, You don't have access permission to this page !</h3>
		</div>
		@endif
	</div>
	@include('point_wallet.modals.detail')
@endsection

@push('scripts')
	<script src="{{ asset('template/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('template/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('template/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js') }}"></script>
	<script src="{{ asset('template/assets/plugins/datatables-responsive/js/datatables.responsive.js') }}" type="text/javascript"></script>
	<script src="{{ asset('template/assets/plugins/datatables-responsive/js/lodash.min.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		function ConfirmDelete()
		{
		  var x = confirm("Are you sure you want to delete ?");
		  
		  if(x)
		  	return true;
		  else
		    return false;
			
		}   
	</script>
	<script>
		function relocateAddPointWallet() {
			window.location.href = "{{ route('point_wallet.create') }}"
		}

		// make datatable width fixed
		$(document).on('shown.bs.modal', function (e) {
		      $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
		});

		function reloadDataTransaction(id, member_id)
		{
			// id = point wallet id
			$http('{{ route("point_wallet.transaction") }}' + '?point_wallet_id=' + id).get().then(function(resp) {
				resp = JSON.parse(resp)
				var transaction = resp.transaction
				var point_type = resp.point_type
				$('#transaction-table').empty()
				for(i in transaction)
				{
					$('#transaction-table').append('<tr><td>'+(transaction[i].point_in != null ? transaction[i].point_in : 0 )+'</td><td>'+ (transaction[i].point_out != null ? transaction[i].point_out : 0 ) +'</td>'+ '<td>' + transaction[i].point_type.point_type +'</td>' +'<td>'+transaction[i].transaction_date+'</td></tr>')
				}
				$('#transaction-datatable').DataTable({
					"destroy": true,
					"order": [[3, 'desc']]
				});
				$('#wallet-type-select').empty()
				$('#wallet-type-select').append('<option selected disabled>Select Wallet Type</option>')
				for(i in point_type)
				{
					$('#wallet-type-select').append('<option value="' + point_type[i].id + '">' + point_type[i].point_type + '</option>')
				}
			}).catch(function(err) {
				// console.log(err)
			})
		}

		function showModal(id, member_id)
		{
			$('#point-wallet-detail').modal()
			$('[name="member_id"]').val(member_id)
			$('[name="point_wallet_id"]').val(id)
			reloadDataTransaction(id, member_id)
		}
		$('#wallet-type-action').change(function() {
			if($('#wallet-type-action option:selected').val() == 'add')
			{
				$('#point-amount').attr('name', 'point_in').val('')
				$('#point-container').empty()
				$('#point-container').append('<input type="hidden" name="point_out">')
			}
			else
			{
				$('#point-amount').attr('name', 'point_out').val('')
				$('#point-container').empty()
				$('#point-container').append('<input type="hidden" name="point_in">')
			}	
		})
		
		$('#transaction-form').submit(function(e) {
			e.preventDefault()
			var data = {
				'member_id' : $('[name="member_id"]').val(),
				'point_in' : $('[name="point_in"]').val(),
				'point_out' : $('[name="point_out"]').val(),
				'point_wallet_id' : $('[name="point_wallet_id"]').val(),
				'point_type_id' : $('#wallet-type-select option:selected').val(),
				'_token' : $('[name="_token"]').val()
			}
			$http('{{ route("point-transaction.store") }}').post(data).then(function(resp) {
				resp = JSON.parse(resp)
				reloadDataTransaction($('[name="point_wallet_id"]').val(), $('[name="member_id"]').val())	
				$('#transaction-datatable').DataTable().row.add([resp.point_in != null ? resp.point_in : 0, resp.point_out != null ? resp.point_out : 0, resp.point_type.point_type, resp.transaction_date]).draw()
			})

		})

		$(document).ready(function() {
		    // Setup - add a text input to each footer cell
		    $('#pointWalletTable tfoot th').each( function () {
		        var title = $(this).text();
		        $(this).html( '<input type="text" />' );
		    } );

		    var table = $('#pointWalletTable').DataTable();

		    // Apply the search
		    table.columns().every( function () {
		        var that = this;
		 
		        $( 'input', this.footer() ).on( 'keyup change', function () {
		            if ( that.search() !== this.value ) {
		                that
		                    .search( this.value )
		                    .draw();
		            }
		        } );
		    } );
		} );
	</script>
@endpush