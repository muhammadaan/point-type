<div class=" container-fluid  container-fixed-lg footer">
  <div class="copyright sm-text-center">
    <p class="small no-margin pull-left sm-pull-reset">
      <span class="hint-text">Copyright &copy; 2018 </span>
      <span class="font-montserrat">Point Type Solutech</span>.
      <span class="hint-text">All rights reserved. </span>
    </p>
    <div class="clearfix"></div>
  </div>
</div>