<div class="sidebar-header">
        <h3 style="color: white; padding-top: 10px;"><b>{{ Auth::user()->member ? Auth::user()->member->company->company_name : 'Solutech' }}</b></h3>
      </div>
      <!-- END SIDEBAR MENU HEADER-->
      <!-- START SIDEBAR MENU -->
      <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
          <li class="m-t-30 ">
            <a href="{{ route('home') }}" class="detailed">
              <span class="title">Dashboard</span>
            </a>
            <span class="bg-success icon-thumbnail"><i class="pg-home"></i></span>
          </li>
          <li>
            <a href="javascript:;"><span class="title">Wisata</span>
            <span class="arrow"></span></a>
            <span class="icon-thumbnail"><i class="fa fa-building"></i></span> 
            <ul class="sub-menu">
              <li>
                <a href="{{ route('company.index') }}">Wisata Religi</a>
                <span class="icon-thumbnail"><i class="fa fa-circle-o"></i></span>
              </li>
              <li>
                <a href="{{ route('division.index') }}">Wisata Sejarah</a>
                <span class="icon-thumbnail"><i class="fa fa-circle-o"></i></span>
              </li>
              <li>
                <a href="{{ route('division.index') }}">Wisata Budaya</a>
                <span class="icon-thumbnail"><i class="fa fa-circle-o"></i></span>
              </li>
              <li>
                <a href="{{ route('division.index') }}">Wisata Kuliner</a>
                <span class="icon-thumbnail"><i class="fa fa-circle-o"></i></span>
              </li>
              <li>
                <a href="{{ route('division.index') }}">Ekonomi Kreatif</a>
                <span class="icon-thumbnail"><i class="fa fa-circle-o"></i></span>
              </li>
            </ul>     
          </li>
          
        <li>
          <a href="{{route('membercard.index')}}"><span class="title">User</span>
          <span></span></a>
          <span class="icon-thumbnail"><i class="fa fa-gift"></i></span> 
        </li>


        </ul>
        <div class="clearfix"></div>
      </div>