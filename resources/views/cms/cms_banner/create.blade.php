@extends('layouts.app')

@section('_title')
<title>Banner | Add</title>
@endsection

@section('content')
<?php $acl_btn = App\Models\Usergroup::where('name', \Auth::user()->roles)->where('route_access_list', 'LIKE', '%'.'cms_banner.create'.'%')->count(); ?>		 
@if($acl_btn != 0 || \Auth::user()->roles == 'superadmin')

	<div class="col-md-12">
		<div data-pages="card" class="card card-default">
			<div class="card-header">
				<div class="card-title">
					<h4>Add Banner</h4>
				</div>
				<div class="card-controls">
					<ul>
						<li>
							<a data-toggle="collapse" class="card-collapse" href="#"><i class="card-icon card-icon-collapse"></i></a>
						</li>	
					</ul>
				</div>
			</div>
			<div class="card-block">
				<button onclick="goBack()" class="btn btn-complete m-b-15">Back</button>
				<form id="form1" action="{{ url('/cms_banner/insert')}}" method="POST" enctype="multipart/form-data" role="form" autocomplete="off">
				{{csrf_field()}}
					<div class="form-group form-group-default col-md-9" style="border: 0px;">
					 	<div class="form-input-group">
							<label class="inline">Status</label>
						</div>
						<div>
							<input type="checkbox" name="status" data-init-plugin="switchery" data-size="medium" data-color="primary" />
						</div>
					</div>
					<div class="form-group form-group-default col-md-9">
						<label>Company</label>
						<select name="cms_home_id" class="full-width select2-hidden-accessible" data-init-plugin="select2" tabindex="-1" aria-hidden="true" required="">
							<option>Select Company</option>
							@foreach($home as $row)
							<option value="{{$row->id}}">{{$row->company->company_name}}</option>
							@endforeach
						</select>
					</div>		
					<div class="form-group form-group-default col-md-9">
						<label>Image</label>
						<input type="file" name="image" class="form-control" required="">
					</div>
					<div class="form-group form-group-default col-md-9">
						<label>URL</label>
						<input type="text" name="url" class="form-control" >
					</div>
					<button class="btn btn-success" type="submit">Submit</button>
				</form>
			</div>
		</div>
		@else
		<div class="card-block"> 
    	 	<h3 align="center">Sorry, You don't have access permission to this page !</h3>
		</div>
		@endif
	</div>
@endsection