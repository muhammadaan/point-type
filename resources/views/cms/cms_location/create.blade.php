@extends('layouts.app')

@section('_title')
<title>Location | Add</title>
@endsection

@section('content')
<?php $acl_btn = App\Models\Usergroup::where('name', \Auth::user()->roles)->where('route_access_list', 'LIKE', '%'.'cms_location.create'.'%')->count(); ?>		 
@if($acl_btn != 0 || \Auth::user()->roles == 'superadmin')

	<div class="col-md-12">
		<div data-pages="card" class="card card-default">
			<div class="card-header">
				<div class="card-title">
					<h4>Add Location</h4>
				</div>
				<div class="card-controls">
					<ul>
						<li>
							<a data-toggle="collapse" class="card-collapse" href="#"><i class="card-icon card-icon-collapse"></i></a>
						</li>	
					</ul>
				</div>
			</div>
			<div class="card-block">
				<button onclick="goBack()" class="btn btn-complete m-b-15">Back</button>
				<form id="form1" action="{{ url('/cms_location/insert')}}" method="POST" enctype="multipart/form-data" role="form" autocomplete="off">
				{{csrf_field()}}
					<div class="form-group form-group-default col-md-9">
						<label>Company</label>
						<select name="cms_home_id" class="full-width select2-hidden-accessible" data-init-plugin="select2" tabindex="-1" aria-hidden="true" required="">
							<option>Select Company</option>
							@foreach($home as $row)
							<option value="{{$row->id}}">{{$row->company->company_name}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group form-group-default col-md-9">
						<label>Name</label>
						<input type="text" name="name" class="form-control" required="">
					</div>
					<div class="form-group form-group-default col-md-9">
						<label>Country</label>
						<input type="text" name="country" class="form-control" required="">
					</div>
					<div class="form-group form-group-default col-md-9">
						<label>Region</label>
						<input type="text" name="region" class="form-control" required="">
					</div>
					<div class="form-group form-group-default col-md-9">
						<label>Address</label>
						<textarea name="address" class="form-control"></textarea>
					</div>	
					<div class="form-group form-group-default col-md-9">
						<label>Image</label>
						<input type="file" name="image" class="form-control" required="">
					</div>
					<div class="form-group form-group-default col-md-9">
						<label>URL</label>
						<input type="text" name="url" class="form-control">
					</div>
					<div class="form-group form-group-default col-md-9">
						<label>GPS</label>
						<input type="hidden" name="gps">
						<input type="text" id="searchmap" class="form-control" required="">
					</div>
					<div id="map-canvas"></div>				
					<button class="btn btn-success" type="submit">Submit</button>
				</form>
			</div>
		</div>
		@else
		<div class="card-block"> 
    	 	<h3 align="center">Sorry, You don't have access permission to this page !</h3>
		</div>
		@endif
	</div>
	<script type="text/javascript">
		
		 function initMap() 
		 {
		    
		 	var map = new google.maps.Map(document.getElementById('map-canvas'), {
	          center: {lat: -6.175392, lng: 106.827153},
	          zoom: 15
	        });
	        var marker = new google.maps.Marker({
	        position: {
	          lat:  -6.175392,
	          lng: 106.827153
	        },
	        map: map,
	        draggable: true
	        });

		    var input = document.getElementById('searchmap');  
		    var autocomplete = new google.maps.places.Autocomplete(input);

		    autocomplete.bindTo('bounds', map);
		    var infowindow = new google.maps.InfoWindow();

		    marker = new google.maps.Marker({
		      map: map,
		      anchorPoint: new google.maps.Point(0, -29),
		      draggable: true
		    });

		    var infowindowContent = document.getElementById('infowindow-content');
		    infowindow.setContent(infowindowContent);
		    var marker = new google.maps.Marker({
		      map: map,
		      anchorPoint: new google.maps.Point(0, -29)
		    });

		    autocomplete.addListener('place_changed', function() {
		      infowindow.close();
		      marker.setVisible(false);
		      var place = autocomplete.getPlace();
		      if (!place.geometry) {
		        
		        window.alert("No details available for input: '" + place.name + "'");
		        return;
		      }

		      // If the place has a geometry, then present it on a map.
		      if (place.geometry.viewport) {
		        map.fitBounds(place.geometry.viewport);
		      } else {
		        map.setCenter(place.geometry.location);
		        map.setZoom(30);  // Why 17? Because it looks good.
		      }
		      marker.setPosition(place.geometry.location);
		      marker.setVisible(true);

		      var address = '';
		      if (place.address_components) {
		        address = [
		          (place.address_components[0] && place.address_components[0].short_name || ''),
		          (place.address_components[1] && place.address_components[1].short_name || ''),
		          (place.address_components[2] && place.address_components[2].short_name || '')
		        ].join(' ');
		      }
		    });

			  google.maps.event.addListener(marker,'position_changed', function(){
			  var lat = marker.getPosition().lat();
			  var lng = marker.getPosition().lng();

			  console.log(lat+" "+lng)
			  var lokasi = [];
			  lokasi.push(lat)
			  lokasi.push(lng)

			  $('[name="gps"]').val(lat+" "+lng);

			 });
	  }
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnZEfJBQQ1DTkBn4qqwXJmLTNhHds8r2o&callback=initMap&libraries=places" async defer></script>	
@endsection