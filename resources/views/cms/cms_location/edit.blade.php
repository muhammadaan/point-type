@extends('layouts.app')

@section('_title')
<title>Location | Add</title>
@endsection

@section('content')
<?php $acl_btn = App\Models\Usergroup::where('name', \Auth::user()->roles)->where('route_access_list', 'LIKE', '%'.'cms_location.create'.'%')->count(); ?>		 
@if($acl_btn != 0 || \Auth::user()->roles == 'superadmin')

	<div class="col-md-12">
		<div data-pages="card" class="card card-default">
			<div class="card-header">
				<div class="card-title">
					<h4>Add Location</h4>
				</div>
				<div class="card-controls">
					<ul>
						<li>
							<a data-toggle="collapse" class="card-collapse" href="#"><i class="card-icon card-icon-collapse"></i></a>
						</li>	
					</ul>
				</div>
			</div>
			<div class="card-block">
				<button onclick="goBack()" class="btn btn-complete m-b-15">Back</button>
				<form id="form1" action="{{ url('/cms_location/update')}}/{{$location->id}}" method="POST" enctype="multipart/form-data" role="form" autocomplete="off">
				{{csrf_field()}}
					<input type="hidden" name="id" value="{{ $location->id }}">
					<div class="form-group form-group-default col-md-9">
						<label>Company</label>
						<select name="cms_home_id" class="full-width select2-hidden-accessible" data-init-plugin="select2" tabindex="-1" aria-hidden="true" required="">
							<option>Select Company</option>
							@foreach($home as $row)
							<option value="{{$row->id}}" {{$location->cms_home_id == $row->id ? 'selected="selected"' : '' }}>{{$row->company->company_name}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group form-group-default col-md-9">
						<label>Name</label>
						<input type="text" name="name" class="form-control" required="" value="{{$location->name}}">
					</div>
					<div class="form-group form-group-default col-md-9">
						<label>Country</label>
						<input type="text" name="country" class="form-control" required="" value="{{$location->country}}">
					</div>
					<div class="form-group form-group-default col-md-9">
						<label>Region</label>
						<input type="text" name="region" class="form-control" required="" value="{{$location->region}}">
					</div>
					<div class="form-group form-group-default col-md-9">
						<label>Address</label>
						<textarea name="address" class="form-control">{{$location->address}}</textarea>
					</div>	
					<div class="form-group form-group-default col-md-9">
						<label>Image</label>
						<img src="{{ asset('assets/images/') }}/{{ $location->image }}" style="max-width:100px;max-height:100px; border-width:2px; margin-bottom: auto;" id="prev">
						<br>
						<input type="file" name="image" id="avatar" style="padding-top: 10px; padding-bottom:8px; ">
					</div>
					<div class="form-group form-group-default col-md-9">
						<label>URL</label>
						<input type="text" name="url" class="form-control" required="" value="{{$location->url}}">
					</div>
					<div class="form-group form-group-default col-md-9">
						<label>GPS</label>
						<input type="text" name="gps" class="form-control" required="" value="{{$location->gps}}">
					</div>				
					<button class="btn btn-success" type="submit">Submit</button>
				</form>
			</div>
		</div>
		@else
		<div class="card-block"> 
    	 	<h3 align="center">Sorry, You don't have access permission to this page !</h3>
		</div>
		@endif
	</div>
@endsection