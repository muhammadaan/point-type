@extends('layouts.app')

@section('_title')
<title>Home | List</title>
@endsection

@push('styles')
	<link type="text/css" rel="stylesheet" href="{{ asset('template/assets/plugins/jquery-datatable/media/css/jquery.dataTables.css') }}">
	<link type="text/css" rel="stylesheet" href="{{ asset('template/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css') }}">
	<link media="screen" type="text/css" rel="stylesheet" href="{{ asset('template/assets/plugins/datatables-responsive/css/datatables.responsive.css') }}">
	<style>
		.form-group-attached {
			padding-left: 15px
		}
	</style>
@endpush

@section('content')
<?php $acl_btn = App\Models\Usergroup::where('name', \Auth::user()->roles)->where('route_access_list', 'LIKE', '%'.'cms_home.index'.'%')->count(); ?>		 
@if($acl_btn != 0 || \Auth::user()->roles == 'superadmin')

	<div class="col-md-12">
		<div data-pages="card" class="card card-default">
			<div class="card-header">
				<div class="card-title">
					<h4>Home List</h4>
				</div>
				<div class="card-controls">
					<ul>
						<li>
							<a data-toggle="collapse" class="card-collapse" href="#"><i class="card-icon card-icon-collapse"></i></a>
						</li>	
					</ul>
				</div>
				@if (session('status'))
				    <div class="alert alert-success" role="alert">
				    	<button class="close" data-dismiss="alert"></button>
				        {{ session('status') }}
				    </div>
				@endif	
			</div>
			<div class="card-block">
				<button onclick="goBack()" class="btn btn-complete m-b-15">Back</button>
				<button onclick="relocateAddCMS()" class="btn btn-primary m-b-15">Add Home</button>
				<table id="cmsHomeTable" class="table table-hover" cellspacing="0" width="100%">
				    <thead>
				        <tr>
				            <th>Company Name</th>
				            <th>Banner Status</th>
				            <th>Service Status</th>
				            <th>Team Status</th>
				            <th>About Status</th>
				            <th>News Status</th>
				            <th>Portfolio Status</th>
				            <th>Location Status</th>
				            <th>Action</th>
				        </tr>
				    </thead>
				    <tfoot style="display: table-header-group;">
				        <tr>
				            <th>Company Name</th>
				            <th>Banner Status</th>
				            <th>Service Status</th>
				            <th>Team Status</th>
				            <th>About Status</th>
				            <th>News Status</th>
				            <th>Portfolio Status</th>
				            <th>Location Status</th>
				            <th>Action</th>  
				        </tr>
				    </tfoot>
				    <tbody>
				    	@foreach($cms as $show)
				        <tr>
				            <td>{{ $show->company->company_name or '' }}</td>
				            <td>{{ $show->banner_main_status == 1 ? 'Active' : 'In Active'}}</td>
				            <td>{{ $show->services_status == 1 ? 'Active' : 'In Active'}}</td>
				            <td>{{ $show->team_status == 1 ? 'Active' : 'In Active'}}</td>
				            <td>{{ $show->about_status == 1 ? 'Active' : 'In Active'}}</td>
				            <td>{{ $show->news_status == 1 ? 'Active' : 'In Active'}}</td>
				            <td>{{ $show->portfolio_status == 1 ? 'Active' : 'In Active'}}</td>
				            <td>{{ $show->location_status == 1 ? 'Active' : 'In Active'}}</td>
				            <td>
				            	<a onclick="showModal({{ $show->id }})" href="#" data-toggle="modal" title="Show Detail"><i class="fa fa-search"></i></a>
				            	<a href="{{ url('/cms_home/edit')}}/{{ $show->id }}" title="Edit"><i class="fa fa-edit"></i></a>
			                    <a href="{{ url('/cms_home/delete')}}/{{ $show->id }}" onclick="return ConfirmDelete()" title="Delete"><i class="pg-trash"></i></a>
			                </td>
				        </tr>
				        @endforeach
				    </tbody>
				</table>		
			</div>
		</div>
		@else
		<div class="card-block"> 
    	 	<h3 align="center">Sorry, You don't have access permission to this page !</h3>
		</div>
		@endif
	</div>
	@include('cms.cms_home.modals.detail')
@endsection

@push('scripts')
	<script src="{{ asset('template/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('template/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('template/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js') }}"></script>
	<script src="{{ asset('template/assets/plugins/datatables-responsive/js/datatables.responsive.js') }}" type="text/javascript"></script>
	<script src="{{ asset('template/assets/plugins/datatables-responsive/js/lodash.min.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		function ConfirmDelete()
		{
		  var x = confirm("Are you sure you want to delete ?");
		  
		  if(x)
		  	return true;
		  else
		    return false;
			
		}   
	</script>
	<script>
		$(document).ready(function() {
		    // Setup - add a text input to each footer cell
		    $('#cmsHomeTable tfoot th').each( function () {
		        var title = $(this).text();
		        $(this).html( '<input type="text" style="width:80%;" />' );
		    } );

		    var table = $('#cmsHomeTable').DataTable();

		    // Apply the search
		    table.columns().every( function () {
		        var that = this;
		 
		        $( 'input', this.footer() ).on( 'keyup change', function () {
		            if ( that.search() !== this.value ) {
		                that
		                    .search( this.value )
		                    .draw();
		            }
		        } );
		    } );
		} );

		function relocateAddCMS() {
			window.location.href = "{{ route('cms_home.create') }}"
		}

		function showModal(id)
		{	
			$('#cms_home-detail').modal('show')
			$.ajax({
				url: '{{ route("cms_home.show") }}',
				data: {
					id: id
				},
				type: "GET",
				success: function(response) {
					$('#company_name').empty()
					$('#banner_main_status').empty()
					$('#services_status').empty()
					$('#team_status').empty()
					$('#about_status').empty()
					$('#news_status').empty()
					$('#portfolio_status').empty()
					$('#location_status').empty()
					$('#contact_phone').empty()
					$('#contact_gps_coor').empty()
					$('#contact_email').empty()
					$('#contact_instagram').empty()
					$('#contact_twitter').empty()
					$('#contact_facebook').empty()

					$('#company_name').append(response.company_name)
					$('#banner_main_status').append(response.banner_status)
					$('#services_status').append(response.services_status)
					$('#team_status').append(response.team_status)
					$('#about_status').append(response.about_status)
					$('#news_status').append(response.news_status)
					$('#portfolio_status').append(response.portfolio_status)
					$('#location_status').append(response.location_status)
					$('#contact_phone').append(response.contact_phone)
					$('#contact_gps_coor').append(response.contact_gps_coor)
					$('#contact_email').append(response.contact_email)
					$('#contact_instagram').append(response.contact_instagram)
					$('#contact_twitter').append(response.contact_twitter)
					$('#contact_facebook').append(response.contact_twitter)					
					
				}
			})
		}

	</script>
@endpush