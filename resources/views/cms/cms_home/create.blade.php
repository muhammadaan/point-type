@extends('layouts.app')

@section('_title')
<title>Home | Add</title>
@endsection

@section('content')
<?php $acl_btn = App\Models\Usergroup::where('name', \Auth::user()->roles)->where('route_access_list', 'LIKE', '%'.'cms_home.create'.'%')->count(); ?>		 
@if($acl_btn != 0 || \Auth::user()->roles == 'superadmin')

	<div class="col-md-12">
		<div data-pages="card" class="card card-default">
			<div class="card-header">
				<div class="card-title">
					<h4>Add Home</h4>
				</div>
				<div class="card-controls">
					<ul>
						<li>
							<a data-toggle="collapse" class="card-collapse" href="#"><i class="card-icon card-icon-collapse"></i></a>
						</li>	
					</ul>
				</div>
			</div>
			<div class="card-block">
				<button onclick="goBack()" class="btn btn-complete m-b-15">Back</button>
				<form id="formhome_create" action="{{ url('/cms_home/insert') }}" method="POST" enctype="multipart/form-data" role="form" autocomplete="off">
				{{csrf_field()}}
					<div class="form-group form-group-default col-md-9" style="border: 0px;">
					 	<div class="form-input-group">
							<label class="inline">Banner Status</label>
						</div>
						<div>
							<input type="checkbox" name="banner_main_status" data-init-plugin="switchery" data-size="medium" data-color="primary" />
						</div>
					</div>

					<div class="form-group form-group-default col-md-9">
						<label>Company Name</label>
						<select name="company_id" class="full-width select2-hidden-accessible" data-init-plugin="select2" tabindex="-1" aria-hidden="true" required="">
							<option>Select Company Name</option>
							@foreach($company as $row)
								<option value="{{$row->id}}">{{ $row->company_name }}</option>
							@endforeach
						</select>
					</div>
					<div class="row col-md-10">
						<div class="form-group form-group-default col-md-5" style="border: 0px;">
						 	<div class="form-input-group">
								<label class="inline">Service Status</label>
							</div>
							<div>
								<input type="checkbox" name="services_status" data-init-plugin="switchery" data-size="medium" data-color="primary" />
							</div>
						</div>
						<div class="form-group form-group-default col-md-5" style="border: 0px;">
							<div class="form-input-group">
								<label class="inline">Team Status</label>
							</div>
							<div>
								<input type="checkbox" name="team_status" data-init-plugin="switchery" data-size="medium" data-color="primary" />
							</div>							
						</div>
					</div>

					<div class="row col-md-10">
						<div class="form-group form-group-default col-md-5" style="border: 0px;">
						 	<div class="form-input-group">
								<label class="inline">About Status</label>
							</div>
							<div>
								<input type="checkbox" name="about_status" data-init-plugin="switchery" data-size="medium" data-color="primary" />
							</div>
						</div>
						<div class="form-group form-group-default col-md-5" style="border: 0px;">
						 	<div class="form-input-group">
								<label class="inline">News Status</label>
							</div>
							<div>
								<input type="checkbox" name="news_status" data-init-plugin="switchery" data-size="medium" data-color="primary" />
							</div>
						</div>
					</div>

					<div class="row col-md-10">
						<div class="form-group form-group-default col-md-5" style="border: 0px;">
						 	<div class="form-input-group">
								<label class="inline">Portofolio Status</label>
							</div>
							<div>
								<input type="checkbox" name="portfolio_status" data-init-plugin="switchery" data-size="medium" data-color="primary" />
							</div>
						</div>
						<div class="form-group form-group-default col-md-5" style="border: 0px;">
						 	<div class="form-input-group">
								<label class="inline">Location Status</label>
							</div>
							<div>
								<input type="checkbox" name="location_status" data-init-plugin="switchery" data-size="medium" data-color="primary" />
							</div>
						</div>
					</div>

					<div class="form-group form-group-default col-md-9">
						<label>Phone</label>
						<input type="text" name="contact_phone" class="form-control" required="">
					</div>
					<div class="form-group form-group-default col-md-9">
						<label>Email</label>
						<input type="text" name="contact_email" class="form-control" required="">
					</div>
					<div class="form-group form-group-default col-md-9">
						<label>GPS Coordinates</label>
						<input type="hidden" name="contact_gps_coor">
						<input type="text" id="searchmap" class="form-control" required="">
					</div>
					<div id="map-canvas"></div>
					<div class="form-group form-group-default col-md-9">
						<label>Instagram</label>
						<input type="text" name="contact_instagram" class="form-control" required="">
					</div>
					<div class="form-group form-group-default col-md-9">
						<label>Twitter</label>
						<input type="text" name="contact_twitter" class="form-control" required="">
					</div>
					<div class="form-group form-group-default col-md-9">
						<label>Facebook</label>
						<input type="text" name="contact_facebook" class="form-control" required="">
					</div>
					<button class="btn btn-success" type="submit">Submit</button>
				</form>
			</div>
		</div>
		@else
		<div class="card-block"> 
    	 	<h3 align="center">Sorry, You don't have access permission to this page !</h3>
		</div>
		@endif
	</div>

<script type="text/javascript">
	
	 function initMap() 
	 {
	    
	 	var map = new google.maps.Map(document.getElementById('map-canvas'), {
          center: {lat: -6.175392, lng: 106.827153},
          zoom: 15
        });
        var marker = new google.maps.Marker({
        position: {
          lat:  -6.175392,
          lng: 106.827153
        },
        map: map,
        draggable: true
        });

	    var input = document.getElementById('searchmap');  
	    var autocomplete = new google.maps.places.Autocomplete(input);

	    autocomplete.bindTo('bounds', map);
	    var infowindow = new google.maps.InfoWindow();

	    marker = new google.maps.Marker({
	      map: map,
	      anchorPoint: new google.maps.Point(0, -29),
	      draggable: true
	    });

	    var infowindowContent = document.getElementById('infowindow-content');
	    infowindow.setContent(infowindowContent);
	    var marker = new google.maps.Marker({
	      map: map,
	      anchorPoint: new google.maps.Point(0, -29)
	    });

	    autocomplete.addListener('place_changed', function() {
	      infowindow.close();
	      marker.setVisible(false);
	      var place = autocomplete.getPlace();
	      if (!place.geometry) {
	        
	        window.alert("No details available for input: '" + place.name + "'");
	        return;
	      }

	      // If the place has a geometry, then present it on a map.
	      if (place.geometry.viewport) {
	        map.fitBounds(place.geometry.viewport);
	      } else {
	        map.setCenter(place.geometry.location);
	        map.setZoom(30);  // Why 17? Because it looks good.
	      }
	      marker.setPosition(place.geometry.location);
	      marker.setVisible(true);

	      var address = '';
	      if (place.address_components) {
	        address = [
	          (place.address_components[0] && place.address_components[0].short_name || ''),
	          (place.address_components[1] && place.address_components[1].short_name || ''),
	          (place.address_components[2] && place.address_components[2].short_name || '')
	        ].join(' ');
	      }
	    });

		  google.maps.event.addListener(marker,'position_changed', function(){
		  var lat = marker.getPosition().lat();
		  var lng = marker.getPosition().lng();

		  console.log(lat+" "+lng)
		  var lokasi = [];
		  lokasi.push(lat)
		  lokasi.push(lng)

		  $('[name="contact_gps_coor"]').val(lat+" "+lng);

		 });
  }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnZEfJBQQ1DTkBn4qqwXJmLTNhHds8r2o&callback=initMap&libraries=places" async defer></script>
@endsection

@push('scripts')
<script type="text/javascript">
$(document).ready(function(){
  
  $.validator.addMethod('emailcustom', function(val, elem)
      {
          
          var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

          if (val != '') 
          {
              return filter.test(val);
          } 
          else 
          {
              return true;
          }

  },'Please enter valid email address');  

	$('#formhome_create').validate({
	    ignore: [],
	    rules: {
	      banner_main_status:"required",
	      company_id:"required",
	      services_status:"required",
	      team_status:"required",
	      about_status:"required",
	      news_status:"required",
	      portfolio_status:"required",
	      location_status:"required",
	      team_status:"required",
	      
	      contact_phone: {
	        required: true,
	        number:true,
	        maxlegth:13
	      },

	      contact_email: {
	        required: true,
	        emailcustom: true
	      },
	      
	      contact_instagram:"required",
	      contact_gps_coor:"required", 
	      contact_twitter:"required",
	      contact_facebook:"required"
	    }

	});
});  
</script>
@endpush
