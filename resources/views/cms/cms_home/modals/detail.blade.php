<div class="modal fade slide-up disable-scroll" id="cms_home-detail" tabindex="-1" role="dialog" aria-labelledby="company-detail" aria-hidden="false">
    <div class="modal-dialog ">
        <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                  <i class="pg-close fs-14"></i>
                </button>
                <h5 style="padding-left: 8.5px">CMS Home Detail</h5>
            </div>
            <div class="modal-body">
                <form>
                	<div class="form-group-attached">
                		<div class="form-group row">
                			<label class="col-md-4">Company Name</label>
                			<div class="col-md-8" id="company_name"></div>
                		</div>
                        <div class="form-group row">
                            <label class="col-md-4">Banner Main Status</label>
                            <div class="col-md-8" id="banner_main_status"></div>
                        </div>
                		<div class="form-group row">
                			<label class="col-md-4">Service Status</label>
                			<div class="col-md-8" id="services_status"></div>
                		</div>
                        <div class="form-group row">
                            <label class="col-md-4">Team Status</label>
                            <div class="col-md-8" id="team_status"></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4">About Status</label>
                            <div class="col-md-8" id="about_status"></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4">News Status</label>
                            <div class="col-md-8" id="news_status"></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4">Portfolio Status</label>
                            <div class="col-md-8" id="portfolio_status"></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4">Location Status</label>
                            <div class="col-md-8" id="location_status"></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4">Phone</label>
                            <div class="col-md-8" id="contact_phone"></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4">Email</label>
                            <div class="col-md-8" id="contact_email"></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4">GPS Coor</label>
                            <div class="col-md-8" id="contact_gps_coor"></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4">Instagram</label>
                            <div class="col-md-8" id="contact_instagram"></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4">Twitter</label>
                            <div class="col-md-8" id="contact_twitter"></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4">Facebook</label>
                            <div class="col-md-8" id="contact_facebook"></div>
                        </div>
                	</div>
                </form>
            </div>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>