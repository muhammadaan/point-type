@extends('layouts.app')

@section('_title')
<title>Services | Edit</title>
@endsection

@section('content')

<?php $acl_btn = App\Models\Usergroup::where('name', \Auth::user()->roles)->where('route_access_list', 'LIKE', '%'.'cms_services.edit'.'%')->count(); ?>		 
@if($acl_btn != 0 || \Auth::user()->roles == 'superadmin')

	<div class="col-md-12">
		<div data-pages="card" class="card card-default">
			<div class="card-header">
				<div class="card-title">
					<h4>Edit Services</h4>
				</div>
				<div class="card-controls">
					<ul>
						<li>
							<a data-toggle="collapse" class="card-collapse" href="#"><i class="card-icon card-icon-collapse"></i></a>
						</li>	
					</ul>
				</div>
			</div>
			<div class="card-block">
				<button onclick="goBack()" class="btn btn-complete m-b-15">Back</button>
				<form action="{{url('/cms_services/update')}}/{{$cms_services->id}}" method="POST" enctype="multipart/form-data" id="formpoint" role="form" autocomplete="off">
				{{csrf_field()}}
				<input type="hidden" name="id" value="{{$cms_services->id}}">		
					<div class="form-group form-group-default col-md-6">
						<label>Name</label>
						<input type="text" name="name" class="form-control" required="" value="{{$cms_services->service_name}}">
					</div>
					<div class="form-group form-group-default col-md-6">
						<label>Image</label>
						<img src="{{ asset('assets/images/') }}/{{$cms_services->image}}" style="max-width:100px;max-height:100px; border-width:2px; margin-bottom: 10px;" id="prev">
						<input type="file" name="image" class="form-control" id="image">
					</div>
					<div class="form-group form-group-default col-md-6">
						<label>URL</label>
						<input type="text" name="url" class="form-control" required="" value="{{$cms_services->url}}">
					</div>
					<div class="form-group form-group-default col-md-6">
						<label>Desc</label>
						<textarea  class="form-control" style="height: 75px;" required="" name="desc">{{$cms_services->desc}}</textarea>
					</div>
					<div class="form-group form-group-default col-md-6">
						<label>Company</label>
						<select name="cms_home_id" class="form-control" data-init-plugin="select2" tabindex="-1">
							<option value="">Select Company</option>
							@foreach($cms_services1 as $show)
							<option value="{{$show->id}}">{{$show->company->company_name}}</option>
							@endforeach	
						</select>
					</div>
					<button class="btn btn-success" type="submit">Submit</button>
				</form>
			</div>
		</div>
		@else
		<div class="card-block"> 
    	 	<h3 align="center">Sorry, You don't have access permission to this page !</h3>
		</div>
		@endif
	</div>	
@endsection

@push('scripts')
	<script src="{{ asset('template/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('template/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('template/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js') }}"></script>
	<script src="{{ asset('template/assets/plugins/datatables-responsive/js/datatables.responsive.js') }}" type="text/javascript"></script>
	<script src="{{ asset('template/assets/plugins/datatables-responsive/js/lodash.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('template/assets/plugins/jquery-ui/jquery-ui_back.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('template/assets/js_validation/jquery.validate.js') }}" type="text/javascript"></script>
	<script>
		$(document).ready(function() {
	    	
			 $( "#start" ).spinner({min: 0, max: 500});
			 $( "#min" ).spinner({min: 0, max: 500});
			 $( "#max" ).spinner({min: 0, max: 500});
			 
 			 $('#formpoint').validate();			
		});
	</script>
	<script type="text/javascript">
	 function readURL(input) {
	         if (input.files && input.files[0]) {
	             var reader = new FileReader();
	             
	             reader.onload = function (e) {
	                 $('#prev').attr('src', e.target.result);
	             }
	             
	             reader.readAsDataURL(input.files[0]);
	         }
	     }
	     
	     $("#image").change(function(){
	         readURL(this);
	     }); 
	</script>  

@endpush