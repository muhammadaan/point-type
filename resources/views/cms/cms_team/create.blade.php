@extends('layouts.app')

@section('_title')
<title>Team | Add</title>
@endsection

@section('content')

<?php $acl_btn = App\Models\Usergroup::where('name', \Auth::user()->roles)->where('route_access_list', 'LIKE', '%'.'cms_team.create'.'%')->count(); ?>		 
@if($acl_btn != 0 || \Auth::user()->roles == 'superadmin')

	<div class="col-md-12">
		<div data-pages="card" class="card card-default">
			<div class="card-header">
				<div class="card-title">
					<h4>Add Team</h4>
				</div>
				<div class="card-controls">
					<ul>
						<li>
							<a data-toggle="collapse" class="card-collapse" href="#"><i class="card-icon card-icon-collapse"></i></a>
						</li>	
					</ul>
				</div>
			</div>
			<div class="card-block">
				<button onclick="goBack()" class="btn btn-complete m-b-15">Back</button>
				<form action="{{ url('/cms_team/insert') }}" method="POST" enctype="multipart/form-data" id="formcmsteam" role="form" autocomplete="off">
				{{csrf_field()}}

					<div class="form-group form-group-default col-md-6">
						<label>Name</label>
						<input type="text" name="name" class="form-control" required="">
					</div>
					<div class="form-group form-group-default col-md-6">
						<label>Image</label>
						<input type="file" name="image" class="form-control" required="">
					</div>
					<div class="form-group form-group-default col-md-6">
						<label>URL</label>
						<input type="text" name="url" class="form-control" required="">
					</div>
					<div class="form-group form-group-default col-md-6">
						<label>Position</label>
						<input type="text" name="position" class="form-control" required="">
					</div>
					<div class="form-group form-group-default col-md-6">
						<label>Desc</label>
						<textarea  class="form-control" style="height: 75px;" required="" name="desc"></textarea>
					</div>
					<div class="form-group form-group-default col-md-6">
						<label>Team Desc</label>
						<textarea  class="form-control" style="height: 75px;" required="" name="team_desc"></textarea>
					</div>
					<div class="form-group form-group-default col-md-6">
						<label>Company</label>
						<select name="cms_home_id" class="form-control" data-init-plugin="select2" tabindex="-1">
							<option value="">Select Company</option>
							@foreach($cms_team as $show)
							<option value="{{$show->id}}">{{$show->company->company_name}}</option>
							@endforeach	
						</select>
					</div>	
					<button class="btn btn-success" type="submit">Submit</button>
				</form>
			</div>
		</div>
		@else
		<div class="card-block"> 
    	 	<h3 align="center">Sorry, You don't have access permission to this page !</h3>
		</div>
		@endif
	</div>	
@endsection

@push('scripts')
	<script type="text/javascript">
		$(document).ready(function() {
			 
 			 $('#formcmsteam').validate();			
		});
	</script>
@endpush