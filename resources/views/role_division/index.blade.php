@extends('layouts.app')

@section('_title')
<title>Role | List</title>
@endsection

@push('styles')
	<link type="text/css" rel="stylesheet" href="{{ asset('template/assets/plugins/jquery-datatable/media/css/jquery.dataTables.css') }}">
	<link type="text/css" rel="stylesheet" href="{{ asset('template/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css') }}">
	<link media="screen" type="text/css" rel="stylesheet" href="{{ asset('template/assets/plugins/datatables-responsive/css/datatables.responsive.css') }}">
@endpush

@section('content')
	<div class="col-md-12">
		<div data-pages="card" class="card card-default">
			<div class="card-header">
				<div class="card-title">
					<h4>Role List</h4>
				</div>
				<div class="card-controls">
					<ul>
						<li>
							<a data-toggle="collapse" class="card-collapse" href="#"><i class="card-icon card-icon-collapse"></i></a>
						</li>	
					</ul>
				</div>
			@if (session('message'))
			    <div class="alert alert-success" role="alert">
			    	<button class="close" data-dismiss="alert"></button>
			        {{ session('message') }}
			    </div>
			@endif	
			</div>
			<div class="card-block">
				<button onclick="goBack()" class="btn btn-complete m-b-15">Back</button>
				<button onclick="relocateAddRole()" class="btn btn-primary m-b-15">Add Role</button>
				<table id="memberTable" class="table table-hover" cellspacing="0" width="100%">
				    <thead>
				        <tr>
				            <th>Role Code</th>
				            <th>Role Name</th>
				            <th>Created By</th>
				            <th>Action</th>
				        </tr>
				    </thead>
				    <tfoot style="display: table-header-group;">
				        <tr>
				            <th>Role Code</th>
				            <th>Role Name</th>
				            <th>Created By</th>
				            <th>Action</th>
				        </tr>
				    </tfoot>
				    <tbody>
				    	@foreach($role as $show)
				        <tr>
				            <td>{{ $show->role_code }}</td>
				            <td>{{ $show->role_name }}</td>
				            <td>{{ $show->created_by }}</td> 
				            <td>
				    			<a href="{{url('/role-division/edit')}}/{{$show->id}}" title="Edit"><i class="fa fa-edit"></i></a>
			                    <a href="{{url('/role-division/delete')}}/{{$show->id}}" onclick="return ConfirmDelete()" title="Delete"><i class="pg-trash"></i></a>
			               	</td>
				        </tr>
				        @endforeach 
				    </tbody>
				</table>		
			</div>
		</div>
	</div>
@endsection

@push('scripts')
	<script src="{{ asset('template/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('template/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('template/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js') }}"></script>
	<script src="{{ asset('template/assets/plugins/datatables-responsive/js/datatables.responsive.js') }}" type="text/javascript"></script>
	<script src="{{ asset('template/assets/plugins/datatables-responsive/js/lodash.min.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		function ConfirmDelete()
		{
		  var x = confirm("Are you sure you want to delete ?");
		  
		  if(x)
		  	return true;
		  else
		    return false;
			
		}   
	</script>
	<script>
		$(document).ready(function() {
		    // Setup - add a text input to each footer cell
		    $('#memberTable tfoot th').each( function () {
		        var title = $(this).text();
		        $(this).html( '<input type="text" />' );
		    } );

		    var table = $('#memberTable').DataTable();

		    // Apply the search
		    table.columns().every( function () {
		        var that = this;
		 
		        $( 'input', this.footer() ).on( 'keyup change', function () {
		            if ( that.search() !== this.value ) {
		                that
		                    .search( this.value )
		                    .draw();
		            }
		        } );
		    } );
		} );

		function relocateAddRole() {
			window.location.href = "{{ route('role_division.create') }}"
		}

	</script>
@endpush