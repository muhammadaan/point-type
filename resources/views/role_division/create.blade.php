@extends('layouts.app')

@section('_title')
<title>Role | Add</title>
@endsection

@section('content')
	<div class="col-md-12">
		<div data-pages="card" class="card card-default">
			<div class="card-header">
				<div class="card-title">
				  <h4>Add Role Division</h4>
				</div>
				<div class="card-controls">
					<ul>
						<li>
							<a data-toggle="collapse" class="card-collapse" href="#"><i class="card-icon card-icon-collapse"></i></a>
						</li>	
					</ul>
				</div>
			</div>
			<div class="card-block">
				<button onclick="goBack()" class="btn btn-complete m-b-15">Back</button>
				<form action="{{ route('role_division.insert') }}" method="POST" enctype="multipart/form-data" id="form1" role="form" autocomplete="off">
				{{ csrf_field() }}
					<div class="form-group form-group-default">
						<label>Role Code</label>
						<input type="text" name="role_code" class="form-control" required="">
					</div>
					<div class="form-group form-group-default">
						<label>Role Name</label>
						<input type="text" name="role_name" class="form-control" required="">
					</div>
					<button class="btn btn-success" type="submit">Submit</button>
				</form>
			</div>
		</div>
	</div>
@endsection