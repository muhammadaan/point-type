@extends('layouts.app')

@section('_title')
<title>Reward | Add</title>
@endsection

@section('content')

<?php $acl_btn = App\Models\Usergroup::where('name', \Auth::user()->roles)->where('route_access_list', 'LIKE', '%'.'reward.create'.'%')->count(); ?>		 
@if($acl_btn != 0 || \Auth::user()->roles == 'superadmin')

	<div class="col-md-12">
		<div data-pages="card" class="card card-default">
			<div class="card-header">
				<div class="card-title">
					<h4>Add Reward</h4>
				</div>
				<div class="card-controls">
					<ul>
						<li>
							<a data-toggle="collapse" class="card-collapse" href="#"><i class="card-icon card-icon-collapse"></i></a>
						</li>	
					</ul>
				</div>
			</div>
			<div class="card-block">
				<button onclick="goBack()" class="btn btn-complete m-b-15">Back</button>
				<form action="{{ url('/reward/insert') }}" method="POST" enctype="multipart/form-data" id="form1" role="form" autocomplete="off">
					{{ csrf_field() }}
					<div class="form-group form-group-default col-md-6">
						<label>Reward Name</label>
						<input type="text" name="reward_name" class="form-control" required="">
					</div>
					<div class="form-group form-group-default col-md-6">
						<label>Point for Redeem</label>
						<input type="text" name="point_required" class="col-lg-11" required="" id="point_required">
					</div>
					<div class="form-group form-group-default col-md-6">
						<label>Quantity Available</label>
						<input type="text" name="quantity" class="col-lg-11" required="" id="quantity">
					</div>
					<div class="form-group form-group-default col-md-6">
		                <label>Description</label>
		                <textarea class="form-control" name="description" style="height: 100px; width: 300px;"></textarea>
	              	</div>
	              	<div class="form-group form-group-default col-md-6">
		                <label for="file">Choose file to upload</label>
		                <input type="file" id="file" name="reward_image" multiple>
		             </div>
					<button class="btn btn-success" type="submit">Submit</button>
				</form>
			</div>
		</div>
		@else
		<div class="card-block"> 
    	 	<h3 align="center">Sorry, You don't have access permission to this page !</h3>
		</div>
		@endif
	</div>
@endsection
@push('scripts')
	<script src="{{ asset('template/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('template/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('template/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js') }}"></script>
	<script src="{{ asset('template/assets/plugins/datatables-responsive/js/datatables.responsive.js') }}" type="text/javascript"></script>
	<script src="{{ asset('template/assets/plugins/datatables-responsive/js/lodash.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('template/assets/plugins/jquery-ui/jquery-ui_back.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('template/assets/js_validation/jquery.validate.js') }}" type="text/javascript"></script>
	<script>
		$(document).ready(function() {
	    	
			 $( "#start" ).spinner({min: 0, max: 500});
			 $( "#point_required" ).spinner({min: 0, max: 500});
			 $( "#quantity" ).spinner({min: 0, max: 500});
			 
 			 $('#formpoint').validate();			
		});
	</script>
@endpush