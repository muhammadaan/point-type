@extends('layouts.app')

@section('_title')
<title>Reward | List</title>
@endsection

@push('styles')
	<link type="text/css" rel="stylesheet" href="{{ asset('template/assets/plugins/jquery-datatable/media/css/jquery.dataTables.css') }}">
	<link type="text/css" rel="stylesheet" href="{{ asset('template/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css') }}">
	<link media="screen" type="text/css" rel="stylesheet" href="{{ asset('template/assets/plugins/datatables-responsive/css/datatables.responsive.css') }}">
	<style>
		.form-group-attached {
			padding-left: 15px
		}
	</style>
@endpush

@section('content')

<?php $acl_btn = App\Models\Usergroup::where('name', \Auth::user()->roles)->where('route_access_list', 'LIKE', '%'.'reward.index'.'%')->count(); ?>		 
@if($acl_btn != 0 || \Auth::user()->roles == 'superadmin')

	<div class="col-md-12">

		<div data-pages="card" class="card card-default">
			<div class="card-header">
				<div class="card-title">
					<h4>Reward List</h4>
				</div>
				<div class="card-controls">
					<ul>
						<li>
							<a data-toggle="collapse" class="card-collapse" href="#"><i class="card-icon card-icon-collapse"></i></a>
						</li>	
					</ul>
				</div>
				@if (session('message'))
				    <div class="alert alert-success" role="alert">
				    	<button class="close" data-dismiss="alert"></button>
				        {{ session('message') }}
				    </div>
				@endif
			</div>
			<div class="card-block">
				<!-- <button class="btn btn-complete m-b-15" onclick="goBack()">Back</button> -->
				<button class="btn btn-primary m-b-15" onclick="relocateAddreward()">Add reward</button>
				<table id="rewardTable" class="table table-hover" cellspacing="0" width="100%">
				    <thead>
				        <tr>
				            <th>Reward Name</th>
				            <th>Point for Redeem</th>
				            <th>Quantity Available</th>
				            <th>Image</th>
				            <th>Action</th>
				        </tr>
				    </thead>
				    <tfoot style="display: table-header-group;">
				        <tr>
				            <th>Reward Name</th>
				            <th>Point for Redeem</th>
				            <th>Quantity Available</th>
				            <th>Image</th>
				            <th>Action</th>
				        </tr>
				    </tfoot>
				    <tbody>
				    @foreach($reward as $show)
				    	<tr>
				    		<td>{{ $show->reward_name }}</td>
				    		<td>{{ $show->point_required }}</td>
				    		<td>{{ $show->quantity }}</td>
				    		<td><img src="{{ asset('assets/images/') }}/{{$show->reward_image}}" style="max-width:100px;max-height:100px;float:left;"></td>
				    		<td align="center">
				    			<a href="{{url('/reward/edit')}}/{{$show->id}}" title="Edit"><i class="fa fa-edit"></i></a>
			                    <a href="{{url('/reward/delete')}}/{{$show->id}}" onclick="return ConfirmDelete()" title="Delete"><i class="pg-trash"></i></a>
			                    <!-- <a onclick="showModal({{ $show->id }})" href="#" data-toggle="modal" title="Show Detail"><i class="fa fa-search"></i></a> -->
			               	</td>
			            </tr>
				    @endforeach
				    </tbody>
				</table>		
			</div>
		</div>
		@else
		<div class="card-block"> 
    	 	<h3 align="center">Sorry, You don't have access permission to this page !</h3>
		</div>
		@endif
	</div>
@endsection

@push('scripts')
	<script src="{{ asset('template/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('template/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('template/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js') }}"></script>
	<script src="{{ asset('template/assets/plugins/datatables-responsive/js/datatables.responsive.js') }}" type="text/javascript"></script>
	<script src="{{ asset('template/assets/plugins/datatables-responsive/js/lodash.min.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		function ConfirmDelete()
		{
		  var x = confirm("Benarkah anda ingin menghapus data ini?");
		  
		  if(x)
		  	return true;
		  else
		    return false;
			
		}   
	</script>
	<script>
		$(document).ready(function() {
		    // Setup - add a text input to each footer cell
		    $('#rewardTable tfoot th').each( function () {
		        var title = $(this).text();
		        $(this).html( '<input type="text" />' );
		    } );

		    var table = $('#rewardTable').DataTable();

		    // Apply the search
		    table.columns().every( function () {
		        var that = this;
		 
		        $( 'input', this.footer() ).on( 'keyup change', function () {
		            if ( that.search() !== this.value ) {
		                that
		                    .search( this.value )
		                    .draw();
		            }
		        } );
		    } );
		} );
		
		function relocateAddreward() {
			window.location.href = "{{ route('reward.create') }}";
		}
	</script>
@endpush