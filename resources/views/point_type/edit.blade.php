@extends('layouts.app')

@section('_title')
<title>Point Type | Edit</title>
@endsection

@section('content')
<?php $acl_btn = App\Models\Usergroup::where('name', \Auth::user()->roles)->where('route_access_list', 'LIKE', '%'.'point_type.edit'.'%')->count(); ?>		 
@if($acl_btn != 0 || \Auth::user()->roles == 'superadmin')

	<div class="col-md-12">
		<div data-pages="card" class="card card-default">
			<div class="card-header">
				<div class="card-title">
					<h4>Edit Point Type</h4>
				</div>
				<div class="card-controls">
					<ul>
						<li>
							<a data-toggle="collapse" class="card-collapse" href="#"><i class="card-icon card-icon-collapse"></i></a>
						</li>	
					</ul>
				</div>
			</div>
			<div class="card-block">
				<button onclick="goBack()" class="btn btn-complete m-b-15">Back</button>
				<form action="{{url('/point_type/update')}}/{{$point_type->id}}" method="POST" enctype="multipart/form-data" id="form1" role="form" autocomplete="off">
					{{ csrf_field() }}
					<div class="form-group form-group-default">
						<label>Point Type</label>
						<input type="text" name="point_type" class="form-control" required="" value="{{ $point_type->point_type }}">
					</div>
					<div class="form-group form-group-default">
						<label>Description</label>
						<input type="text" name="description" class="form-control" required="" value="{{ $point_type->description }}">
					</div>
					<button class="btn btn-success" type="submit">Update</button>
				</form>
			</div>
		</div>
		@else
		<div class="card-block"> 
    	 	<h3 align="center">Sorry, You don't have access permission to this page !</h3>
		</div>
		@endif
	</div>
@endsection