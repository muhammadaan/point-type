@extends('layouts.app')

@section('_title')
<title>Dashboard</title>
@endsection


@section('content')
<style>
    .liquidFillGaugeText { font-family: Helvetica; font-weight: bold; font-size: 18px; }
    #ket-percent{
      width: 100%;
    }
    #ket-percent h4{
      text-align: center!important;
      font-size: 15px;
      color: #0088cc;
      font-weight: bold;
    }
    .sm-gutter .row > [class^="col-"], .sm-gutter .row > [class*="col-"] {
    padding-left: none;
    }
    html, body, #container {
        width: 100%;
        height: 50%;
        margin: 0;
        padding: 0;
    }
</style>
<script src="https://cdn.anychart.com/releases/8.1.0/js/anychart-base.min.js" type="text/javascript"></script>
<script type="text/javascript">
    anychart.onDocumentReady(function () {
    // create data set on our data
    var dataSet = anychart.data.set(getData());

    // map data for the first series, take x from the zero column and value from the first column of data set
    var seriesData_1 = dataSet.mapAs({'x': 0, 'value': 1});

    // create line chart
    var chart = anychart.line();

    // turn on chart animation
    chart.animation(true);

    // set chart padding
    chart.padding([10, 20, 5, 20]);

    // turn on the crosshair
    chart.crosshair()
            .enabled(true)
            .yLabel(false)
            .yStroke(null);

    // set tooltip mode to point
    chart.tooltip().positionMode('point');

    // set chart title text settings
    // chart.title('Trend of Sales of the Most Popular Products of ACME Corp.');

    // set yAxis title
    chart.yAxis().title('Number of Bottles Sold (thousands)');
    chart.xAxis().labels().padding(5);

    // create first series with mapped data
    var series_1 = chart.line(seriesData_1);
    series_1.hovered().markers()
            .enabled(true)
            .type('circle')
            .size(4);
    series_1.tooltip()
            .position('right')
            .anchor('left-center')
            .offsetX(5)
            .offsetY(5);

    // turn the legend on
    // chart.legend()
    //         .enabled(true)
    //         .fontSize(13)
    //         .padding([0, 0, 10, 0]);

    // set container id for the chart and set up paddings
    chart.container('container');

    // initiate chart drawing
    chart.draw();
});

function getData() {
    return [
        ['January', 1000000],
        ['February', 2000000],
        ['March', 3000000],
        ['April', 1000000],
        ['May', 4000000],
        ['June', 2000000]
    ]
}
</script>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

      // Load Charts and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Draw the pie chart for Sarah's pizza when Charts is loaded.
      google.charts.setOnLoadCallback(drawSarahChart);

      // Draw the pie chart for the Anthony's pizza when Charts is loaded.
      google.charts.setOnLoadCallback(drawAnthonyChart);

      // Callback that draws the pie chart for Sarah's pizza.
      function drawSarahChart() {

        // Create the data table for Sarah's pizza.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['Monday', 7000000],
          ['Tuesday', 8000000],
          ['Wednesday', 6000000],
          ['Thursday', 3000000],
          ['Friday', 900000]
        ]);

        // Set options for Sarah's pie chart.
        var options = {title:'Year To Date Week',
                       width:400,
                       height:300};

        // Instantiate and draw the chart for Sarah's pizza.
        var chart = new google.visualization.PieChart(document.getElementById('Sarah_chart_div'));
        chart.draw(data, options);
      }

      // Callback that draws the pie chart for Anthony's pizza.
      function drawAnthonyChart() {

        // Create the data table for Anthony's pizza.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['Indomie', 20],
          ['Club', 50],
          ['Bimoli', 30],
          ['Indomilk', 35],
        ]);

        // Set options for Anthony's pie chart.
        var options = {title:'Top 5 Sales Product',
                       width:400,
                       height:300};

        // Instantiate and draw the chart for Anthony's pizza.
        var chart = new google.visualization.PieChart(document.getElementById('Anthony_chart_div'));
        chart.draw(data, options);
      }
    </script>
<script language="JavaScript">
    var config4 = liquidFillGaugeDefaultSettings();
    config4.circleThickness = 0.15;
    config4.textSize = 0.75;
    config4.textVertPosition = 0.8;
    config4.waveAnimateTime = 1000;
    config4.waveHeight = 0.05;
    config4.waveAnimate = true;
    config4.waveRise = false;
    config4.waveHeightScaling = false;
    config4.waveOffset = 0.25;
    config4.waveCount = 3;
    var gauge5 = loadLiquidFillGauge("fillgauge5", 60.44, config4);
    var config5 = liquidFillGaugeDefaultSettings();
    config5.circleThickness = 0.15;
    config5.textSize = 0.75;
    config5.textVertPosition = 0.8;
    config5.waveAnimateTime = 1000;
    config5.waveHeight = 0.05;
    config5.waveAnimate = true;
    config5.waveRise = false;
    config5.waveHeightScaling = false;
    config5.waveOffset = 0.25;
    config5.waveCount = 3;
    var gauge6 = loadLiquidFillGauge("fillgauge6", 31.44, config5);
    function NewValue(){
        if(Math.random() > .5){
            return Math.round(Math.random()*100);
        } else {
            return (Math.random()*100).toFixed(1);
        }
    }

    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawBasic);

    function drawBasic() {

      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Month in Year');
      data.addColumn('number', 'Amount');

      data.addRows([
        [{v: 'January', f: 'January'}, 7500000],
        [{v: 'February', f: 'February'}, 2500000],
        [{v:'March', f:'March'}, 12500000],
        [{v: 'April', f: 'April'}, 10000000],
        [{v: 'May', f: 'May'}, 5000000],
        [{v: 'June', f: 'June'}, 3000000],
        [{v: 'July', f: 'July'}, 8500000],
        [{v: 'August', f: 'August'}, 3500000],
        [{v: 'October', f: 'October'}, 8000000],
        [{v: 'November', f: 'November'}, 5500000],
        [{v: 'December', f: 'December'}, 7500000],
      ]);

      var options = {
        // title: 'Event Sales Amount',
        hAxis: {
          title: 'Month in Year'
        },
        vAxis: {
          title: 'Amount (IDR)'
        },
        legend: { position: "none" }
      };

      var chart = new google.visualization.ColumnChart(
        document.getElementById('chart_div'));

      chart.draw(data, options);
    }
</script>

<!-- START PAGE CONTENT -->
<div class="page-content-wrapper">
    <div class=" container-fluid container-fixed-lg">
      <!-- <div class="card card-default"> -->
        <div>
        <div class="">
          @if (session('status'))
            <div class="alert alert-success">
              <strong>{{ session('status') }}.</strong>
              <button type="button" class="close" data-dismiss="alert">×</button>  
            </div>
          @endif
          <div class="card-title">
            <h2 style="margin-left: 15px;"><strong>Dashboard</strong></h2>
          </div>


          <div class="row">
              <div class="col-lg-7 col-xlg-5">
                <div class="row">
                  <div class="col-md-12 m-b-10" style="display: flex">
                    <!-- <div class="gallery-item " data-width="1" data-height="1" style="background-color: #80deea; height:230px; width:200px; margin-left: 7px; margin-top: 20px;">
                        <img src="{{ asset('public/img/rev.png') }}" alt="" class="image-responsive-height" style="margin-top: 20px">
                        <div class="overlayer bottom-left full-width">
                            <div class="gradient-grey p-l-20 p-r-20 p-t-20 p-b-5">
                              <p class="pull-left bold text-white p-t-10" style="font-size: 15pt;">Revenue</p><br>
                              <h5 class="pull-right semi-bold text-white font-montserrat bold">80</h5>
                              <div class="clearfix"></div>
                            </div>
                        </div>
                    </div> -->
                    <div class="gallery-item " data-width="1" data-height="1" style="background-color: #ef9a9a; height:230px; width:230px;">
                        <img src="{{ asset('template/img/store.png') }}" alt="" class="image-responsive-height" style="width: 200px; margin: 10px;">
                        <div class="overlayer bottom-left full-width">
                            <div class="gradient-grey p-l-20 p-r-20 p-t-20 p-b-5">
                              <p class="pull-left bold text-white p-t-10" style="font-size: 15pt;">Store Partner</p><br>
                              <h5 class="pull-right semi-bold text-white font-montserrat bold">0</h5>
                              <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div onclick="location.href = '#' "&nbsp class="gallery-item" data-width="1" data-height="1" style="background-color: #e1bee7; height:230px; width:230px; margin-left: 7px;">
                        <img src="{{ asset('public/img/box.png') }}" alt="" class="image-responsive-height" style="width: 200px; margin: 10px;">
                        <div class="overlayer bottom-left full-width">
                            <div class="gradient-grey p-l-20 p-r-20 p-t-20 p-b-5">
                              <p class="pull-left bold text-white p-t-10" style="font-size: 15pt;">Total Product</p><br>
                              <h5 class="pull-right semi-bold text-white font-montserrat bold">10</h5>
                              <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="gallery-item " data-width="1" data-height="1" style="background-color: #82b1ff; height:230px; width:230px; margin-left: 7px;">
                        <img src="{{ asset('public/img/list.png') }}" alt="" class="image-responsive-height" style="width: 200px; margin: 10px;">
                        <div class="overlayer bottom-left full-width">
                            <div class="gradient-grey p-l-20 p-r-20 p-t-20 p-b-5">
                              <p class="pull-left bold text-white p-t-10" style="font-size: 15pt;">Total Transaction</p><br>
                              <h5 class="pull-right semi-bold text-white font-montserrat bold">10</h5>
                              <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-lg-5 col-xlg-4">
                <div class="row">
                  <div class="card">
                    <div class="row">
                      <div class="col-md-12" style="">
                        <h4><center><strong>Payment Method Percentage</strong></center></h4>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <svg id="fillgauge5" width="19%" onclick="gauge5.update(NewValue());"></svg>
                      </div>
                      <div class="col-md-6">
                        <svg id="fillgauge6" width="19%" onclick="gauge6.update(NewValue());"></svg>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6 text-center">
                        <p><strong>Bank Transfer</strong></p>
                      </div>
                      <div class="col-md-6 text-center">
                        <p><strong>Others</strong></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Filler -->
              <div class="visible-xlg col-xlg-3">
                <div class="ar-2-3">
                  <!-- START WIDGET widget_tableWidget-->
                  <div class="widget-11 card no-border  no-margin widget-loader-bar">
                    <div class="card-header  ">
                      <div class="card-title">Today's Table
                      </div>
                      <div class="card-controls">
                        <ul>
                          <li><a data-toggle="refresh" class="card-refresh text-black" href="#"><i
              					class="card-icon card-icon-refresh"></i></a>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div class="p-l-25 p-r-25 p-b-20">
                      <div class="pull-left">
                        <h2 class="text-success no-margin">webarch</h2>
                      </div>
                      <h3 class="pull-right semi-bold"><sup>
			        <small class="semi-bold">$</small>
			      </sup> 102,967
			      </h3>
                      <div class="clearfix"></div>
                    </div>
                    <div class="widget-11-table auto-overflow">
                      <table class="table table-condensed table-hover">
                        <tbody>
                          <tr>
                            <td class="font-montserrat all-caps fs-12">Purchase CODE #2345</td>
                            <td class="text-right">
                              <span class="hint-text small">dewdrops</span>
                            </td>
                            <td class="text-right b-r b-dashed b-grey">
                              <span class="hint-text small">Qty 1</span>
                            </td>
                            <td>
                              <span class="font-montserrat fs-18">$27</span>
                            </td>
                          </tr>
                          <tr>
                            <td class="font-montserrat all-caps fs-12">Purchase CODE #2345</td>
                            <td class="text-right">
                              <span class="hint-text small">johnsmith</span>
                            </td>
                            <td class="text-right b-r b-dashed b-grey">
                              <span class="hint-text small">Qty 1</span>
                            </td>
                            <td>
                              <span class="font-montserrat fs-18 text-primary">$1000</span>
                            </td>
                          </tr>
                          <tr>
                            <td class="font-montserrat all-caps fs-12">Purchase CODE #2345</td>
                            <td class="text-right">
                              <span class="hint-text small">janedrooler</span>
                            </td>
                            <td class="text-right b-r b-dashed b-grey">
                              <span class="hint-text small">Qty 1</span>
                            </td>
                            <td>
                              <span class="font-montserrat fs-18">$27</span>
                            </td>
                          </tr>
                          <tr>
                            <td class="font-montserrat all-caps fs-12">Purchase CODE #2345</td>
                            <td class="text-right">
                              <span class="hint-text small">johnsmith</span>
                            </td>
                            <td class="text-right b-r b-dashed b-grey">
                              <span class="hint-text small">Qty 1</span>
                            </td>
                            <td>
                              <span class="font-montserrat fs-18 text-primary">$1000</span>
                            </td>
                          </tr>
                          <tr>
                            <td class="font-montserrat all-caps fs-12">Purchase CODE #2345</td>
                            <td class="text-right">
                              <span class="hint-text small">dewdrops</span>
                            </td>
                            <td class="text-right b-r b-dashed b-grey">
                              <span class="hint-text small">Qty 1</span>
                            </td>
                            <td>
                              <span class="font-montserrat fs-18">$27</span>
                            </td>
                          </tr>
                          <tr>
                            <td class="font-montserrat all-caps fs-12">Purchase CODE #2345</td>
                            <td class="text-right">
                              <span class="hint-text small">johnsmith</span>
                            </td>
                            <td class="text-right b-r b-dashed b-grey">
                              <span class="hint-text small">Qty 1</span>
                            </td>
                            <td>
                              <span class="font-montserrat fs-18 text-primary">$1000</span>
                            </td>
                          </tr>
                          <tr>
                            <td class="font-montserrat all-caps fs-12">Purchase CODE #2345</td>
                            <td class="text-right">
                              <span class="hint-text small">dewdrops</span>
                            </td>
                            <td class="text-right b-r b-dashed b-grey">
                              <span class="hint-text small">Qty 1</span>
                            </td>
                            <td>
                              <span class="font-montserrat fs-18">$27</span>
                            </td>
                          </tr>
                          <tr>
                            <td class="font-montserrat all-caps fs-12">Purchase CODE #2345</td>
                            <td class="text-right">
                              <span class="hint-text small">johnsmith</span>
                            </td>
                            <td class="text-right b-r b-dashed b-grey">
                              <span class="hint-text small">Qty 1</span>
                            </td>
                            <td>
                              <span class="font-montserrat fs-18 text-primary">$1000</span>
                            </td>
                          </tr>
                          <tr>
                            <td class="font-montserrat all-caps fs-12">Purchase CODE #2345</td>
                            <td class="text-right">
                              <span class="hint-text small">dewdrops</span>
                            </td>
                            <td class="text-right b-r b-dashed b-grey">
                              <span class="hint-text small">Qty 1</span>
                            </td>
                            <td>
                              <span class="font-montserrat fs-18">$27</span>
                            </td>
                          </tr>
                          <tr>
                            <td class="font-montserrat all-caps fs-12">Purchase CODE #2345</td>
                            <td class="text-right">
                              <span class="hint-text small">johnsmith</span>
                            </td>
                            <td class="text-right b-r b-dashed b-grey">
                              <span class="hint-text small">Qty 1</span>
                            </td>
                            <td>
                              <span class="font-montserrat fs-18 text-primary">$1000</span>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="padding-25">
                      <p class="small no-margin">
                        <a href="#"><i class="fa fs-16 fa-arrow-circle-o-down text-success m-r-10"></i></a>
                        <span class="hint-text ">Show more details of APPLE . INC</span>
                      </p>
                    </div>
                  </div>
                  <!-- END WIDGET -->
                </div>
              </div>
            </div>
          <div class="row">
            <!-- <div class="col-lg-8">
              <div class="card-block">
                <div data-pages="card" class="card card-default" id="card-basic">
                  <div class="card-controls">
                    <div class="row">
                      <div class="col-md-12">
                        <h4><strong>Monthly Sales Amount</strong></h4>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div id="chart_div"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div> -->
            <div class="col-lg-7 col-xl-7 col-xlg-7 m-b-10">
                <div class="row">
                  <div class="col-md-12">
                    <!-- START WIDGET D3 widget_graphWidget-->
                    <div class="widget-12 card widget-loader-circle no-margin">
                      <div class="row">
                        <div class="col-xlg-8 ">
                          <div class="card-header  pull-up top-right ">
                            <div class="card-controls">
                              <ul>
                                <li class="hidden-xlg">
                                  <div class="dropdown">
                                    <a data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
                                      <i class="card-icon card-icon-settings"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                      <li><a href="#">AAPL</a>
                                      </li>
                                      <li><a href="#">YHOO</a>
                                      </li>
                                      <li><a href="#">GOOG</a>
                                      </li>
                                    </ul>
                                  </div>
                                </li>
                                <li>
                                  <a data-toggle="refresh" class="card-refresh text-black" href="#"><i class="card-icon card-icon-refresh"></i></a>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="card-block">
                        <div class="row">
                          <div class="col-xlg-8 col-lg-12">
                            <div class="p-l-5">
                              <h2 class="pull-left m-t-5 m-b-5">Apple Inc.</h2>
                              <h2 class="pull-left m-l-50 m-t-5 m-b-5 text-danger">
                                <span class="">448.97</span>
                                <span class="text-danger fs-12">-318.24</span>
                              </h2>
                              <div class="clearfix"></div>

                              <div id="container" style="height: 320px;"></div>

                              <!-- <span class="dynamicsparkline">Loading..</span> -->
                              <!-- <div class="nvd3-line line-chart text-center" data-x-grid="false">
                                <svg></svg>
                              </div> -->
                            </div>
                          </div>
                          <div class="col-xlg-4 visible-xlg">
                            <div class="widget-12-search">
                              <p class="pull-left">Company
                                <span class="bold">List</span>
                              </p>
                              <button class="btn btn-default btn-xs pull-right">
                                <span class="bold">+</span>
                              </button>
                              <div class="clearfix"></div>
                              <input type="text" placeholder="Search list" class="form-control m-t-5">
                            </div>
                            <div class="company-stat-boxes">
                              <div data-index="0" class="company-stat-box m-t-15 active padding-20 bg-master-lightest">
                                <div>
                                  <button type="button" class="close" data-dismiss="modal">
                                    <i class="pg-close fs-12"></i>
                                  </button>
                                  <p class="company-name pull-left text-uppercase bold no-margin">
                                    <span class="fa fa-circle text-success fs-11"></span> AAPL
                                  </p>
                                  <small class="hint-text m-l-10">Yahoo Inc.</small>
                                  <div class="clearfix"></div>
                                </div>
                                <div class="m-t-10">
                                  <p class="pull-left small hint-text no-margin p-t-5">9:42AM ET</p>
                                  <div class="pull-right">
                                    <p class="small hint-text no-margin inline">37.73</p>
                                    <span class=" label label-important p-t-5 m-l-5 p-b-5 inline fs-12">+ 0.09</span>
                                  </div>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                              <div data-index="1" class="company-stat-box m-t-15  padding-20 bg-master-lightest">
                                <div>
                                  <button type="button" class="close" data-dismiss="modal">
                                    <i class="pg-close fs-12"></i>
                                  </button>
                                  <p class="company-name pull-left text-uppercase bold no-margin">
                                    <span class="fa fa-circle text-primary fs-11"></span> YHOO
                                  </p>
                                  <small class="hint-text m-l-10">Yahoo Inc.</small>
                                  <div class="clearfix"></div>
                                </div>
                                <div class="m-t-10">
                                  <p class="pull-left small hint-text no-margin p-t-5">9:42AM ET</p>
                                  <div class="pull-right">
                                    <p class="small hint-text no-margin inline">37.73</p>
                                    <span class=" label label-success p-t-5 m-l-5 p-b-5 inline fs-12">+ 0.09</span>
                                  </div>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                              <div data-index="2" class="company-stat-box m-t-15  padding-20 bg-master-lightest">
                                <div>
                                  <button type="button" class="close" data-dismiss="modal">
                                    <i class="pg-close fs-12"></i>
                                  </button>
                                  <p class="company-name pull-left text-uppercase bold no-margin">
                                    <span class="fa fa-circle text-complete fs-11"></span> GOOG
                                  </p>
                                  <small class="hint-text m-l-10">Yahoo Inc.</small>
                                  <div class="clearfix"></div>
                                </div>
                                <div class="m-t-10">
                                  <p class="pull-left small hint-text no-margin p-t-5">9:42AM ET</p>
                                  <div class="pull-right">
                                    <p class="small hint-text no-margin inline">37.73</p>
                                    <span class=" label label-success p-t-5 m-l-5 p-b-5 inline fs-12">+ 0.09</span>
                                  </div>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- END WIDGET -->
                  </div>
                </div>
              </div>

          <div class="col-lg-5 col-xl-5 m-b-5 hidden-xlg">
            <!-- START WIDGET widget_tableWidgetBasic-->
            <div class="widget-11-2 card card-condensed no-margin widget-loader-circle full-height d-flex flex-column">
              <div class="card-header top-right">
                <div class="card-controls">
                  <ul>
                    <li><a data-toggle="refresh" class="card-refresh text-black" href="#"><i
                        class="card-icon card-icon-refresh"></i></a>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="padding-25">
                <div class="pull-left">
                  <h2 class="text-success no-margin">Top Sales Products</h2>
                  <!-- <p class="no-margin">Today's sales</p> -->
                </div>
                <h3 class="pull-right semi-bold"><sup>
                  <small class="semi-bold">$</small>
                </sup> 102,967
                </h3>
                <div class="clearfix"></div>
              </div>
              <div class="auto-overflow widget-11-2-table">
                <table class="table table-condensed table-hover">
                  <tbody>
                    <tr>
                      <td class="font-montserrat all-caps fs-12 w-50">Purchase CODE #2345</td>
                      <td class="text-right hidden-lg">
                        <span class="hint-text small">dewdrops</span>
                      </td>
                      <td class="text-right b-r b-dashed b-grey w-25">
                        <span class="hint-text small">Qty 1</span>
                      </td>
                      <td class="w-25">
                        <span class="font-montserrat fs-18">$27</span>
                      </td>
                    </tr>
                    <tr>
                      <td class="font-montserrat all-caps fs-12 w-50">Purchase CODE #2345</td>
                      <td class="text-right hidden-lg">
                        <span class="hint-text small">dewdrops</span>
                      </td>
                      <td class="text-right b-r b-dashed b-grey w-25">
                        <span class="hint-text small">Qty 1</span>
                      </td>
                      <td class="w-25">
                        <span class="font-montserrat fs-18">$27</span>
                      </td>
                    </tr>
                    <tr>
                      <td class="font-montserrat all-caps fs-12 w-50">Purchase CODE #2345</td>
                      <td class="text-right hidden-lg">
                        <span class="hint-text small">dewdrops</span>
                      </td>
                      <td class="text-right b-r b-dashed b-grey w-25">
                        <span class="hint-text small">Qty 1</span>
                      </td>
                      <td class="w-25">
                        <span class="font-montserrat fs-18">$27</span>
                      </td>
                    </tr>
                    <tr>
                      <td class="font-montserrat all-caps fs-12 w-50">Purchase CODE #2345</td>
                      <td class="text-right hidden-lg">
                        <span class="hint-text small">dewdrops</span>
                      </td>
                      <td class="text-right b-r b-dashed b-grey w-25">
                        <span class="hint-text small">Qty 1</span>
                      </td>
                      <td class="w-25">
                        <span class="font-montserrat fs-18">$27</span>
                      </td>
                    </tr>
                    <tr>
                      <td class="font-montserrat all-caps fs-12 w-50">Purchase CODE #2345</td>
                      <td class="text-right hidden-lg">
                        <span class="hint-text small">dewdrops</span>
                      </td>
                      <td class="text-right b-r b-dashed b-grey w-25">
                        <span class="hint-text small">Qty 1</span>
                      </td>
                      <td class="w-25">
                        <span class="font-montserrat fs-18">$27</span>
                      </td>
                    </tr>
                    <tr>
                      <td class="font-montserrat all-caps fs-12 w-50">Purchase CODE #2345</td>
                      <td class="text-right hidden-lg">
                        <span class="hint-text small">dewdrops</span>
                      </td>
                      <td class="text-right b-r b-dashed b-grey w-25">
                        <span class="hint-text small">Qty 1</span>
                      </td>
                      <td class="w-25">
                        <span class="font-montserrat fs-18">$27</span>
                      </td>
                    </tr>
                    <tr>
                      <td class="font-montserrat all-caps fs-12 w-50">Purchase CODE #2345</td>
                      <td class="text-right hidden-lg">
                        <span class="hint-text small">dewdrops</span>
                      </td>
                      <td class="text-right b-r b-dashed b-grey w-25">
                        <span class="hint-text small">Qty 1</span>
                      </td>
                      <td class="w-25">
                        <span class="font-montserrat fs-18">$27</span>
                      </td>
                    </tr>
                    <tr>
                      <td class="font-montserrat all-caps fs-12 w-50">Purchase CODE #2345</td>
                      <td class="text-right hidden-lg">
                        <span class="hint-text small">dewdrops</span>
                      </td>
                      <td class="text-right b-r b-dashed b-grey w-25">
                        <span class="hint-text small">Qty 1</span>
                      </td>
                      <td class="w-25">
                        <span class="font-montserrat fs-18">$27</span>
                      </td>
                    </tr>
                    <tr>
                      <td class="font-montserrat all-caps fs-12 w-50">Purchase CODE #2345</td>
                      <td class="text-right hidden-lg">
                        <span class="hint-text small">dewdrops</span>
                      </td>
                      <td class="text-right b-r b-dashed b-grey w-25">
                        <span class="hint-text small">Qty 1</span>
                      </td>
                      <td class="w-25">
                        <span class="font-montserrat fs-18">$27</span>
                      </td>
                    </tr>
                    <tr>
                      <td class="font-montserrat all-caps fs-12 w-50">Purchase CODE #2345</td>
                      <td class="text-right hidden-lg">
                        <span class="hint-text small">dewdrops</span>
                      </td>
                      <td class="text-right b-r b-dashed b-grey w-25">
                        <span class="hint-text small">Qty 1</span>
                      </td>
                      <td class="w-25">
                        <span class="font-montserrat fs-18">$27</span>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="padding-25 mt-auto">
                <p class="small no-margin">
                  <a href="#"><i class="fa fs-16 fa-arrow-circle-o-down text-success m-r-10"></i></a>
                  <span class="hint-text ">Show more details of APPLE . INC</span>
                </p>
              </div>
            </div>
            <!-- END WIDGET -->
          </div>
            <!-- <div class="col-lg-4">
              <div class="card-block">
                <div data-pages="card" class="card card-default" id="card-basic">
                  <div class="card-controls">
                    <div class="row">
                      <div class="col-md-12">
                        <h4><strong>Top 5 Sales Products</strong></h4>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <table width="100%" border="0" cellpadding="10">
                          <thead>
                            <tr>
                              <th><center>SKU</center></th>
                              <th><center>Product</center></th>
                              <th><center>Qty</center></th>
                              <th><center>Total (IDR)</center></th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td align="center">001</td>
                              <td>Product 1</td>
                              <td align="center">100</td>
                              <td align="right">300.000</td>
                            </tr>
                            <tr>
                              <td align="center">002</td>
                              <td>Product 2</td>
                              <td align="center">95</td>
                              <td align="right">285.000</td>
                            </tr>
                            <tr>
                              <td align="center">003</td>
                              <td>Product 3</td>
                              <td align="center">92</td>
                              <td align="right">276.000</td>
                            </tr>
                            <tr>
                              <td align="center">004</td>
                              <td>Product 4</td>
                              <td align="center">80</td>
                              <td align="right">240.000</td>
                            </tr>
                            <tr>
                              <td align="center">005</td>
                              <td>Product 5</td>
                              <td align="center">60</td>
                              <td align="right">200.000</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
    
@endsection
