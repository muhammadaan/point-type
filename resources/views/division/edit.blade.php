@extends('layouts.app')

@section('_title')
<title>Division | Edit</title>
@endsection

@section('content')
<?php $acl_btn = App\Models\Usergroup::where('name', \Auth::user()->roles)->where('route_access_list', 'LIKE', '%'.'division.edit'.'%')->count(); ?>		 
@if($acl_btn != 0 || \Auth::user()->roles == 'superadmin')

	<div class="col-md-12">
		<div data-pages="card" class="card card-default">
			<div class="card-header">
				<div class="card-title">
					<h4>Edit Division</h4>
				</div>
				<div class="card-controls">
					<ul>
						<li>
							<a data-toggle="collapse" class="card-collapse" href="#"><i class="card-icon card-icon-collapse"></i></a>
						</li>	
					</ul>
				</div>
			</div>
			<div class="card-block">
				<button onclick="goBack()" class="btn btn-complete m-b-15">Back</button>
				<form action="{{url('/division/update')}}/{{$division->id}} " method="POST" enctype="multipart/form-data" id="form1" role="form" autocomplete="off">
				{{csrf_field()}}	
				<input type="hidden" name="id" value="{{$division->id}}">	
					<div class="form-group form-group-default">
						<label>Division Name</label>
						<input type="text" name="division_name" class="form-control" value="{{$division->division_name}}">
					</div>
					<input type="hidden" name="created_by" class="form-control" value="{{$division->created_by}}">
					<button class="btn btn-success" type="submit">Submit</button>
				</form>
			</div>
		</div>
		@else
		<div class="card-block"> 
		 	<h3 align="center">Sorry, You don't have access permission to this page !</h3>
		</div>
		@endif
	</div>
@endsection