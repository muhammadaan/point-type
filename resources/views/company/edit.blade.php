@extends('layouts.app')

@section('_title')
<title>Company | Edit</title>
@endsection

@section('content')

<?php $acl_btn = App\Models\Usergroup::where('name', \Auth::user()->roles)->where('route_access_list', 'LIKE', '%'.'company.edit'.'%')->count(); ?>		 
	@if($acl_btn != 0 || \Auth::user()->roles == 'superadmin')

	<div class="col-md-12">
		<div data-pages="card" class="card card-default">
			<div class="card-header">
				<div class="card-title">
					<h4>Edit Company</h4>
				</div>
				<div class="card-controls">
					<ul>
						<li>
							<a data-toggle="collapse" class="card-collapse" href="#"><i class="card-icon card-icon-collapse"></i></a>
						</li>	
					</ul>
				</div>
			</div>
			<div class="card-block">
				<button onclick="goBack()" class="btn btn-complete m-b-15">Back</button>
				<form action="{{url('/company/update')}}/{{$company->id}}" method="POST" enctype="multipart/form-data" id="form1" role="form" autocomplete="off">
				{{csrf_field()}}
				<input type="hidden" name="id" value="{{$company->id}}">		
					<div class="form-group form-group-default">
						<label>Company Code</label>
						<input type="text" name="company_code" class="form-control" value="{{$company->company_code}}">
					</div>
					<div class="form-group form-group-default">
						<label>Company Name</label>
						<input type="text" name="company_name" class="form-control" value="{{$company->company_name}}">
					</div>
					<div class="form-group form-group-default">
						<label>Company Email</label>
						<input type="email" name="email" class="form-control" value="{{$company->email}}">
					</div>
					<div class="form-group form-group-default">
						<label>Company Telephone</label>
						<input type="text" name="telephone" class="form-control" value="{{$company->telephone}}">
					</div>
					<div class="form-group form-group-default">
						<label>Company Address</label>
						<textarea name="address" class="form-control">{{$company->address}}</textarea>
					</div>
					<div class="form-group form-group-default">
						<label>PIC Name</label>
						<input type="text" name="pic_name" class="form-control" required="" value="{{$company->pic_name}}">
					</div>
					<div class="form-group form-group-default">
						<label>PIC Email</label>
						<input type="email" name="pic_email" class="form-control" required="" value="{{$company->pic_email}}">
					</div>
					<div class="form-group form-group-default">
						<label>PIC Telephone</label>
						<input type="text" name="pic_telephone" class="form-control" required="" value="{{$company->pic_telephone}}">
					</div>
					<div class="form-group form-group-default">
						<div class="row" style="margin-left: 0px; margin-right: 0px;">
							<div class="col-md-4">
								<div class="form-group ">
									<label>Primary Color</label>
								    <input type="color" name="primary_color" value="{{$company->primary_color}}">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group ">
									<label>Secondary Color</label>
								    <input type="color"  name="secondary_color" value="{{$company->secondary_color}}">
								</div>
							</div>
						 </div>
					</div>
					<div class="form-group form-group-default">
		                <div class="form-group form-group-default input-group col-md-2">
		                  <div class="form-input-group">
		                    <label class="inline">License Status</label>
		                  </div>
		                  <div class="input-group-addon bg-transparent h-c-50">
		                  	@if($company->license_status == 1)
		                    	<input type="checkbox" name="license_status" data-init-plugin="switchery" data-size="medium" data-color="primary" checked="checked" />
		                   	@else
		                   		<input type="checkbox" name="license_status" data-init-plugin="switchery" data-size="medium" data-color="primary" />
 							@endif
		                  </div>
		                </div>
		             </div>
					<input type="hidden" name="created_by" class="form-control" value="{{$company->created_by}}">
					<button class="btn btn-success">Submit</button>
				</form>
			</div>
		</div>
		@else
		<div class="card-block"> 
    	 	<h3 align="center">Sorry, You don't have access permission to this page !</h3>
		</div>
		@endif
	</div>
@endsection