<div class="modal fade slide-up disable-scroll" id="company-detail" tabindex="-1" role="dialog" aria-labelledby="company-detail" aria-hidden="false">
    <div class="modal-dialog ">
        <div class="modal-content-wrapper">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                  <i class="pg-close fs-14"></i>
                </button>
                <h5 style="padding-left: 8.5px">Company Detail</h5>
            </div>
            <div class="modal-body">
                <form>
                	<div class="form-group-attached">
                        <img id="company-logo">
                		<div class="form-group row">
                			<label class="col-md-4 control-label">Company Code</label>
                			<div class="col-md-8" id="company-code"></div>
                		</div>
                		<div class="form-group row">
                			<label class="col-md-4">Company Name</label>
                			<div class="col-md-8" id="company-name"></div>
                		</div>
                		<div class="form-group row">
                			<label class="col-md-4">Email</label>
                			<div class="col-md-8" id="email"></div>
                		</div>
                		<div class="form-group row">
                			<label class="col-md-4">Telephone</label>
                			<div class="col-md-8" id="telephone"></div>
                		</div>
                		<div class="form-group row">
                			<label class="col-md-4">Address</label>
                			<div class="col-md-8" id="address"></div>
                		</div>
                        <div class="form-group row">
                            <label class="col-md-4">PIC Name</label>
                            <div class="col-md-8" id="pic_name"></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4">PIC Email</label>
                            <div class="col-md-8" id="pic_email"></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4">PIC Telephone</label>
                            <div class="col-md-8" id="pic_telephone"></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4">Primary Color</label>
                            <div class="col-md-8" id="primary_color"></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4">Secondary Color</label>
                            <div class="col-md-8" id="secondary_color"></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4">License Status</label>
                            <div class="col-md-8" id="license_status"></div>
                        </div>
                		<div class="form-group row">
                			<label class="col-md-4">Created At</label>
                			<div class="col-md-8" id="created-at"></div>
                		</div>
                		<div class="form-group row">
                			<label class="col-md-4">Created By</label>
                			<div class="col-md-8" id="created-by"></div>
                		</div>
                	</div>
                </form>
            </div>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>