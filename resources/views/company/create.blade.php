@extends('layouts.app')

@section('_title')
<title>Company | Add</title>
@endsection

@section('content')

<?php $acl_btn = App\Models\Usergroup::where('name', \Auth::user()->roles)->where('route_access_list', 'LIKE', '%'.'company.create'.'%')->count(); ?>		 
@if($acl_btn != 0 || \Auth::user()->roles == 'superadmin')

	<div class="col-md-12">
		<div data-pages="card" class="card card-default">
			<div class="card-header">
				<div class="card-title">
					<h4>Add Company</h4>
				</div>
				<div class="card-controls">
					<ul>
						<li>
							<a data-toggle="collapse" class="card-collapse" href="#"><i class="card-icon card-icon-collapse"></i></a>
						</li>	
					</ul>
				</div>
			</div>
			<div class="card-block">
				<button onclick="goBack()" class="btn btn-complete m-b-15">Back</button>
				<form action="{{ url('/company/insert') }}" method="POST" enctype="multipart/form-data" id="form1" role="form" autocomplete="off">
					{{ csrf_field() }}
					<div class="form-group">
						<label>Company Code</label>
						<input type="text" name="company_code" class="form-control" required="" placeholder="ex: SOL">
					</div>
					<div class="form-group ">
						<label>Company Name</label>
						<input type="text" name="company_name" class="form-control" required="" placeholder="ex: Company">
					</div>
					<div class="form-group">
						<label>Company Logo</label>
						<input type="file" name="company_logo" required="" class="form-control-file">
					</div>
					<div class="form-group ">
						<label>Company Email</label><br>
						<input type="email" name="email" class="form-control" required="" placeholder="ex: mail@company.com">
					</div>
					<div class="form-group ">
						<label>Company Telephone</label>
						<input type="text" name="telephone" class="form-control" required="" placeholder="ex: 021xxxxxx">
					</div>
					<div class="form-group ">
						<label>Company Address</label>
						<textarea style="height: 60px;" name="address" class="form-control" required="" placeholder="ex: Rasuna Said, South Jakarta"></textarea>
					</div>
					<div class="form-group ">
						<label>PIC Name</label>
						<input type="text" name="pic_name" class="form-control" required="" placeholder="ex: John Doe">
					</div>
					<div class="form-group ">
						<label>PIC Email</label>
						<input type="email" name="pic_email" class="form-control" required="" placeholder="ex: johndoe@example.com">
					</div>
					<div class="form-group ">
						<label>PIC Telephone</label>
						<input type="text" name="pic_telephone" class="form-control" required="" placeholder="ex: 021xxxxxx">
					</div>
					<div class="form-group ">
						<div class="row" style="margin-left: 0px; margin-right: 0px;">
							<div class="col-md-4">
								<div class="form-group ">
									<label>Primary Color</label>
								    <input type="color" name="primary_color">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group ">
									<label>Secondary Color</label>
								    <input type="color"  name="secondary_color">
								</div>
							</div>
						 </div>
					</div>
					<div class="form-group">
		                <div class="form-group form-group-default input-group col-md-2">
		                  <div class="form-input-group">
		                    <label class="inline">License Status</label>
		                  </div>
		                  <div class="input-group-addon bg-transparent h-c-50">
		                    <input type="checkbox" name="license_status" data-init-plugin="switchery" data-size="medium" data-color="primary" />
		                  </div>
		                </div>
		             </div>   
					<button class="btn btn-success" type="submit" style="margin-top: 1%">Submit</button>
				</form>
			</div>
		</div>
		@else
		<div class="card-block"> 
    	 	<h3 align="center">Sorry, You don't have access permission to this page !</h3>
		</div>
		@endif
	</div>
@endsection
