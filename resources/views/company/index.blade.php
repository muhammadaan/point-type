@extends('layouts.app')

@section('_title')
<title>Company | List</title>
@endsection

@push('styles')
	<link type="text/css" rel="stylesheet" href="{{ asset('template/assets/plugins/jquery-datatable/media/css/jquery.dataTables.css') }}">
	<link type="text/css" rel="stylesheet" href="{{ asset('template/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css') }}">
	<link media="screen" type="text/css" rel="stylesheet" href="{{ asset('template/assets/plugins/datatables-responsive/css/datatables.responsive.css') }}">
	<style>
		.form-group-attached {
			padding-left: 15px
		}

	</style>
@endpush

@section('content')

<?php $acl_btn = App\Models\Usergroup::where('name', \Auth::user()->roles)->where('route_access_list', 'LIKE', '%'.'company.index'.'%')->count(); ?>		 
	@if($acl_btn != 0 || \Auth::user()->roles == 'superadmin')

	<div class="col-md-12">
		<div data-pages="card" class="card card-default">
			<div class="card-header">
				<div class="card-title">
					<h4>Company List</h4>
				</div>
				<div class="card-controls">
					<ul>
						<li>
							<a data-toggle="collapse" class="card-collapse" href="#"><i class="card-icon card-icon-collapse"></i></a>
						</li>	
					</ul>
				</div>
				@if (session('message'))
				    <div class="alert alert-success" role="alert">
				    	<button class="close" data-dismiss="alert"></button>
				        {{ session('message') }}
				    </div>
				@endif
			</div>
			<div class="card-block">
				<button class="btn btn-complete m-b-15" onclick="goBack()">Back</button>
				<button class="btn btn-primary m-b-15" onclick="relocateAddCompany()">Add New Company</button>
				<table id="companyTable" class="table table-hover" cellspacing="0" width="100%">
				    <thead>
				        <tr>
				            <th>Company Code</th>
				            <th>Company Name</th>
				            <th>Email</th>
				            <th>Created At</th>
				            <th>Updated At</th>
				            <th>Action</th>
				        </tr>
				    </thead>
				    <tfoot style="display: table-header-group;">
				        <tr>
				            <th>Company Code</th>
				            <th>Company Name</th>
				            <th>Email</th>
				            <th>Created At</th>
				            <th>Updated At</th>
				            <th>Action</th>
				        </tr>
				    </tfoot>
				    <tbody>
				    	@foreach($company as $show)
				    		<tr>
					    		<td>{{ $show->company_code }}</td>
					    		<td>{{ $show->company_name }}</td>
					    		<td>{{ $show->email }}</td>
					    		<td>{{ $show->created_at }}</td>
					    		<td>{{ $show->updated_at }}</td>
					    		<td align="center">
					    			<a href="{{url('/company/edit')}}/{{$show->id}}" title="Edit"><i class="fa fa-edit"></i></a>
				                    <a href="{{url('/company/delete')}}/{{$show->id}}" onclick="return ConfirmDelete()" title="Delete"><i class="pg-trash"></i></a>
				                    <a onclick="showModal({{ $show->id }})" href="#" data-toggle="modal" title="Show Detail"><i class="fa fa-search"></i></a>
				               	</td>
			               </tr>
			            @endforeach   	
				    </tbody>
				</table>		
			</div>
		</div>
		@else
		<div class="card-block"> 
    	 	<h3 align="center">Sorry, You don't have access permission to this page !</h3>
		</div>
		@endif
	</div>
	
	@include('company.modals.detail')
@endsection

@push('scripts')
	<script src="{{ asset('template/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('template/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('template/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js') }}"></script>
	<script src="{{ asset('template/assets/plugins/datatables-responsive/js/datatables.responsive.js') }}" type="text/javascript"></script>
	<script src="{{ asset('template/assets/plugins/datatables-responsive/js/lodash.min.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		function ConfirmDelete()
		{
		  var x = confirm("Are you sure you want to delete ?");
		  
		  if(x)
		  	return true;
		  else
		    return false;
			
		}   
	</script>
	<script>
		$(document).ready(function() {
		    // Setup - add a text input to each footer cell
		    $('#companyTable tfoot th').each( function () {
		        var title = $(this).text();
		        $(this).html( '<input type="text" />' );
		    } );

		    var table = $('#companyTable').DataTable();

		    // Apply the search
		    table.columns().every( function () {
		        var that = this;
		 
		        $( 'input', this.footer() ).on( 'keyup change', function () {
		            if ( that.search() !== this.value ) {
		                that
		                    .search( this.value )
		                    .draw();
		            }
		        } );
		    } );
		} );

		function relocateAddCompany() {
			window.location.href = "{{ route('company.create') }}";
		}

		function showModal(id)
		{	
			$('#company-detail').modal('show')
			$.ajax({
				url: '{{ route("company.show") }}',
				data: {
					id: id
				},
				type: "GET",
				success: function(response) {
					$('#company-logo').empty()
					$('#company-code').empty()
					$('#company-name').empty()
					$('#email').empty()
					$('#telephone').empty()
					$('#address').empty()
					$('#created-at').empty()
					$('#created-by').empty()
					$('#pic_name').empty()
					$('#pic_email').empty()
					$('#pic_telephone').empty()
					$('#valid_on').empty()
					$('#valid_until').empty()
					$('#company-logo').attr('src', '/assets/images/' + response.company_logo)
					$('#company-code').append(response.company_code)
					$('#company-name').append(response.company_name)
					$('#email').append(response.email)
					$('#telephone').append(response.telephone)
					$('#address').append(response.address)
					$('#created-at').append(response.created_at)
					$('#created-by').append(response.created_by)
					$('#pic_name').append(response.pic_name)
					$('#pic_email').append(response.pic_email)
					$('#pic_telephone').append(response.pic_telephone)
					$('#primary_color').append(response.primary_color)
					$('#secondary_color').append(response.secondary_color)
					$('#license_status').append(response.license_status == 1 ? 'Active' : 'In Active')	
				}
			})
		}
	</script>
@endpush