<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>DisKebPar Back-End | Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" href="{{ asset('template/pages/ico/60.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('template/pages/ico/76.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('template/pages/ico/120.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('template/pages/ico/152.png') }}">
    <link rel="icon" type="image/x-icon" href="{{ asset('template/favicon.ico') }}" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
    <link href="{{ asset('template/assets/plugins/pace/pace-theme-flash.css') }}" rel="stylesheet" type="text/css" />
    <!-- <link href="{{ asset('template/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> -->
    <link href="{{ asset('template/assets/plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('template/assets/plugins/jquery-scrollbar/jquery.scrollbar.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{ asset('template/assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{ asset('template/assets/plugins/switchery/css/switchery.min.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{ asset('template/pages/css/pages-icons.css') }}" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="{{ asset('template/pages/css/pages.css') }}" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="{{ asset('template/pages/css/windows.chrome.fix.css') }}" />'
    }
    </script>
  </head>
  <body class="fixed-header ">
    <div class="login-wrapper ">
      <!-- START Login Background Pic Wrapper-->
      <div class="bg-pic" style="background-color: #ffffff">
        <!-- START Background Pic-->
        <!-- <img src="{{ asset('template/assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg') }}" data-src="{{ asset('template/assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg') }}" data-src-retina="{{ asset('template/assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg') }}" alt="" class="lazy"> -->
        <img src="{{url('storage/app/images/gambar3.png')}}" data-src="{{url('storage/app/images/gambar3.png')}}" data-src-retina="{{url('storage/app/images/gambar3.png')}}" >
        <!-- END Background Pic-->
        <!-- START Background Caption-->
        <div class="bg-caption pull-bottom sm-pull-bottom p-l-20 m-b-20" style="color: #000000">
          <!-- <h1 class="semi-bold text-white">
          Jadilah yang terbaik!</h1>
          <p class="x-large" style="font-size: 21px">
            Kumpulkan Solutech Point sebanyak-banyaknya
          </p>
          <p class="x-large" style="font-size: 21px">
            Dan tukarkan pointnya dengan berbagai hadiah menarik
          </p>
          <br /> -->
          <p class="small">
            Developed by Muhammad Wardhiansyah, S.Kom &copy; 2018
          </p>
        </div>
        <!-- END Background Caption-->
      </div>
      <!-- END Login Background Pic Wrapper-->
      <!-- START Login Right Container-->
      <div class="login-container bg-white" style="background-color: #9ede15">
        <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-30 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
          <h1 style="text-align: center;"><img src="{{url('storage/app/images/pemko.png')}}" alt="User Image" height="90"><br /><strong style="font-style: 11px;">Dinas Kebudayaan dan Pariwisata Kota Tanjungpinang</strong></h1>
          <!-- <h2><strong>Point Type</strong></h2> -->
          <!-- START Login Form -->
          <form id="form-register" class="p-t-15" role="form" action="{{ route('register') }}" method="POST">
            <!-- START Form Control-->
            {{ csrf_field() }}
            <div class="row">
            <div class="col-md-12">
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} form-group-default">
                <label>Name</label>
                <input type="text" name="name" placeholder="Name" class="form-control" value="{{ old('name') }}" required>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
              </div>
            </div>

          <div class="row">
            <div class="col-md-12">
              <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }} form-group-default">
                <label>Username</label>
                <input type="text" name="username" placeholder="Username" class="form-control" required value="{{ old('username') }}">
                @if ($errors->has('username'))
                    <span class="help-block">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
              </div>
            </div>
          </div>
            <div class="row">
            <div class="col-md-6">
              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} form-group-default">
                <label>Password</label>
                <input type="password" name="password" placeholder="Password" class="form-control" required>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-group-default">
                <label>Password Confirmation</label>
                <input type="password" name="password_confirmation" placeholder="Password Confirmation" class="form-control" required>
              </div>
            </div>
          </div>

           <div class="row">
            <div class="col-md-12">
              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} form-group-default">
                <label>Email</label>
                <input type="email" name="email" placeholder="Email" class="form-control" required value="{{ old('email') }}">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              </div>
            </div>
          </div>
          <!-- END Form Control-->

            <button class="btn btn-primary col-md-12" type="submit" style="background-color: #00802b; border: #00802b">Register</button>
            {{ csrf_field() }}
          </form>
          <div style="padding-top: 10px;">    
            <div class="col-md-8 no-padding sm-p-l-10">
              <a href="{{ route('login') }}" class="text-info medium" style="text-align: left; padding-left: 10px;">Back</a>
            </div>
         </div>
          <!--END Login Form-->
        </div>
      </div>
      <!-- END Login Right Container-->
    </div>
    <!-- BEGIN VENDOR JS -->
    <script src="{{ asset('template/assets/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('template/assets/plugins/jquery/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('template/assets/plugins/modernizr.custom.js') }}" type="text/javascript"></script>
    <script src="{{ asset('template/assets/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('template/assets/plugins/tether/js/tether.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('template/assets/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('template/assets/plugins/jquery/jquery-easy.js') }}" type="text/javascript"></script>
    <script src="{{ asset('template/assets/plugins/jquery-unveil/jquery.unveil.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('template/assets/plugins/jquery-ios-list/jquery.ioslist.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('template/assets/plugins/jquery-actual/jquery.actual.min.js') }}"></script>
    <script src="{{ asset('template/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/assets/plugins/select2/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('template/assets/plugins/classie/classie.js') }}"></script>
    <script src="{{ asset('template/assets/plugins/switchery/js/switchery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('template/assets/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <!-- END VENDOR JS -->
    <script src="{{ asset('template/pages/js/pages.min.js') }}"></script>

  </body>
</html>