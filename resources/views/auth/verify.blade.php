
@extends('layouts.apps')

@section('_title')
<title>MPOS | Verify Account</title>
@endsection
@section('_style')

<link href="{{url('public/template/pages/assets/plugins/pace/pace-theme-flash.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('public/template/pages/assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('public/template/pages/assets/plugins/font-awesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('public/template/pages/assets/plugins/jquery-scrollbar/jquery.scrollbar.css')}}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{url('public/template/pages/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{url('public/template/pages/assets/plugins/switchery/css/switchery.min.css')}}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{url('public/template/pages/assets/plugins/nvd3/nv.d3.min.css')}}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{url('public/template/pages/assets/plugins/mapplic/css/mapplic.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('public/template/pages/assets/plugins/rickshaw/rickshaw.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('public/template/pages/assets/plugins/jquery-metrojs/MetroJs.css')}}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{url('public/template/pages/pages/css/pages-icons.css')}}" rel="stylesheet" type="text/css">
<link class="main-stylesheet" href="{{url('public/template/pages/pages/css/pages.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('public/template/pages/assets/plugins/dropzone/css/dropzone.css')}}" rel="stylesheet" type="text/css" />
<link href="{{url('public/template/pages/assets/plugins/bootstrap-datepicker/css/datepicker3.css')}}" rel="stylesheet" type="text/css" media="screen">
<link href="{{url('public/template/pages/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet" type="text/css" media="screen">
<link rel="stylesheet" type="text/css" href="{{ url('public/template/pages/assets/css/style.css') }}">

@endsection

@section('_script')
{{-- <script type="text/javascript" src="{{ url('public/js/coverage.js') }}"></script> --}}


    <script src="{{url('public/template/pages/assets/plugins/pace/pace.min.js')}}" type="text/javascript"></script>
    <script src="{{url('public/template/pages/assets/plugins/jquery/jquery-1.11.1.min.js')}}" type="text/javascript"></script>
    <script src="{{url('public/template/pages/assets/plugins/modernizr.custom.js')}}" type="text/javascript"></script>
    <script src="{{url('public/template/pages/assets/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
    <script src="{{url('public/template/pages/assets/plugins/tether/js/tether.min.js')}}" type="text/javascript"></script>
    <script src="{{url('public/template/pages/assets/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{url('public/template/pages/assets/plugins/jquery/jquery-easy.js')}}" type="text/javascript"></script>
    <script src="{{url('public/template/pages/assets/plugins/jquery-unveil/jquery.unveil.min.js')}}" type="text/javascript"></script>
    <script src="{{url('public/template/pages/assets/plugins/jquery-ios-list/jquery.ioslist.min.js')}}" type="text/javascript"></script>
    <script src="{{url('public/template/pages/assets/plugins/jquery-actual/jquery.actual.min.js')}}"></script>
    <script src="{{url('public/template/pages/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{url('public/template/pages/assets/plugins/select2/js/select2.full.min.js')}}"></script>
    <script type="text/javascript" src="{{url('public/template/pages/assets/plugins/classie/classie.js')}}"></script>
    <script src="{{url('public/template/pages/assets/plugins/switchery/js/switchery.min.js')}}" type="text/javascript"></script>
    <script src="{{url('public/template/pages/assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
    <script type="text/javascript" src="{{url('public/template/pages/assets/plugins/jquery-autonumeric/autoNumeric.js')}}"></script>
    <script type="text/javascript" src="{{url('public/template/pages/assets/plugins/dropzone/dropzone.min.js')}}"></script>
    <script type="text/javascript" src="{{url('public/template/pages/assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js')}}"></script>
    <script type="text/javascript" src="{{url('public/template/pages/assets/plugins/jquery-inputmask/jquery.inputmask.min.js')}}"></script>
    <script src="{{url('public/template/pages/assets/plugins/bootstrap-form-wizard/js/jquery.bootstrap.wizard.min.js')}}" type="text/javascript"></script>
    <script src="{{url('public/template/pages/assets/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
    <script src="{{url('public/template/pages/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
    <script src="{{url('public/template/pages/assets/plugins/summernote/js/summernote.min.js')}}" type="text/javascript"></script>
    <script src="{{url('public/template/pages/assets/plugins/moment/moment.min.js')}}"></script>
    <script src="{{url('public/template/pages/assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{url('public/template/pages/assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js')}}"></script>
    <!-- END VENDOR JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="{{url('public/template/pages/pages/js/pages.min.js')}}"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="{{url('public/template/pages/assets/js/form_wizard.js')}}" type="text/javascript"></script>
    <script src="{{url('public/template/pages/assets/js/scripts.js')}}" type="text/javascript"></script>



@endsection


@section('_container')

<div class="content sm-gutter">  
  <div class="container-fluid padding-25 sm-padding-10">
   
              <!-- Nav tabs -->
       <h4><strong>VERIFY YOUR ACCOUNT</strong></h4>
     
        <hr>
          <div class="col-md-8">
             <p>Go to your email and click on the link provide to verify your account</p>
             <p>No received an email ? <a href="" >Resend email</a></p>
          </div>
        <br><br>  
        <h4><strong>DOWNLOAD THE APP</strong></h4>
         <hr>
           <div class="col-md-8">
              <p>Download the Solutech app for Android smartphones from the Google Play Store</p>
              <p>Start selling today !</p>
              <img src="{{url('public/img/android.png')}}" class="img-circle" alt="User Image" width="150" height="50" style="margin-right: 10px;">
           </div>
    </div>
</div> 

@endsection  
       
      
