<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCmsHome extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_home', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('banner_main_status');
            $table->integer('company_id')->unsigned();
            $table->integer('services_status');
            $table->integer('team_status');
            $table->integer('about_status');
            $table->integer('news_status');
            $table->integer('portfolio_status');
            $table->integer('location_status'); 
            $table->string('contact_phone');
            $table->string('contact_email');
            $table->string('contact_gps_coor');
            $table->string('contact_instagram');
            $table->string('contact_twitter');
            $table->string('contact_facebook');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_home');
    }
}
