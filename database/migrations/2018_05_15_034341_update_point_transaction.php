<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePointTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::select("ALTER TABLE `point_transaction` CHANGE `point_in` `point_in` INT NULL");
        \DB::select("ALTER TABLE `point_transaction` CHANGE `point_out` `point_out` INT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('point_transaction', function (Blueprint $table) {
            //
        });
    }
}
