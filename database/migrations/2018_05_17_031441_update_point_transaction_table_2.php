<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePointTransactionTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::select("ALTER TABLE `point_transaction` ALTER `point_in` SET DEFAULT 0");
        \DB::select("ALTER TABLE `point_transaction` ALTER `point_out` SET DEFAULT 0");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('point_transaction', function (Blueprint $table) {
            //
        });
    }
}
