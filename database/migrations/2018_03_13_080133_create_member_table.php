<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member', function (Blueprint $table) {
            $table->increments('id');
            $table->string('member_code');
            $table->string('member_name');
            $table->string('email');
            $table->string('password');
            $table->integer('status');
            $table->string('avatar');
            $table->integer('id_company')->unsigned();
            $table->integer('id_division')->unsigned();
            $table->integer('id_role')->unsigned();
            $table->string('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member');
    }
}
