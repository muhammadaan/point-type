<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableCompany001 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('company', function (Blueprint $table) {
            $table->dropColumn(['valid_on', 'valid_until']);
            $table->string('primary_color')->after('pic_telephone');
            $table->string('secondary_color')->after('primary_color');
            $table->string('license_status')->after('secondary_color');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
