<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCmsTeam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_team', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cms_home_id')->unsigned();
            $table->string('name');
            $table->string('image');
            $table->string('url');
            $table->string('position');
            $table->text('desc');
            $table->string('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_team');
    }
}
