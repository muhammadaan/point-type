<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRouteList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
           Schema::create('route_list', function (Blueprint $table) {
               $table->increments('id')->index();
               $table->string('route_name',191)->index();
               $table->string('description',191)->index();
               $table->string('status',191)->index();
               $table->string('modul',191)->index();
               $table->timestamps();
           });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('route_list');
    }
}
