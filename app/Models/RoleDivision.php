<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleDivision extends Model
{
    protected $table = 'role_division';

    protected $fillable = ['id', 'role_code', 'role_name', 'created_by'];


    public function division()
    {
    	return $this->hasMany('App\Models\Division', 'id', 'id_role_division');	
    }

    public function member()
    {
    	return $this->hasMany('App\Models\Member', 'id', 'id_role');	
    }


}
