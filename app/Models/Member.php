<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $table =  'member';

    protected $fillable = ['img_tmp'];

    protected $hidden = [

        'user_id', 'id_company', 'id_division', 'password'
    ];


    public function division()
    {
    	return $this->belongsTo('App\Models\Division', 'id_division', 'id');	
    }

    public function company()
    {
    	return $this->belongsTo('App\Models\Company', 'id_company', 'id');	
    }

    public function user()
    {
        return $this->belongsTo('App\User');  
    }
}
