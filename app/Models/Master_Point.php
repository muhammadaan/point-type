<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Master_Point extends Model
{
    protected $table = 'master_point';

    protected $fillable = ['id', 'division_id', 'jumlah_point', 'point_type_id', 'reward_id', 'created_by'];
}
