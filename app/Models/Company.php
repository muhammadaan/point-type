<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'company';

    protected $fillable = ['id', 'company_code', 'company_name', 'email', 'telephone', 'address', 'created_by', 'company_logo', 'pic_name', 'pic_email', 'pic_telephone', 'primary_color', 'secondary_color', 'license_status'];

    public function divisions()
    {
    	return $this->hasMany('App\Models\Division', 'id_company', 'id');
    }

    public function member()
    {
    	return $this->hasMany('App\Models\Member', 'id_company', 'id');
    }
}
