<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PointTransaction extends Model
{
	protected $table = 'point_transaction';

	protected $fillable = ['transaction_date', 'member_id', 'point_wallet_id', 'created_by', 'point_in', 'point_out', 'point_type_id'];    

	public function pointType()
	{
		return $this->belongsTo('App\Models\PointType', 'point_type_id', 'id');
	}
}
