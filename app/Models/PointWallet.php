<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PointWallet extends Model
{
    protected $table = 'point_wallet';

    protected $fillable = ['current_point', 'current_point_kpi', 'member_id', 'wallet_level_id','created_by','updated_by'];


    public function member()
    {
    	return $this->belongsTo('App\Models\Member', 'member_id', 'id');	
    }
	

	public function wallet_level()
    {
    	return $this->belongsTo('App\Models\WalletLevel', 'wallet_level_id', 'id');	
    }

    public function pointTransaction()
    {
        return $this->hasMany('App\Models\PointTransaction', 'point_wallet_id', 'id');
    }

}
