<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CmsLocation extends Model
{
    protected $table = 'cms_location';

    protected $fillable = [
       'cms_home_id', 'name','country', 'region', 'about_status', 'address', 'gps', 'image', 'url', 'created_by'
    ];


    public function cms_home()
    {
    	return $this->belongsTo('App\Models\CmsHome', 'cms_home_id', 'id');
    }

}
