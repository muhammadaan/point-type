<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CMSNews extends Model
{
    protected $table = 'cms_news';

    protected $fillable = ['cms_home_id','title', 'desc', 'name','category','image','tags','created_by','updated_by'];

    public function cms_home()
    {
    	return $this->belongsTo('App\Models\CmsHome', 'cms_home_id', 'id');
    }
}
