<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CmsHome extends Model
{
    
    protected $table = 'cms_home';

    protected $fillable = [
       'banner_main_status', 'company_id','team_status', 'services_status', 'about_status', 'news_status', 'portfolio_status',
       'location_status', 'contact_phone', 'contact_email', 'contact_gps_coor', 'contact_instagram', 'contact_twitter',
       'contact_facebook'
    ];


    public function company()
    {
    	return $this->belongsTo('App\Models\Company', 'company_id', 'id');
    }

}
