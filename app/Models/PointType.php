<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PointType extends Model
{
    protected $table = 'point_type';

    protected $fillable = ['point_type', 'description', 'created_by'];
}
