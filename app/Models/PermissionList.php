<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionList extends Model
{
   
   protected $table = 'permission_list';
   protected $fillable = [
    'usergroup_id','route_list_id','status'
  	];
}
