<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalletLevel extends Model
{
    
    protected $table = 'wallet_level';



    public function point_type()
    {
        return $this->belongsTo('App\Models\PointType', 'point_type_id', 'id');
    }

}
