<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberCard extends Model
{
    protected $table = 'member_card';

    protected $fillable = ['member_id', 'background_image'];

    public function member()
    {
    	return $this->belongsTo('App\Models\Member', 'member_id', 'id');	
    }
}
