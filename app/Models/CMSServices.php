<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CMSServices extends Model
{
    protected $table = 'cms_services';

    protected $fillable = ['cms_home_id','service_name','image','url','desc','created_by','updated_by'];

    public function cms_home()
    {
    	return $this->belongsTo('App\Models\CmsHome', 'cms_home_id', 'id');
    }
}
