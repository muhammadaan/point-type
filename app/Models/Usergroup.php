<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usergroup extends Model
{
    protected $table = 'usergroup';
    
	protected $fillable = [
    'user_id', 'permission_list_id', 'name', 'status', 'route_access_list', 'description'
  	];
}
