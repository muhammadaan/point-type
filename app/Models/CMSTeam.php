<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CMSTeam extends Model
{
    protected $table = 'cms_team';

    protected $fillable = ['cms_home_id','name','image','url','position','desc','team_desc','created_by','updated_by'];

    public function cms_home()
    {
    	return $this->belongsTo('App\Models\CmsHome', 'cms_home_id', 'id');
    }
}
