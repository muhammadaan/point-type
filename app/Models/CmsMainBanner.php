<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CmsMainBanner extends Model
{
    protected $table = 'cms_main_banner';

    protected $fillable = [
       'image', 'url','status', 'cms_home_id'
    ];

    protected $appends = [
    	'company_name'
    ];

    public function cms_home()
    {
    	return $this->belongsTo('App\Models\CmsHome', 'cms_home_id', 'id');
    }

    public function getCompanyNameAttribute()
    {
    	return $this->cms_home->company->company_name;

    }


}
