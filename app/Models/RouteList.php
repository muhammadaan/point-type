<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RouteList extends Model
{
    protected $table = 'route_list';
    protected $fillable = [
            'route_name','description','modul','status'
        ];
}
