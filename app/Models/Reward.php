<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    protected $table = 'reward';

    protected $fillable = ['id', 'reward_name', 'point_required', 'quantity', 'description', 'reward_image', 'created_by'];
}
