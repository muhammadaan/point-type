<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    protected $table = 'division';

    protected $fillable = ['id', 'division_name', 'id_role_division','created_by', 'id_company'];


    
    public function company()
    {
    	return $this->belongsTo('App\Models\Company', 'id_company', 'id');
    }
}