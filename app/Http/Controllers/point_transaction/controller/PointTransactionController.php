<?php

namespace App\Http\Controllers\point_transaction\controller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PointTransaction;
use App\Models\PointWallet;
use App\Http\Controllers\point_wallet\helper\PointWalletHelper;

class PointTransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $point_transaction = PointTransaction::create([
            'point_in' => $request->point_in,
            'point_out' => $request->point_out,
            'member_id' => $request->member_id,
            'transaction_date' => new \DateTime(),
            'created_by' => \Auth::user()->name,
            'point_wallet_id' => $request->point_wallet_id,
            'point_type_id' => $request->point_type_id
        ]);

        PointWalletHelper::updateBalance((object) $request->all());
        return PointTransaction::where('id', $point_transaction->id)->with('pointType')->first();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
