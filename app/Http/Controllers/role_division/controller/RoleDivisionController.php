<?php

namespace App\Http\Controllers\role_division\controller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\RoleDivision;
use Auth;

class RoleDivisionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $role = RoleDivision::all();
    	return view('role_division.index', compact('role'));
    }

    public function create()
    {
    	return view('role_division.create');
    }


    public function store(Request $request)
    {
        $add = new RoleDivision;
        $add->role_code   = $request->role_code;
        $add->role_name   = $request->role_name;
        $add->created_by  = Auth::user('name')->name;
        
        $add->save();

        return redirect('/role-division/list')->with('message', 'Role Division Added');     
    }

    public function edit($id)
    {
        $role = RoleDivision::find($id);
    	return view('role_division.edit',compact('role'));
    }

    public function update(Request $request, $id)
    {
        $input  = $request->all();

        $edit   = RoleDivision::find($id);
        $edit->update($input); 

        return redirect('/role-division/list')->with('message', 'Role Division Edited');
    }

    public function destroy($id)
    {

        $delete   = RoleDivision::find($id);
        $delete->delete();

        return redirect('/role-division/list')->with('message', 'Role Division Deleted');
    }
}
