<?php

namespace App\Http\Controllers\Acl\Controller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
use Session;
use App\Models\Usergroup;
class AclController extends Controller
{

    public function index(){

    	$acl = Usergroup::all();
        return view('usergroup.index',compact('acl'));

        // return $acl;
    }

    public function create(){

    	return view('usergroup.create');
    }

    public function store(Request $request){
        
        	$input 	                      = $request->all();
        	$input['route_access_list']	  = json_encode(['routelist' => $request->input('routelist')]);
        	$input['user_id']	          = 0;
        	$input['permission_list_id']  = 0;
        	$input['status']	          = 1;

        	$store 	                      = \App\Models\Usergroup::create($input);

        	// return Redirect::to('/usergroup/list');
            return redirect('/usergroup/list')->with('message', 'Usergroup Added!');

        
    }

    public function edit($id){

    	$data 	= Usergroup::find($id);
    	
    	return view('usergroup.edit')->with(['data' => $data]);
    }

    public function update(Request $request, $id){
    	// return $request;
        try
        {
        	$input 	                    = $request->all();
        	$input['route_access_list'] = json_encode(['routelist' => $request->input('routelist')]);

        	$update 	                = Usergroup::find($id);
        	$update->update($input);

            return redirect('/usergroup/list')->with('message', 'Usergroup Edited!');

         }catch( \Exception $e){
            // Session::flash('error', 'Failed Input Data');
             return redirect('/usergroup/list');
        }
    	// return redirect::to('users-group');

    }

    public function destroy($id){

          $data= Usergroup::find($id);
          
          $data->delete();
          
          return redirect::to('/usergroup/list')->with('message', 'Usergroup Deleted!');
    
    }

}
