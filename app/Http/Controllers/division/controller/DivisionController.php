<?php

namespace App\Http\Controllers\division\controller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Division;
use App\Models\RoleDivision;
use App\Models\Company;
use Auth;


class DivisionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        if(Auth::user()->roles != 'superadmin')
        {
           $division = Auth::user()->company->divisions;
        }
        else
        {
            $division = Division::all();
        }
        return view('division.index', compact('division'));
    }

    public function create()
    {
        $role = RoleDivision::all();
        if(Auth::user()->roles != 'superadmin')
        {
           $companies = Auth::user()->company;
        }
        else
        {
            $companies = Company::all();
        }
    	return view('division.create', compact('role', 'companies'));
    }

    public function store(Request $request)
    {
 
       $add = new Division;
       $add->division_name       = $request->division_name;
       $add->id_company   = $request->id_company;
       $add->created_by         = Auth::user('name')->name;
       $add->save();

       return redirect('/division/list')->with('message', 'Division Added'); 
    }

    public function edit($id)
    {   
        $division = Division::find($id);
        $role = RoleDivision::all();
    	return view('division.edit', compact('division', 'role'));
    }

    public function update(Request $request, $id)
    {
        $input  = $request->all();
    
        $edit   = Division::find($id);
        $edit->update($input);

       

        return redirect('/division/list')->with('message', 'Division Edited');
    }

    public function destroy($id)
    {

        $delete   = Division::find($id);
        $delete->delete();

        return redirect('/division/list')->with('message', 'Division Deleted');
    }

}
