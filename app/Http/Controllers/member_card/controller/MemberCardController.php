<?php

namespace App\Http\Controllers\member_card\controller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Member;
use App\Models\MemberCard;
use Auth;

class MemberCardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $member_card = MemberCard::all();
        if(Auth::user()->roles != 'superadmin')
        {
            $members = Auth::user()->company->members;
        }
        else
        {
            $members = Member::all();
        }

        return view('member_card.list', compact('member_card', 'members'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $member = Member::all();
        return view('member_card.create',compact('company'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $add = new MemberCard;
        $add->card_name     = $request->card_name;

        $file = $request->file('background_image');
        $fileName = uniqid();
        $request->file('background_image')->move("assets/images/", $fileName);
        $add->background_image        = $fileName;

        $add->save();

        return redirect('/membercard/list')->with('message', 'Member Card Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $membercard = MemberCard::find($id);

        return view('member_card.edit', compact('membercard'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $edit = MemberCard::find($id);
        $edit->card_name     = $request->card_name;

        $avatarId     = $request->background_image;

        if($request->hasFile('background_image'))
        {     
            $file = $request->file('background_image');
            $fileName = uniqid();
            $request->file('background_image')->move("assets/images/", $fileName);
            $edit->background_image        = $fileName;
        
        }else{

            $fileName = $avatarId;
        } 
           
        // $edit->created_by    = Auth::user('name')->name;
        $edit->update();
        return redirect('/membercard/list')->with('message', 'Member Card Edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete   = MemberCard::find($id);
        $delete->delete();

        return redirect('/membercard/list')->with('message', 'Member Card Deleted');
    }

    public function generate(Request $request)
    {
        $member = Member::find($request->member_id);
        $member_card = MemberCard::find($request->card_id);

        $background_image_url = asset('assets/images') . '/' . $member_card->background_image;
        $name = $member->member_name;
        $code = $member->member_code;
        $company = $member->company->company_name;
        $font = base_path('assets/font.ttf');
        $image_info = getimagesize($background_image_url);
        $width = $image_info[0];
        $height = $image_info[1];

        if(exif_imagetype($background_image_url) == IMAGETYPE_JPEG)
        {
            $image = imagecreatefromjpeg($background_image_url);
            $color = imagecolorallocate($image, 255, 255, 255);
            imagettftext($image, 30, 0, 10, 40, $color, $font, $company);
            imagettftext($image, 30, 0, 10, $height-60, $color, $font, $code);
            imagettftext($image, 30, 0, 10, $height-20, $color, $font, $name);
            $fileName = $member->img_tmp != null ? $member->img_tmp : uniqid();
            $member->update(['img_tmp' => $fileName]);
            imagejpeg($image, base_path('assets/images/'.$fileName));
            imagedestroy($image);
        }
        elseif(exif_imagetype($background_image_url) == IMAGETYPE_PNG)
        {
            $image = imagecreatefrompng($background_image_url);
            $color = imagecolorallocate($image, 255, 255, 255);
            imagettftext($image, 30, 0, 10, 30, $color, $font, $company);
            imagettftext($image, 30, 0, 10, $height-60, $color, $font, $code);
            imagettftext($image, 30, 0, 10, $height-20, $color, $font, $name);
            $fileName = $member->img_tmp != null ? $member->img_tmp : uniqid();
            $member->update(['img_tmp' => $fileName]);
            imagepng($image, base_path('assets/images/'.$fileName));
            imagedestroy($image);   
        }
        return response(['message' => 'Successfully generate member card', 'image_url' => asset('assets/images') . '/' . $fileName]);
    }
}
