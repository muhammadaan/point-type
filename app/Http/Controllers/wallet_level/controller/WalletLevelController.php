<?php

namespace App\Http\Controllers\wallet_level\controller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\WalletLevel;
use App\Models\PointType;
use Auth;



class WalletLevelController extends Controller
{
    

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $wallet_level = WalletLevel::all();

        return view('wallet_level.index', compact('wallet_level'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {       
        $point = PointType::all();

        return view('wallet_level.create', compact('point'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $add                  = new WalletLevel;
        $add->level_name      = $request->level;
        $add->min_point       = $request->minimum;
        $add->point_type_id   = $request->type;
        $add->max_point       = $request->maximum;

        $file = $request->file('image');
        $fileName = uniqid().".png";
        $request->file('image')->move("assets/images/", $fileName);
        $add->level_image     = $fileName;

        $add->created_by      = Auth::user()->name; 
        $add->save();

        return redirect('/wallet_level/list')->with('message', 'Wallet Level Added');     

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $wallet = WalletLevel::find($id);
        $point = PointType::all();
        // return $wallet;
        return view('wallet_level.edit',compact('wallet','point'));
        // return view('wallet_level.edit',compact('wallet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $edit                  = WalletLevel::find($request->id);
        $edit->level_name      = $request->level;
        $edit->point_type_id      = $request->type;
        $edit->min_point       = $request->minimum;
        $edit->max_point       = $request->maximum;

        $imageId               = $request->avatar;

        if($request->hasFile('image'))
        {     
            $file = $request->file('image');
            $fileName = uniqid().".png";
            $request->file('image')->move("assets/images/", $fileName);
            $edit->level_image        = $fileName;
        
        }else{

            $fileName = $imageId;
        }
        $edit->created_by    = Auth::user('name')->name;
        $edit->update(); 

        return redirect('/wallet_level/list')->with('message', 'Wallet Level Edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete   = WalletLevel::find($id);
        $delete->delete();

        return redirect('/wallet_level/list')->with('message', 'Wallet Level Deleted');
    }
}
