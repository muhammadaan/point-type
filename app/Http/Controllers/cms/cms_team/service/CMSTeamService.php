<?php

namespace App\Http\Controllers\cms\cms_team\service;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CMSTeam;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Models\CmsMainBanner;

class CMSTeamService extends Controller
{
    public function index()
	{
	    $cms_team = CMSTeam::with('cms_home')->get();

			if (count($cms_team) > 0)
			{
				$result = array('result' => 1, 'message' => 'Data Succses', 'data' => $cms_team);
			}
			else
			{
				$result = array('result' => 0, 'message' => 'Data Failed', 'data' => 'Null');
			}

		  return ($result);	
    }
}
