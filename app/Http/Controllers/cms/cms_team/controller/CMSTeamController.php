<?php

namespace App\Http\Controllers\cms\cms_team\controller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CMSNews;
use App\Models\CMSServices;
use App\Models\CMSTeam;
use App\Models\CmsHome;
use App\Models\Company;
use Auth;

class CMSTeamController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
       $cms_team = CMSTeam::all();
       return view('cms.cms_team.index', compact('cms_team'));
    }

    public function show(Request $request)
    {
        $id = $request->get('id');
        $cms_team = CMSTeam::find($id);
        return $cms_team;
    }

    public function create()
    {
      // $cms_team = CMSTeam::all();
      $cms_team = CmsHome::all();
      return view('cms.cms_team.create', compact('cms_team'));
    }

    public function store(Request $request)  
    {
        $add                  = new CMSTeam;
        $add->cms_home_id     = $request->cms_home_id;
        $add->name            = $request->name;
        $add->url             = $request->url;
        $add->position        = $request->position;
        $add->team_desc       = $request->team_desc;
        $add->desc            = $request->desc;

        $file = $request->file('image');
        $fileName = uniqid().".png";
        $request->file('image')->storeAs("images/", $fileName);
        $add->image     = $fileName;

        $add->created_by      = Auth::user()->name; 
        $add->save();

        return redirect('/cms_team/list')->with('message', 'CMS Team Added'); 
    }

    public function edit($id)
    {
        $cms_team = CMSTeam::find($id);
        $cms_team1 = CmsHome::all();
        // return $cms_team;
        return view('cms.cms_team.edit', compact('cms_team','cms_team1'));
    }

    public function update(Request $request, $id)
    {
        $input  = $request->all();

        $edit   = CMSTeam::find($id);
        $edit->update($input); 

        return redirect('/cms_team/list')->with('message', 'CMS Team Edited');    
    }

    public function destroy($id)
    {

        $delete   = CMSTeam::find($id);
        $delete->delete();

        return redirect('/cms_team/list')->with('message', 'CMS Team Deleted');
    }

}