<?php

namespace App\Http\Controllers\cms\cms_location\controller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CmsLocation;
use App\Models\CmsHome;

class CMSLocationController extends Controller
{
    
	 public function __construct()
	 {
	 	 $this->middleware('auth');	
	 
	 }

	 public function index()
	 {
	 	 $query = CmsLocation::all();
	 	 return view('cms.cms_location.index', compact('query'));
	 }

	 public function create()
	 {	
	 	 $home = CmsHome::all();
	 	 return view('cms.cms_location.create', compact('home'));
	 }

	 public function store(Request $request)
	 {

	 	 $input 				= $request->all();
	 	 $input['created_by']	= \Auth::user()->name;

 		 define('UPLOAD_DIR', 'assets/images/');
         $image       			= uniqid().".jpeg";
         $imgpicture  			= $request->file('image')->storeAs("images/", $image);
 		 $input['image'] 		= $image;

	 	 $store = CmsLocation::create($input);

	 	 return redirect()->route('cms_location.index')->with('status', 'CMS Location Added ');

	 }

	 public function edit($id)
	 {

	 	 $location = CmsLocation::find($id);
	 	 $home 	   = CmsHome::all();

	 	 return view('cms.cms_location.edit', compact('location', 'home'));

	 }

	 public function update(Request $request, $id)
	 {

	 	 $input  = $request->all();
	 	 $update = CmsLocation::find($id);

	 	 if ($request->file('image'))
	 	 {
		 	 define('UPLOAD_DIR', 'assets/images/');
	         $image       			= uniqid().".jpeg";
	         $imgpicture  			= $request->file('image')->move('assets/images/', $image);
	 		 $input['image'] 		= $image;
	 	 }
	 	 else
	 	 {
	 	 	 $input['image']		= $update->image;
	 	 }

	 	 $update->update($input);

	 	 return redirect()->route('cms_location.index')->with('status', 'CMS Location Edited ');

	 }

	 public function destroy($id)
	 {
	 	 $delete = CmsLocation::find($id);
	 	 $delete->delete();

	 	 return redirect()->route('cms_location.index')->with('status', 'CMS Location Deleted ');
	 }


}
