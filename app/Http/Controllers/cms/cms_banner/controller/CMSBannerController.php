<?php

namespace App\Http\Controllers\cms\cms_banner\controller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CmsMainBanner;
use App\Models\CmsHome;

class CMSBannerController extends Controller
{
    
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{
		$cms = CmsMainBanner::all();
		return view('cms.cms_banner.index', compact('cms'));
	}

	public function create()
	{	
		$home = CmsHome::all();
		return view('cms.cms_banner.create', compact('home'));

	}

	public function store(Request $request)
	{
		
		$input = $request->all();

		define('UPLOAD_DIR', 'assets/images/');
        $image       	= uniqid().".jpeg";
        $imgpicture  	= $request->file('image')->storeAs("images/", $image);
		$input['image'] = $image;

		if($request->status == 'on')
        {
            $input['status']     = '1';

        }
        else
        {
            $input['status']     = '0';
        }

		$store = CmsMainBanner::create($input);

		return redirect()->route('cms_banner.index')->with('status', 'CMS Banner Added ');
	}


	public function edit($id)
	{	
		$cms 	= CmsMainBanner::find($id);
		$home 	= CmsHome::all();
		return view('cms.cms_banner.edit', compact('cms', 'home'));

	}

	public function update(Request $request, $id)
	{
		$input 	= $request->all();
		$update = CmsMainBanner::find($id);

		if($request->file('image'))
		{
			define('UPLOAD_DIR', 'assets/images/');
	        $image       	= uniqid().".jpeg";
	        $imgpicture  	= $request->file('image')->move('assets/images/', $image);
			$input['image'] = $image;
			
		}
		else
		{
			$input['image'] = $update->image;
		}

		if($request->status == 'on')
        {
            $input['status']     = '1';

        }
        else
        {
            $input['status']     = '0';
        }

		$update->update($input); 

		return redirect()->route('cms_banner.index')->with('status', 'CMS Banner Edited ');
	}

	public function destroy($id)
	{
		$data  = CmsMainBanner::find($id);
		$data->delete();

		return redirect()->route('cms_banner.index')->with('status', 'CMS Banner Deleted ');
	}


}
