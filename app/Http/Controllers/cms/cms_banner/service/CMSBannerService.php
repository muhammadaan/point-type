<?php

namespace App\Http\Controllers\cms\cms_banner\service;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\CmsMainBanner;

class CMSBannerService extends Controller
{
    
	public function index()
	{

		$query = CmsMainBanner::with('cms_home')->get();
		
		if(count($query) > 0)
		{
			$result = array('result' => 1, 'message' => 'Data Success', 'data' => $query);
		}
		else
		{
			$result = array('result' => 0, 'message' => 'Data Failed', 'data' => 'null');
		}

		return $result;

	}

}
