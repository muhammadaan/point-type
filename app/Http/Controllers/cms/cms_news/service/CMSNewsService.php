<?php

namespace App\Http\Controllers\cms\cms_news\service;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CMSNews;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Models\CmsMainBanner;

class CMSNewsService extends Controller
{

	public function index()
	{
	    $cms_news = CMSNews::with('cms_home')->get();

			if (count($cms_news) > 0)
			{
				$result = array('result' => 1, 'message' => 'Data Succses', 'data' => $cms_news);
			}
			else
			{
				$result = array('result' => 0, 'message' => 'Data Failed', 'data' => 'Null');
			}

		  return json_encode($result);	
    }
}

