<?php

namespace App\Http\Controllers\cms\cms_news\controller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CMSNews;
use App\Models\CMSServices;
use App\Models\CMSTeam;
use App\Models\CmsHome;
use App\Models\Company;
use Auth;

class CMSNewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
       $cms_news = CMSNews::all();
       return view('cms.cms_news.index', compact('cms_news'));
    }

    public function show(Request $request)
    {
        $id = $request->get('id');
        $cms_news = CMSNews::find($id);
        return $cms_news;
    }

    public function create()
    {
      $cms_news = CmsHome::all();
      $tags       = \App\Models\Tag::orderBy('tag', 'asc')->get();
      return view('cms.cms_news.create', compact('cms_news','tags'));
    }

    public function store(Request $request)  
    {
        $add                  = new CMSNews;
        $add->cms_home_id     = $request->cms_home_id;
        $add->title           = $request->title;
        $add->desc            = $request->desc;
        $add->name            = $request->name;
        $add->category        = $request->category;
        // $add->tags            = $request->tags;
        $add['tags']           = json_encode($request->input('tag'));

        $file = $request->file('image');
        $fileName = uniqid().".png";
        $request->file('image')->storeAs("images/", $fileName);
        $add->image     = $fileName;

        $add->created_by      = Auth::user()->name; 
        $add->save();

        return redirect('/cms_news/list')->with('message', 'CMS News Added');
    }

    public function edit($id)
    {
        $cms_news = CMSNews::find($id);
        $cms_news1 = CmsHome::all();
        $tags       = \App\Models\Tag::orderBy('tag', 'asc')->get();
        return view('cms.cms_news.edit', compact('cms_news','cms_news1','tags'));
    }

    public function update(Request $request, $id)
    {
        $input  = $request->all();
        $input['tags']           = json_encode($request->input('tag'));

        $edit   = CMSNews::find($id);
        $edit->update($input); 

        // return $edit;
        return redirect('/cms_news/list')->with('message', 'CMS News Edited');    
    }

    public function destroy($id)
    {

        $delete   = CMSNews::find($id);
        $delete->delete();

        return redirect('/cms_news/list')->with('message', 'CMS News Deleted');
    }
}
