<?php

namespace App\Http\Controllers\cms\cms_services\controller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CMSNews;
use App\Models\CMSServices;
use App\Models\CMSTeam;
use App\Models\CmsHome;
use App\Models\Company;
use Auth;

class CMSServicesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
       $cms_services = CMSServices::all();
       return view('cms.cms_services.index', compact('cms_services'));
    }

    public function show(Request $request)
    {
        $id = $request->get('id');
        $cms_services = CMSServices::find($id);
        return $cms_services;
    }

    public function create()
    {
      $cms_services = CmsHome::all();
      return view('cms.cms_services.create', compact('cms_services'));
    }

    public function store(Request $request)  
    {
        $add                  = new CMSServices;
        $add->cms_home_id     = $request->cms_home_id;
        $add->service_name   = $request->service_name;
        $add->url             = $request->url;
        $add->desc            = $request->desc;

        $file = $request->file('image');
        $fileName = uniqid().".png";
        $request->file('image')->storeAs("images/", $fileName);
        $add->image     = $fileName;

        $add->created_by      = Auth::user()->name; 
        $add->save();
        return redirect('/cms_services/list')->with('message', 'CMS Services Added');
    }

    public function edit($id)
    {
        $cms_services = CMSServices::find($id);
        $cms_services1 = CmsHome::all();
        // return $cms_services;
        return view('cms.cms_services.edit', compact('cms_services','cms_services1'));
    }

    public function update(Request $request, $id)
    {
        $input  = $request->all();

        $edit   = CMSServices::find($id);
        $edit->update($input); 

        return redirect('/cms_services/list')->with('message', 'CMS Services Edited');    
    }

    public function destroy($id)
    {

        $delete   = CMSServices::find($id);
        $delete->delete();

        return redirect('/cms_services/list')->with('message', 'CMS Services Deleted');
    }
}
