<?php

namespace App\Http\Controllers\cms\cms_services\service;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CMSServices;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Models\CmsMainBanner;

class CMSServicesService extends Controller
{
    public function index()
	{
	    
	    $input = Input::all();

	    $Validator = Validator::make(Input::all(),[
			'company_id' 	=> 'required'
		]);

		if($Validator->fails())
		{
			$result = array("result" => 0, "error" => 'Data Not Complete', "errorField" => $Validator->errors());
		}
		else
		{

			try {
				
				$company_id = $input['company_id'];

		    	$cms_services = CMSServices::with('cms_home')
		    					->whereHas('cms_home', function ($q) use ($company_id){
		    						$q->where('company_id', '=', $company_id);
		    					})->first();

				if(count($cms_services) > 0)
				{
					$result = array('result' => 1, 'message' => 'Data Succses', 'data' => $cms_services);
				}
				else
				{
					$result = array('result' => 0, 'message' => 'Data Failed', 'data' => 'Null');
				}
			
			} catch (Exception $e) {

				$result = array('result' => 0, 'message' => 'Data Failed', 'data' => $e->getMessage()." ".$e->getFIle()." ".$e->getLine());
				
			}

		}
	   
	   return $result;	
    }
}