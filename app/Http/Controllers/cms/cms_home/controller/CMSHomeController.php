<?php

namespace App\Http\Controllers\cms\cms_home\controller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CmsHome;
use App\Models\Company;

class CMSHomeController extends Controller
{
    
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{
		$cms = CmsHome::all();
		return view('cms.cms_home.index', compact('cms'));
	}

	public function create()
	{	
		$company = Company::all();
		return view('cms.cms_home.create', compact('company'));

	}

	public function store(Request $request)
	{
		
		$input = $request->all();

		if($request->banner_main_status == 'on')
        {
            $input['banner_main_status']     = '1';
        }
        else
        {
            $input['banner_main_status']     = '0';
        }

        if($request->services_status == 'on')
        {
            $input['services_status']     = '1';
        }
        else
        {
            $input['services_status']     = '0';
        }

        if($request->team_status == 'on')
        {
            $input['team_status']     = '1';
        }
        else
        {
            $input['team_status']     = '0';
        }

        if($request->about_status == 'on')
        {
            $input['about_status']     = '1';
        }
        else
        {
            $input['about_status']     = '0';
        }

        if($request->news_status == 'on')
        {
            $input['news_status']     = '1';
        }
        else
        {
            $input['news_status']     = '0';
        }

        if($request->portfolio_status == 'on')
        {
            $input['portfolio_status']     = '1';
        }
        else
        {
            $input['portfolio_status']     = '0';
        }

        if($request->location_status == 'on')
        {
            $input['location_status']     = '1';
        }
        else
        {
            $input['location_status']     = '0';
        }

        $store = CmsHome::create($input);
		return redirect()->route('cms_home.index')->with('status', 'CMS Home Added ');
	}

	public function edit($id)
	{	
		$cmshome = CmsHome::find($id);
		$company = Company::all();
		return view('cms.cms_home.edit', compact('cmshome', 'company'));

	}

	public function update(Request $request, $id)
	{
		$input 	= $request->all();
		$update = CmsHome::find($id);

		if($request->banner_main_status == 'on')
        {
            $input['banner_main_status']     = '1';
        }
        else
        {
            $input['banner_main_status']     = '0';
        }

        if($request->services_status == 'on')
        {
            $input['services_status']     = '1';

        }
        else
        {
            $input['services_status']     = '0';
        }

        if($request->team_status == 'on')
        {
            $input['team_status']     = '1';

        }
        else
        {
            $input['team_status']     = '0';
        }

        if($request->about_status == 'on')
        {
            $input['about_status']     = '1';

        }
        else
        {
            $input['about_status']     = '0';
        }

        if($request->news_status == 'on')
        {
            $input['news_status']     = '1';

        }
        else
        {
            $input['news_status']     = '0';
        }

        if($request->portfolio_status == 'on')
        {
            $input['portfolio_status']     = '1';
        }
        else
        {
            $input['portfolio_status']     = '0';
        }

        if($request->location_status == 'on')
        {
            $input['location_status']     = '1';
        }
        else
        {
            $input['location_status']     = '0';
        }

        if($request->has('contact_gps_coor'))
        {
        	$input['contact_gps_coor']     = $request->contact_gps_coor;
        }
        else
        {
        	$input['contact_gps_coor']		= $update->contact_gps_coor;
        }


		$update->update($input); 

		return redirect()->route('cms_home.index')->with('status', 'CMS Home Edited ');
	}

	public function destroy($id)
	{
		$data  = CmsHome::find($id);
		$data->delete();

		return redirect()->route('cms_home.index')->with('status', 'CMS Home Deleted ');
	}

	public function show(Request $request)
	{
		$id 		= $request->get('id');
		$cmshome 	= CmsHome::find($id);
		$response = [
			'banner_status' 	 => $cmshome->banner_main_status == 1 ? 'Active' : 'InActive',
			'company_name' 		 => $cmshome->company->company_name,
			'contact_email' 	 => $cmshome->contact_email,
			'contact_facebook'	 => $cmshome->contact_facebook,
			'contact_gps_coor'	 => $cmshome->contact_gps_coor,
			'contact_instagram'  => $cmshome->contact_instagram,
			'contact_phone'  	 => $cmshome->contact_phone,
			'contact_twitter'  	 => $cmshome->contact_twitter,
			'contact_gps_coor' 	 => $cmshome->contact_gps_coor,
			'location_status'  	 => $cmshome->location_status == 1 ? 'Active' : 'In Active',
			'portfolio_status'   => $cmshome->portfolio_status == 1 ? 'Active' : 'In Active',
			'news_status'  	 	 => $cmshome->news_status == 1 ? 'Active' : 'In Active',
			'about_status'  	 => $cmshome->about_status == 1 ? 'Active' : 'In Active',
			'team_status'  	 	 => $cmshome->team_status == 1 ? 'Active' : 'In Active',
			'services_status'  	 => $cmshome->services_status == 1 ? 'Active' : 'In Active',
        ];

		return $response;	
	}


}
