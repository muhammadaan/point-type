<?php

namespace App\Http\Controllers\cms\cms_home\service;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\CmsHome;

class CMSHomeService extends Controller
{
    
	public function index()
	{

		$input = Input::all();

		$Validator = Validator::make(Input::all(),[
			'company_id' 	=> 'required'
		]);

		if($Validator->fails())
		{
			$result = array("result" => 0, "error" => 'Data Not Complete', "errorField" => $Validator->errors());
		}
		else
		{
			try {
					
				$cmshome = CmsHome::with('company')->where('company_id', $input['company_id'])->first();

				if(count($cmshome) > 0)
				{
					$result = array('result' => 1, 'message' => 'Data Success', 'data' => $cmshome);
				}
				else
				{
					$result = array('result' => 0, 'message' => 'Data Failed', 'data' => 'null');	
				}
				

			} catch (Exception $e) {
				
				$result = array('result' => 0, 'message' => 'Data Failed', 'data' => $e->getMessage()." ".$e->getFIle()." ".$e->getLine());
			}	

		}
		
	   return $result;
	}


}
