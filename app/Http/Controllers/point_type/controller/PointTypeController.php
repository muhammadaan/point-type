<?php

namespace App\Http\Controllers\point_type\controller;

use Auth;
use App\Models\PointType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PointTypeController extends Controller
{
    public function index(Request $request)
    {
    	$point_types = PointType::all();
        
    	return view('point_type.index', compact('point_types'));
    }

    public function create()
    {
    	return view('point_type.create');
    }

    public function edit($id)
    {
    	$point_type = PointType::find($id);
    	return view('point_type.edit', compact('point_type'));
    }

    public function store(Request $request)
    {
    	$request->merge([ 'created_by' => Auth::user()->name ]);
    	PointType::create($request->all());
    	return redirect(route('point_type.index'))->with('message', 'Point Type added');
    }

    public function update(Request $request, $id)
    {
    	$point_type = PointType::find($id);
    	$point_type->update($request->all());
    	return redirect(route('point_type.index'))->with('message', 'Point Type updated');
    }

    public function destroy($id)
    {
        $point_type = PointType::find($id);
        $point_type->delete();
        return redirect(route('point_type.index'))->with('message', 'Point Type Deleted');
    }
}
