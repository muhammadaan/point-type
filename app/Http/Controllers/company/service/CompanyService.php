<?php

namespace App\Http\Controllers\company\service;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Models\Company;

class CompanyService extends Controller
{
    
	 public function index()
	 {
	 	 $company = Company::with('divisions', 'member')->get();

	 	 if(count($company) > 0)
	 	 {
	 	 	 $result = array('result' => 1, 'message' => 'Data Success', 'data' => $company);
	 	 }
	 	 else
	 	 {
	 	 	 $result = array('result' => 1, 'message' => 'Data Success', 'data' => 'null' );	

	 	 }

	 	 return $company;
	 }

}
