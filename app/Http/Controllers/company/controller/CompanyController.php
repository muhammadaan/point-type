<?php

namespace App\Http\Controllers\company\controller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Company;
use Auth;
use Carbon\Carbon;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $company = Company::all();
    	return view('company.index', compact('company'));
    }

    public function show(Request $request)
    {
        $id = $request->get('id');
        $company = Company::find($id);
        return $company;
    }

    public function create()
    {
    	$company = Company::all();
        return view('company.create', compact('company'));
    }


    public function store(Request $request)  
    {
        
        $fileName = uniqid().".png";
        $request->file('company_logo')->move("assets/images/", $fileName);
        $request->merge([
            'created_by' => Auth::user('name')->name,  
        ]);
        $data = $request->except('company_logo');
        $data['company_logo'] = $fileName;

        if($request->license_status == 'on')
        {
            $data['license_status']     = '1';
        }
        else
        {
            $data['license_status']     = '0';
        }

        Company::create($data);
        return redirect('/company/list')->with('message', 'Company Added');
    }

    public function edit($id)
    {
        $company = Company::find($id);
    	return view('company.edit', compact('company'));
    }

    public function update(Request $request, $id)
    {
        $input  = $request->all();

        if($request->license_status == 'on')
        {
            $input['license_status']     = '1';
        }
        else
        {
            $input['license_status']     = '0';
        }

        $edit   = Company::find($id);
        $edit->update($input); 

        return redirect('/company/list')->with('message', 'Company Edited');
    
    }

    public function destroy($id)
    {

        $delete   = Company::find($id);
        $delete->delete();

        return redirect('/company/list')->with('message', 'Company Deleted');
    }
}
