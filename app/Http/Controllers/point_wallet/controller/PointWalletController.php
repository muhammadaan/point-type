<?php

namespace App\Http\Controllers\point_wallet\controller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PointWallet;
use App\Models\PointType;
use App\Models\PointTransaction;
use App\Models\Member;
use App\Models\WalletLevel;
use App\Models\Company;
use App\Http\Controllers\point_wallet\helper\PointWalletHelper;
use Auth;
use DB;



class PointWalletController extends Controller
{
    

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $point_wallet = PointWallet::all();

        return view('point_wallet.index', compact('point_wallet'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        if(Auth::user()->roles != 'superadmin')
        {
            $member = Auth::user()->company->member;
            $company = Auth::user()->company;
        }
        else
        {
            $member = Member::all();
            $company = Company::all();
        }
        $point_type = PointType::all();
        return view('point_wallet.create',compact('member','company', 'point_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $add                  = new PointWallet;
        $add->member_id       = $request->member;
        $add->wallet_level_id = PointWalletHelper::getWalletLevelId($request->current, $request->point_type_id);
        $add->created_by      = Auth::user()->name;
        $add->updated_by      = Auth::user()->name;
        $check = PointType::find($request->point_type_id);
        if($check->point_type == 'KPI')
        {
            $add->current_point_kpi = $request->current;
        }
        else
        {
            $add->current_point = $request->current;
        }
        $add->save();

        PointWalletHelper::createTransaction((object) $request->all(), $add->id);
        
        return redirect('/point_wallet/list')->with('message', 'Point Wallet Added');                 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete   = PointWallet::find($id);
        $delete->delete();

        return redirect('/point_wallet/list')->with('message', 'Point Wallet Deleted');
    }

    
    public function checkMember(Request $request)
    {
        if (PointWallet::where('member_id' , $request->member)->count() > 0) {
            echo "false";
        } else {
            echo "true";
        }
    }

    public function showTransaction(Request $request)
    {
        $transaction = PointTransaction::where('point_wallet_id', $request->point_wallet_id)->orderBy('transaction_date', 'asc')->with('pointType')->get();
        $point_type = PointType::all();

        $transaction = !empty($transaction) ? $transaction : "There's no transaction";
        return response(['transaction' => $transaction, 'point_type' => $point_type]);
    }
}
