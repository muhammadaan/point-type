<?php

namespace App\Http\Controllers\point_wallet\helper;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\WalletLevel;
use App\Models\PointTransaction;
use App\Models\PointWallet;
use App\Models\PointType;

class PointWalletHelper extends Controller
{
    public static function getWalletLevelId($current_point, $point_type_id)
    {
    	$wallet_levels = WalletLevel::where('point_type_id', $point_type_id)->get();
    	foreach($wallet_levels as $wallet_level)
    	{
    		if($current_point >= $wallet_level->min_point && $current_point <= $wallet_level->max_point)
    		{
    			return $wallet_level->id != null? $wallet_level->id: 0;
    		}
    	}
    }

    public static function getWalletLevelKpiId($current_point)
    {
        $wallet_levels = WalletLevel::all();
        foreach($wallet_levels as $wallet_level)
        {
            if($current_point >= $wallet_level->min_point && $current_point <= $wallet_level->max_point)
            {
                if($wallet_level->point_type->point_type == 'KPI')
                {
                    return $wallet_level->id != null? $wallet_level->id: 0;
                }  
            }
        }
    }

    public static function createTransaction($data, $point_wallet_id)
    {
        
        PointTransaction::create([
            'point_in' => $data->current,
            'member_id' => $data->member,
            'transaction_date' => new \DateTime(),
            'created_by' => \Auth::user()->name,
            'point_wallet_id' => $point_wallet_id,
            'point_type_id' => $data->point_type_id
        ]);

    }

    public static function updateBalance($data)
    {
        $wallet = PointWallet::find($data->point_wallet_id);
        $check = PointType::find($data->point_type_id);
        if($check->point_type == 'KPI')
        {
            $wallet->update([
                'current_point_kpi' => $wallet->current_point_kpi + $data->point_in - $data->point_out
            ]);
        }
        else
        {   
            $wallet->update([
                'current_point' => $wallet->current_point + $data->point_in - $data->point_out,
                'wallet_level_id' => self::getWalletLevelId($wallet->current_point + $data->point_in - $data->point_out, $data->point_type_id)
            ]);
        }
    }
}
