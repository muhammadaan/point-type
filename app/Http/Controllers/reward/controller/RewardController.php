<?php

namespace App\Http\Controllers\reward\controller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Reward;
use Auth;

class RewardController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $reward = Reward::all();

        return view('reward.list', compact('reward'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $reward = Reward::all();
        return view('reward.create', compact('reward'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)  
    {

        $add = new Reward;
        $add->reward_name            =  $request->reward_name;
        $add->point_required         =  $request->point_required;
        $add->quantity               =  $request->quantity;
        $add->description            =  $request->description;
        // $add->reward_image           =  $request->reward_image;

        $file = $request->file('reward_image');
        $fileName = uniqid().".png";
        $request->file('reward_image')->move("assets/images/", $fileName);
        $add->reward_image     = $fileName;

        $add->created_by             =  Auth::user('name')->name;
        $add->save();

        return redirect('/reward/list')->with('message', 'Reward Added');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reward = Reward::find($id);
        return view('reward.edit', compact('reward'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $edit                       = Reward::find($request->id);
        $edit->reward_name          = $request->reward_name;
        $edit->point_required       = $request->point_required;
        $edit->quantity             = $request->quantity;
        $edit->description          = $request->description;

        $imageId                    = $request->avatar;

        if($request->hasFile('image'))
        {     
            $file = $request->file('reward_image');
            $fileName = uniqid().".png";
            $request->file('reward_image')->move("assets/images/", $fileName);
            $edit->reward_image        = $fileName;
        
        }else{

            $fileName = $imageId;
        }
        $edit->created_by    = Auth::user('name')->name;
        $edit->update(); 

        return redirect('/reward/list')->with('message', 'Reward Edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $delete   = Reward::find($id);
        $delete->delete();

        return redirect('/reward/list')->with('message', 'Reward Deleted');
    }
}
