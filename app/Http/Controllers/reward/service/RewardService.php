<?php

namespace App\Http\Controllers\reward\service;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Reward;

class RewardService extends Controller
{
    

	public function index()
	{

		$reward = Reward::all();

		if (count($reward) > 0)
		{
			$result = array('result' => 1, 'message' => 'Data Succses', 'data' => $reward);
		}
		else
		{
			$result = array('result' => 0, 'message' => 'Data Failed', 'data' => 'Null');
		}

	  return json_encode($result);	

	}


}
