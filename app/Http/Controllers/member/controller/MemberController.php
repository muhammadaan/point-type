<?php

namespace App\Http\Controllers\member\controller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\Division;
use App\Models\RoleDivision;
use App\Models\Company;
use App\Models\Usergroup;
use App\Models\Member;
use App\User;
use Auth;


class MemberController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(Auth::user()->roles != 'superadmin')
        {
           $member = Auth::user()->company->member;
        }
        else
        {
            $member = Member::all();
        }
        return view('member.index', compact('member'));
    }

    public function show(Request $request)
    {
        $id = $request->id;
        $member = Member::find($id);
        $response = [
            'member' => $member,
            'company' => $member->company,
            'division' => $member->division,
            'role' => $member->role_division
        ];
        return $response;
    }

    public function create()
    {
        if(Auth::user()->roles != 'superadmin')
        {
            $division = Auth::user()->company->divisions;
            if(Auth::user()->roles == 'admin_company')
            {   
                $role = Usergroup::where('name', '!=', 'superadmin')->get();
            }
            else
            {
                $role = [];
            }
            $company = Auth::user()->company;
        }
        else
        {
            $division = Division::all();
            $role = Usergroup::all();
            $company = Company::all();
        }

    	return view('member.create', compact('division', 'role', 'company'));
    }

    public function store(Request $request)
    {
        
        $user                   = new User;
        $user->name             = $request->member_name;
        $user->email            = $request->email;
        $user->username         = $request->email;
        $user->password         = Hash::make($request->password);
        $user->remember_token   = $request->remember;
        $user->roles            = $request->role;
        $user->id_company       = $request->company;
        $user->save();
        $user_id = $user->id;
        
        $add = new Member;
        $add->member_code   = $request->member_code;
        $add->member_name   = $request->member_name;
        $add->email         = $request->email;
        $add->password      = Hash::make($request->password);
        $add->status        = $request->status;

        $file = $request->file('avatar');
        $fileName = uniqid().".png";
        $request->file('avatar')->move("assets/images/", $fileName);
        $add->avatar        = $fileName;

        $add->id_company    = $request->company;
        $add->id_division   = $request->division;
        $add->user_id       = $user->id;
        $add->created_by    = Auth::user('name')->name;
        $add->user_id       = $user_id;
        $add->save();

        return redirect('/member/list')->with('message', 'Member Added');
        

    }

    public function edit($id)
    {
        $member = Member::find($id);
        $division = Division::all();
        $role = Usergroup::all();
        $company = Company::all();

    	return view('member.edit', compact('member', 'division', 'role', 'company'));
    }

    public function update(Request $request, $id)
    {

        $edit = Member::find($id);
        $edit->member_code   = $request->member_code;
        $edit->member_name   = $request->member_name;
        $edit->email         = $request->email;
        
        $edit->status        = $request->status;

        $avatarId     = $request->avatar;

        if($request->hasFile('avatar'))
        {     
            $file = $request->file('avatar');
            $fileName = uniqid().".png";
            $request->file('avatar')->move("assets/images/", $fileName);
            $edit->avatar        = $fileName;
        
        }else{

            $fileName = $avatarId;
        } 
           
        $edit->id_company    = $request->company;
        $edit->id_division   = $request->division;
        $edit->created_by    = Auth::user('name')->name;
        $edit->update();

        return redirect('/member/list')->with('message', 'Member Edited');
    }

    public function destroy($id)
    {

        $delete   = Member::find($id);
        $delete->delete();

        return redirect('/member/list')->with('message', 'Member Deleted');
    }
}
