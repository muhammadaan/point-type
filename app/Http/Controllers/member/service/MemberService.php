<?php

namespace App\Http\Controllers\member\service;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Member;

class MemberService extends Controller
{
   	
	public function index()
	{

		$getMemberAll = Member::with('division','company','user')->get();

		if (count($getMemberAll) > 0)
		{

			$result = array('result' => 1, 'message' => 'Data Succses', 'data' => $getMemberAll);
		}
		else 
		{
			$result = array('result' => 0, 'message' => 'Data Failed', 'data' => 'Null');
		
		} 

	   return json_encode($result);	

	}


}
