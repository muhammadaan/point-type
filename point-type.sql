-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 18, 2018 at 08:34 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `point-type`
--

-- --------------------------------------------------------

--
-- Table structure for table `cms_home`
--

CREATE TABLE IF NOT EXISTS `cms_home` (
  `id` int(10) unsigned NOT NULL,
  `banner_main_status` int(11) NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `services_status` int(11) NOT NULL,
  `team_status` int(11) NOT NULL,
  `about_status` int(11) NOT NULL,
  `news_status` int(11) NOT NULL,
  `portfolio_status` int(11) NOT NULL,
  `location_status` int(11) NOT NULL,
  `contact_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_gps_coor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_instagram` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_twitter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_facebook` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_home`
--

INSERT INTO `cms_home` (`id`, `banner_main_status`, `company_id`, `services_status`, `team_status`, `about_status`, `news_status`, `portfolio_status`, `location_status`, `contact_phone`, `contact_email`, `contact_gps_coor`, `contact_instagram`, `contact_twitter`, `contact_facebook`, `created_at`, `updated_at`) VALUES
(1, 1, 9, 1, 0, 1, 0, 1, 1, '085263508244', 'aan@solutechglobal.com', '-6.4024844 106.7942405', 'muhammad_aan', 'aan', 'aan', '2018-07-03 00:30:46', '2018-07-04 21:12:49'),
(2, 1, 9, 1, 1, 1, 1, 1, 1, 'fwefwefwefwfewfw', 'fwefwefwe', 'ewfwefwef', 'wefewfew', 'efwefwe', 'wefewfwefwe', '2018-07-03 21:47:39', '2018-07-03 21:47:39'),
(3, 1, 9, 1, 0, 0, 1, 0, 0, '235432534', 'aan@gmail.com', '-6.263551199999998 106.97323489999997', 'sdfsdf', 'dsfsf', 'sdfsfs', '2018-07-12 21:11:29', '2018-07-12 21:16:32');

-- --------------------------------------------------------

--
-- Table structure for table `cms_location`
--

CREATE TABLE IF NOT EXISTS `cms_location` (
  `id` int(10) unsigned NOT NULL,
  `cms_home_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `region` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gps` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_location`
--

INSERT INTO `cms_location` (`id`, `cms_home_id`, `name`, `country`, `region`, `address`, `image`, `url`, `gps`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'ggergre', 'ergerg', 'regergre', 'Jalan Raya RTM', '5b3c5146601be.jpeg', 'gregerre', 'rgregergregre', 'fajar', '2018-07-03 21:47:02', '2018-07-03 21:47:02'),
(2, 1, 'xcvfdsfdfds', 'sdfgsdssd', 'sdsdfsdsdf', 'sdfsdfsdfsdf', '5b3c839b73793.jpeg', 'dfsdfsdf', '2355435', 'fajar', '2018-07-04 01:21:47', '2018-07-04 01:21:47'),
(3, 1, 'sdfsdf', 'dsfsdfsd', 'sdfdsf', 'sdfsdfds', '5b44748690e3c.jpeg', 'fwefwef', '-6.245447299999999 106.90044720000003', 'fajar', '2018-07-10 01:55:34', '2018-07-10 01:55:34');

-- --------------------------------------------------------

--
-- Table structure for table `cms_main_banner`
--

CREATE TABLE IF NOT EXISTS `cms_main_banner` (
  `id` int(10) unsigned NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `cms_home_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_main_banner`
--

INSERT INTO `cms_main_banner` (`id`, `image`, `url`, `status`, `cms_home_id`, `created_at`, `updated_at`) VALUES
(1, '5b3b267017770.jpeg', '0', 1, 1, '2018-07-03 00:32:00', '2018-07-03 00:32:00'),
(2, '5b3c517679705.jpeg', '0', 0, 1, '2018-07-03 21:47:50', '2018-07-03 21:47:50'),
(3, '5b3c779ae6e65.jpeg', '0', 0, 2, '2018-07-04 00:30:34', '2018-07-04 00:30:34'),
(4, '5b3c7b6bafb0e.jpeg', '0', 1, 2, '2018-07-04 00:46:51', '2018-07-04 01:02:25'),
(5, '5b447640e957a.jpeg', '0', 1, 1, '2018-07-10 02:02:56', '2018-07-10 02:02:56'),
(6, '5b484edcb3004.jpeg', NULL, 1, 1, '2018-07-13 00:03:56', '2018-07-13 00:03:56');

-- --------------------------------------------------------

--
-- Table structure for table `cms_news`
--

CREATE TABLE IF NOT EXISTS `cms_news` (
  `id` int(10) unsigned NOT NULL,
  `cms_home_id` int(10) unsigned NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tags` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_news`
--

INSERT INTO `cms_news` (`id`, `cms_home_id`, `title`, `desc`, `name`, `category`, `image`, `tags`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'Bakrie', 'Consultant', 'Wisma Bakrie', 'Consultant', 'dfgfdsgsfsd.jpg', 'kuningan', 'suminah', NULL, NULL),
(2, 2, 'Solutech Kuningan', 'IT Consultant', 'Consultant', 'IT', '5b3aeefc18751.png', 'Consultant', 'fajar', '2018-07-02 20:35:24', '2018-07-02 20:53:03'),
(3, 1, 'gregrege', 'regregreger', 'regergerg', 'regergre', '5b3c51347de37.png', 'gregerer', 'fajar', '2018-07-03 21:46:44', '2018-07-03 21:46:44'),
(4, 2, 'dsfgsdfg', 'fgfdsgdf', 'gfdgfd', 'gfgdfg', '5b3eebeec327f.png', '["fgfsd","gergerg","aan"]', 'fajar', '2018-07-05 21:11:26', '2018-07-05 21:27:21'),
(5, 2, 'dgfsd', 'gergergre', 'egregre', 'gerger', '5b4476320b745.png', '["grrfrfr"]', 'fajar', '2018-07-10 02:02:42', '2018-07-10 02:02:42');

-- --------------------------------------------------------

--
-- Table structure for table `cms_services`
--

CREATE TABLE IF NOT EXISTS `cms_services` (
  `id` int(10) unsigned NOT NULL,
  `cms_home_id` int(10) unsigned NOT NULL,
  `service_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_services`
--

INSERT INTO `cms_services` (`id`, `cms_home_id`, `service_name`, `image`, `url`, `desc`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'consultant', 'fdbgdfgdfgfd.jpg', 'www.google.com', 'pencarian', 'supeno', NULL, NULL),
(2, 1, 'accounting', '5b3afb473228c.png', 'www.solutechglobal.com', 'searchengine kuningan', 'fajar', '2018-07-02 21:27:51', '2018-07-02 21:31:23'),
(3, 1, 'rgregre', '5b3c51255cb7e.png', 'regegre', 'regregerg', 'fajar', '2018-07-03 21:46:29', '2018-07-03 21:46:29'),
(4, 2, 'rgreer', '5b4474cd4d849.png', 'fdsfsdfsd', 'fsdfsdfsdf', 'fajar', '2018-07-10 01:56:45', '2018-07-10 01:56:45');

-- --------------------------------------------------------

--
-- Table structure for table `cms_team`
--

CREATE TABLE IF NOT EXISTS `cms_team` (
  `id` int(10) unsigned NOT NULL,
  `cms_home_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `team_desc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_team`
--

INSERT INTO `cms_team` (`id`, `cms_home_id`, `name`, `image`, `url`, `position`, `desc`, `team_desc`, `created_by`, `created_at`, `updated_at`) VALUES
(2, 2, 'IDMarco', '5b3adf6342f4f.png', 'www.idmarco.com', 'CEO', 'Penjualan Online', NULL, 'fajar', '2018-07-02 19:28:51', '2018-07-02 19:28:51'),
(3, 2, 'Solutech Kuningan', '5b3ae3b61c7ed.png', 'www.solutech.id', 'Company', 'IT Solution', NULL, 'fajar', '2018-07-02 19:47:18', '2018-07-02 20:00:33'),
(4, 9, 'dfgfdgdfgd', '5b3b2390090bd.png', 'fdgdg', 'dfdgfdgfd', 'fdgfdgdf', NULL, 'fajar', '2018-07-03 00:19:44', '2018-07-03 00:19:44'),
(5, 1, 'Mandiri', '5b3b2750cb336.png', 'www.mandiri.com', 'Bank', 'Bank', NULL, 'fajar', '2018-07-03 00:35:44', '2018-07-03 00:35:44'),
(6, 1, 'sdfggferfge', '5b3c511a3e4a1.png', 'gwgerger', 'ergregre', 'gergegre', NULL, 'fajar', '2018-07-03 21:46:18', '2018-07-03 21:46:18'),
(7, 1, 'sdfs', '5b4474b1606a4.png', 'fwfwef', 'wefwefew', 'wefwefweew', NULL, 'fajar', '2018-07-10 01:56:17', '2018-07-10 01:56:17'),
(8, 1, 'grfreger', '5b48791b9a8bf.png', 'fwefwef', 'wefwefwe', 'wefwefwef', NULL, 'fajar', '2018-07-13 03:04:11', '2018-07-13 03:04:11'),
(9, 2, 'dsfsdfsd', '5b487966c49ca.png', 'fewfwefw', 'ewfwefwe', 'wewefwe', 'mencoba', 'fajar', '2018-07-13 03:05:26', '2018-07-13 03:06:58');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `id` int(10) unsigned NOT NULL,
  `company_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `company_logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pic_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pic_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pic_telephone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `primary_color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secondary_color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `license_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `company_code`, `company_name`, `email`, `telephone`, `address`, `created_by`, `created_at`, `updated_at`, `company_logo`, `pic_name`, `pic_email`, `pic_telephone`, `primary_color`, `secondary_color`, `license_status`) VALUES
(9, 'SOL', 'Solutech', 'admin@solutechglobal.com', '02155550000', 'Jakarta Selatan', 'fajar', '2018-03-14 23:34:08', '2018-03-21 15:24:07', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `division`
--

CREATE TABLE IF NOT EXISTS `division` (
  `id` int(10) unsigned NOT NULL,
  `division_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_company` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `division`
--

INSERT INTO `division` (`id`, `division_name`, `created_by`, `created_at`, `updated_at`, `id_company`) VALUES
(1, 'Web', 'fajar', '2018-03-15 01:42:28', '2018-03-16 11:57:23', NULL),
(2, 'Mobile', 'fajar', '2018-03-15 01:48:40', '2018-03-15 01:48:40', NULL),
(3, 'E-Commerce', 'fajar', '2018-03-15 08:53:52', '2018-03-15 08:53:52', NULL),
(7, 'Analyst', 'fajar', '2018-03-21 15:20:19', '2018-03-21 15:20:19', NULL),
(8, 'Tester', 'fajar', '2018-03-21 15:20:38', '2018-03-21 15:20:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_point`
--

CREATE TABLE IF NOT EXISTS `master_point` (
  `id` int(10) unsigned NOT NULL,
  `division_id` int(11) NOT NULL,
  `jumlah_point` int(11) NOT NULL,
  `point_type_id` int(11) NOT NULL,
  `reward_id` int(11) NOT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `id` int(10) unsigned NOT NULL,
  `member_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_company` int(10) unsigned NOT NULL,
  `id_division` int(10) unsigned NOT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `img_tmp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `member_code`, `member_name`, `email`, `password`, `status`, `avatar`, `id_company`, `id_division`, `created_by`, `created_at`, `updated_at`, `user_id`, `img_tmp`) VALUES
(1, 'SOL00001', 'Fajar', 'fajar@solutechglobal.com', '$2y$10$Gonx3ldmmn1Cap.s4ZA8d.2o0df.bBolD8R6kXXaiWTwooSRuKn.C', 1, '5aab6c0e1b1de.png', 9, 1, 'fajar', '2018-03-15 10:21:16', '2018-03-16 14:02:38', 0, NULL),
(6, 'SOL00002', 'Aan', 'aan@solutechglobal.com', '$2y$10$2rvfK3qRXlJxavYxLmDT1.j70mlcZnObWzuLP7uGxT3xTzpKkx8wy', 1, '5ac1cb2386c9e.png', 9, 1, 'fajar', '2018-04-02 13:18:11', '2018-04-02 13:18:11', 0, NULL),
(7, 'SOL00003', 'Agung', 'agung@solutechglobal.com', '$2y$10$W2../8WBP9K8WDlrMIfHRuBDLJj2kcZyt4l3YOULqKTq9.WxM4n7G', 1, '5ac1cb467dfe5.png', 9, 1, 'fajar', '2018-04-02 13:18:46', '2018-04-02 13:18:46', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `member_card`
--

CREATE TABLE IF NOT EXISTS `member_card` (
  `id` int(10) unsigned NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `card_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `background_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `member_card`
--

INSERT INTO `member_card` (`id`, `member_id`, `card_name`, `background_image`, `created_at`, `updated_at`) VALUES
(2, 1, 'fdgerre', '5aebe5ce6b1cb.png', '2018-05-03 21:32:47', '2018-05-03 21:47:10');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_03_13_075609_create_company_table', 1),
(4, '2018_03_13_080133_create_member_table', 1),
(5, '2018_03_13_081411_create_division_table', 1),
(6, '2018_03_13_081423_create_role_division_table', 1),
(7, '2018_03_14_101811_add_username_field_to_users_table', 1),
(8, '2018_03_26_074548_update_table_users', 2),
(9, '2018_03_26_064712_create_table_point_wallet', 3),
(10, '2018_03_26_074113_create_table_wallet_level', 3),
(11, '2018_03_29_071937_create_point_types_table', 4),
(12, '2018_04_02_042854_create_table_reward', 5),
(13, '2018_04_02_043323_create_table_master_point', 5),
(14, '2018_04_06_075243_add_pic_name_email_dll_to_company_table', 6),
(15, '2018_04_06_080218_add_valid_on_valid_until_to_company_table', 6),
(16, '2018_04_10_074917_add_company_id_to_division', 7),
(17, '2018_04_11_075126_add_user_id_to_member_table', 7),
(18, '2018_04_12_031849_update', 8),
(19, '2018_04_16_031245_update_table_member', 8),
(20, '2018_04_16_031727_update_table_division', 8),
(21, '2018_04_16_040259_create_table_usergroup', 8),
(22, '2018_04_16_040630_create_table_permision_list', 8),
(23, '2018_04_16_041619_create_table_route_list', 8),
(24, '2018_04_16_105258_update_tables_user', 9),
(25, '2018_04_16_144644_update_table_users2', 9),
(26, '2018_04_26_074720_update_table_walletlevel2', 10),
(27, '2018_05_04_021808_create_table_card_member', 11),
(28, '2018_05_04_035546_update_table_card_member', 12),
(29, '2018_05_03_033509_add_company_id_to_users_table', 13),
(30, '2018_05_03_035958_create_point_transaction_table', 13),
(31, '2018_05_04_032158_add_default_value_to_role', 13),
(32, '2018_05_04_034026_add_default_value_to_role2', 13),
(33, '2018_05_04_034156_nullable_to_company_id', 13),
(34, '2018_05_04_034250_nullable_to_company_id2', 13),
(35, '2018_05_04_092321_add_img_tmp_to_member_card_table', 13),
(36, '2018_05_14_072729_add_current_point_kpi_to_point_wallet', 14),
(37, '2018_05_15_034341_update_point_transaction', 14),
(38, '2018_05_17_031441_update_point_transaction_table_2', 14),
(39, '2018_05_18_033332_add_point_type_id_to_point_transaction', 14),
(40, '2016_06_01_000001_create_oauth_auth_codes_table', 15),
(41, '2016_06_01_000002_create_oauth_access_tokens_table', 15),
(42, '2016_06_01_000003_create_oauth_refresh_tokens_table', 15),
(43, '2016_06_01_000004_create_oauth_clients_table', 15),
(44, '2016_06_01_000005_create_oauth_personal_access_clients_table', 15),
(48, '2018_07_02_080233_create_table_cms_team', 16),
(49, '2018_07_02_081708_create_table_cms_news', 16),
(50, '2018_07_02_082321_create_table_cms_services', 17),
(51, '2018_07_02_082025_update_table_point_wallet_nullable', 18),
(52, '2018_07_02_083101_create_table_cms_home', 18),
(53, '2018_07_02_083114_create_table_cms_location', 18),
(54, '2018_07_02_083126_create_table_cms_main_banner', 18),
(55, '2018_07_03_080949_update_table_cms_location', 19),
(56, '2018_07_04_041131_update_table_company_001', 20),
(57, '2018_07_06_035715_create_tags_table', 21),
(58, '2018_07_13_043028_update_table_main_banner_change_url_nullable', 22),
(60, '2018_07_13_050121_update_table_location_change_url_nullable', 23),
(61, '2018_07_13_065217_update_table_cms_location_002', 24),
(62, '2018_07_13_065254_update_table_cms_service_002', 24),
(63, '2018_07_13_065308_update_table_cms_news_002', 24),
(65, '2018_07_13_091457_update_table_team_add_team_desc', 25);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('256597af388214d2796381fbfaf19bd700a105f05e8a6dc373d023e1548695d537d3e38d4aaaf589', 1, 6, NULL, '[]', 0, '2018-07-03 01:58:44', '2018-07-03 01:58:44', '2019-07-03 08:58:44'),
('41b7904b301d5126b9dd4b952d78626ab7d34e9339aada73bba02e448b4bc7b45e4ce7c4d56ba5bf', 1, 4, NULL, '[]', 0, '2018-07-03 01:55:06', '2018-07-03 01:55:06', '2019-07-03 08:55:06'),
('7cffa6d1c67bee9886513f9e17a4f074a75af78568f7f0d0342317711567158d0ab6981163bcff86', 1, 4, NULL, '[]', 0, '2018-07-03 01:54:18', '2018-07-03 01:54:18', '2019-07-03 08:54:18'),
('84922542808b99bd2890fb78e7cb867a50f7ee248a1eaee41de098b2a05a83237c4327b01901a856', 1, 2, NULL, '[]', 0, '2018-07-03 01:56:28', '2018-07-03 01:56:28', '2019-07-03 08:56:28');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'DRTQOOjWFIyqL7SKyMGuQTgTg9KP9zFpVKg8qAGo', 'http://localhost', 1, 0, 0, '2018-07-02 00:55:13', '2018-07-02 00:55:13'),
(2, 1, 'Laravel Password Grant Client', 'IfNUrl1Bc2Gwevozim90KIaWtWruF6iFraDdplLs', 'http://localhost', 0, 1, 0, '2018-07-02 00:55:13', '2018-07-02 00:55:13'),
(3, NULL, 'Laravel Personal Access Client', 'ctdXigOE6ivNw7AKZ5TzD9hhC9ek73iHC6POVzlE', 'http://localhost', 1, 0, 0, '2018-07-03 01:04:36', '2018-07-03 01:04:36'),
(4, 1, 'Laravel Password Grant Client', 'TWqJEZ59JxZmRJxZGaDpwv8d42gWmDhoHZhrMJMa', 'http://localhost', 0, 1, 0, '2018-07-03 01:04:36', '2018-07-03 01:04:36'),
(5, NULL, 'Laravel Personal Access Client', 'ae2b8RRUOq9p1sdFCC8JkRZzCbTGaWxZFKqSNeYj', 'http://localhost', 1, 0, 0, '2018-07-03 01:56:56', '2018-07-03 01:56:56'),
(6, 1, 'Laravel Password Grant Client', 'LUKh2o8oo71LijnOON0mf33BTwfl28DzOFLWVZqy', 'http://localhost', 0, 1, 0, '2018-07-03 01:56:57', '2018-07-03 01:56:57');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-07-02 00:55:13', '2018-07-02 00:55:13'),
(2, 3, '2018-07-03 01:04:36', '2018-07-03 01:04:36'),
(3, 5, '2018-07-03 01:56:57', '2018-07-03 01:56:57');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_refresh_tokens`
--

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('2f6ad2c4fd4cc406086ce9f928f548706be6dbb556df2498603ebf10036ac80a2a29ec0ea9fc7180', '84922542808b99bd2890fb78e7cb867a50f7ee248a1eaee41de098b2a05a83237c4327b01901a856', 0, '2019-07-03 08:56:28'),
('4f4289b289b859396cb58848da439654a835b39e3967d318c8a76dd8e34eae4f2be78a7c55f55e11', '7cffa6d1c67bee9886513f9e17a4f074a75af78568f7f0d0342317711567158d0ab6981163bcff86', 0, '2019-07-03 08:54:18'),
('dd2172adb62a5945f252fdbd187f3196f16462ca6d1204845af663232b1d20ced0d5eec898d330f0', '41b7904b301d5126b9dd4b952d78626ab7d34e9339aada73bba02e448b4bc7b45e4ce7c4d56ba5bf', 0, '2019-07-03 08:55:06'),
('e728815dfbf4b88e8c08041049ebd29af6c930f5a2573d623d37446e8890ff49856db7e4d2262224', '256597af388214d2796381fbfaf19bd700a105f05e8a6dc373d023e1548695d537d3e38d4aaaf589', 0, '2019-07-03 08:58:44');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_list`
--

CREATE TABLE IF NOT EXISTS `permission_list` (
  `id` int(10) unsigned NOT NULL,
  `usergroup_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `route_list_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `point_transaction`
--

CREATE TABLE IF NOT EXISTS `point_transaction` (
  `id` int(10) unsigned NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `transaction_date` datetime NOT NULL,
  `point_in` int(11) DEFAULT '0',
  `point_out` int(11) DEFAULT '0',
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `point_wallet_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `point_type_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `point_type`
--

CREATE TABLE IF NOT EXISTS `point_type` (
  `id` int(10) unsigned NOT NULL,
  `point_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `point_type`
--

INSERT INTO `point_type` (`id`, `point_type`, `description`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'KPI', 'Point from Key Performance Indicator', 'fajar', '2018-04-02 13:14:25', '2018-04-02 13:14:25'),
(3, 'Redeem', 'Point Redeemed by Solutecher', 'fajar', '2018-04-02 13:16:23', '2018-04-02 13:16:23');

-- --------------------------------------------------------

--
-- Table structure for table `point_wallet`
--

CREATE TABLE IF NOT EXISTS `point_wallet` (
  `id` int(10) unsigned NOT NULL,
  `current_point` int(11) DEFAULT NULL,
  `member_id` int(11) NOT NULL,
  `wallet_level_id` int(11) NOT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `current_point_kpi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `point_wallet`
--

INSERT INTO `point_wallet` (`id`, `current_point`, `member_id`, `wallet_level_id`, `created_by`, `created_at`, `updated_at`, `updated_by`, `current_point_kpi`) VALUES
(4, 20, 6, 4, 'fajar', '2018-04-05 11:13:57', '2018-04-05 11:13:57', 'fajar', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reward`
--

CREATE TABLE IF NOT EXISTS `reward` (
  `id` int(10) unsigned NOT NULL,
  `reward_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `point_required` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reward_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reward`
--

INSERT INTO `reward` (`id`, `reward_name`, `point_required`, `quantity`, `description`, `reward_image`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Pop Mie', 5, 10, 'Pop Mie Goreng Pedas', '5ac43772db28d.png', 'fajar', '2018-04-04 09:24:50', '2018-04-04 10:30:57');

-- --------------------------------------------------------

--
-- Table structure for table `role_division`
--

CREATE TABLE IF NOT EXISTS `role_division` (
  `id` int(10) unsigned NOT NULL,
  `role_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_division`
--

INSERT INTO `role_division` (`id`, `role_code`, `role_name`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'DEV', 'Developer', 'fajar', '2018-03-15 00:28:19', '2018-03-16 11:58:17'),
(2, 'ANL', 'Analyst', 'fajar', '2018-03-15 00:35:07', '2018-03-16 09:52:01'),
(3, 'TST', 'Tester', 'fajar', '2018-03-16 09:52:44', '2018-03-16 09:52:44');

-- --------------------------------------------------------

--
-- Table structure for table `route_list`
--

CREATE TABLE IF NOT EXISTS `route_list` (
  `id` int(10) unsigned NOT NULL,
  `route_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `route_list`
--

INSERT INTO `route_list` (`id`, `route_name`, `description`, `status`, `modul`, `created_at`, `updated_at`) VALUES
(1, 'login', 'login', '1', 'login', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(2, 'logout', 'logout', '1', 'logout', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(3, 'register', 'register', '1', 'register', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(4, 'password.request', 'password.request', '1', 'password.request', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(5, 'password.email', 'password.email', '1', 'password.email', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(6, 'password.reset', 'password.reset', '1', 'password.reset', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(7, 'home', 'home', '1', 'home', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(8, 'company.index', 'company.index', '1', 'company.index', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(9, 'company.show', 'company.show', '1', 'company.show', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(10, 'company.create', 'company.create', '1', 'company.create', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(11, 'company.edit', 'company.edit', '1', 'company.edit', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(12, 'member.index', 'member.index', '1', 'member.index', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(13, 'member.show', 'member.show', '1', 'member.show', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(14, 'member.create', 'member.create', '1', 'member.create', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(15, 'member.edit', 'member.edit', '1', 'member.edit', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(16, 'division.index', 'division.index', '1', 'division.index', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(17, 'division.create', 'division.create', '1', 'division.create', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(18, 'division.edit', 'division.edit', '1', 'division.edit', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(19, 'point_wallet.index', 'point_wallet.index', '1', 'point_wallet.index', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(20, 'point_wallet.create', 'point_wallet.create', '1', 'point_wallet.create', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(21, 'point_wallet.destroy', 'point_wallet.destroy', '1', 'point_wallet.destroy', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(22, 'point_wallet.check-member', 'point_wallet.check-member', '1', 'point_wallet.check-member', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(23, 'wallet_level.index', 'wallet_level.index', '1', 'wallet_level.index', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(24, 'wallet_level.create', 'wallet_level.create', '1', 'wallet_level.create', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(25, 'wallet_level.edit', 'wallet_level.edit', '1', 'wallet_level.edit', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(26, 'reward.index', 'reward.index', '1', 'reward.index', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(27, 'reward.create', 'reward.create', '1', 'reward.create', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(28, 'reward.edit', 'reward.edit', '1', 'reward.edit', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(29, 'point_type.index', 'point_type.index', '1', 'point_type.index', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(30, 'point_type.create', 'point_type.create', '1', 'point_type.create', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(31, 'point_type.edit', 'point_type.edit', '1', 'point_type.edit', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(32, 'usergroup.index', 'usergroup.index', '1', 'usergroup.index', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(33, 'usergroup.create', 'usergroup.create', '1', 'usergroup.create', '2018-04-17 11:21:49', '2018-04-17 11:21:49'),
(34, 'usergroup.edit', 'usergroup.edit', '1', 'usergroup.edit', '2018-04-17 11:21:49', '2018-04-17 11:21:49');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(10) unsigned NOT NULL,
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `usergroup`
--

CREATE TABLE IF NOT EXISTS `usergroup` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permission_list_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `route_access_list` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `usergroup`
--

INSERT INTO `usergroup` (`id`, `name`, `status`, `user_id`, `permission_list_id`, `description`, `route_access_list`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', '1', '0', '0', 'superadmin', '{"routelist":["company.index","company.show","company.create","company.edit","division.index","division.create","division.edit","home","login","logout","member.index","member.show","member.create","member.edit","password.request","password.email","password.reset","point_type.index","point_type.create","point_type.edit","point_wallet.index","point_wallet.create","point_wallet.destroy","point_wallet.check-member","register","register","reward.index","reward.create","reward.edit","usergroup.index","usergroup.create","usergroup.edit","wallet_level.index","wallet_level.create","wallet_level.edit"]}', '2018-04-16 16:51:58', '2018-04-17 10:43:51');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inactive` int(11) NOT NULL DEFAULT '1',
  `roles` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'usercompany',
  `id_company` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `username`, `inactive`, `roles`, `id_company`) VALUES
(1, 'fajar', 'fajar@solutechglobal.com', '$2y$10$UjjGeKuKchZQkM25o4IVJOdeYyyUn4Fb..IikLY/qfYcO/KeVb8OG', '2zIURPdcx2TKwYdZY6kggx8IJmqZDOVtU4D274TjIDlegGy32kObohwIidwi', '2018-03-14 04:24:57', '2018-03-14 04:24:57', 'fajar123', 1, 'superadmin', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wallet_level`
--

CREATE TABLE IF NOT EXISTS `wallet_level` (
  `id` int(10) unsigned NOT NULL,
  `level_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `point_type_id` int(11) DEFAULT NULL,
  `min_point` int(11) DEFAULT NULL,
  `max_point` int(11) DEFAULT NULL,
  `level_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wallet_level`
--

INSERT INTO `wallet_level` (`id`, `level_name`, `point_type_id`, `min_point`, `max_point`, `level_image`, `created_by`, `created_at`, `updated_at`) VALUES
(4, 'Bronze', 1, 0, 25, '5ac59f443049b.png', 'fajar', '2018-04-05 11:00:04', '2018-04-05 11:00:04'),
(5, 'Silver', 1, 26, 75, '5ac59f5900a35.png', 'fajar', '2018-04-05 11:00:25', '2018-04-05 11:00:25'),
(6, 'Gold', 1, 11, 1000, '5ac59f70bc6b9.png', 'fajar', '2018-04-05 11:00:48', '2018-05-03 01:28:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cms_home`
--
ALTER TABLE `cms_home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_location`
--
ALTER TABLE `cms_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_main_banner`
--
ALTER TABLE `cms_main_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_news`
--
ALTER TABLE `cms_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_services`
--
ALTER TABLE `cms_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_team`
--
ALTER TABLE `cms_team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `division`
--
ALTER TABLE `division`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_point`
--
ALTER TABLE `master_point`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member_card`
--
ALTER TABLE `member_card`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permission_list`
--
ALTER TABLE `permission_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_list_id_index` (`id`),
  ADD KEY `permission_list_usergroup_id_index` (`usergroup_id`),
  ADD KEY `permission_list_route_list_id_index` (`route_list_id`);

--
-- Indexes for table `point_transaction`
--
ALTER TABLE `point_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `point_type`
--
ALTER TABLE `point_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `point_wallet`
--
ALTER TABLE `point_wallet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reward`
--
ALTER TABLE `reward`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_division`
--
ALTER TABLE `role_division`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `route_list`
--
ALTER TABLE `route_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `route_list_id_index` (`id`),
  ADD KEY `route_list_route_name_index` (`route_name`),
  ADD KEY `route_list_description_index` (`description`),
  ADD KEY `route_list_status_index` (`status`),
  ADD KEY `route_list_modul_index` (`modul`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usergroup`
--
ALTER TABLE `usergroup`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usergroup_id_index` (`id`),
  ADD KEY `usergroup_name_index` (`name`),
  ADD KEY `usergroup_status_index` (`status`),
  ADD KEY `usergroup_user_id_index` (`user_id`),
  ADD KEY `usergroup_permission_list_id_index` (`permission_list_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- Indexes for table `wallet_level`
--
ALTER TABLE `wallet_level`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cms_home`
--
ALTER TABLE `cms_home`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `cms_location`
--
ALTER TABLE `cms_location`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `cms_main_banner`
--
ALTER TABLE `cms_main_banner`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `cms_news`
--
ALTER TABLE `cms_news`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `cms_services`
--
ALTER TABLE `cms_services`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `cms_team`
--
ALTER TABLE `cms_team`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `division`
--
ALTER TABLE `division`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `master_point`
--
ALTER TABLE `master_point`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `member_card`
--
ALTER TABLE `member_card`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `permission_list`
--
ALTER TABLE `permission_list`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `point_transaction`
--
ALTER TABLE `point_transaction`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `point_type`
--
ALTER TABLE `point_type`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `point_wallet`
--
ALTER TABLE `point_wallet`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `reward`
--
ALTER TABLE `reward`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `role_division`
--
ALTER TABLE `role_division`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `route_list`
--
ALTER TABLE `route_list`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `usergroup`
--
ALTER TABLE `usergroup`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wallet_level`
--
ALTER TABLE `wallet_level`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
