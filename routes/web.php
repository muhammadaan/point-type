<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::post('register', 'Auth\RegisterController@register')->name('register');
Route::get('/', 'HomeController@index')->name('home')->middleware('auth');

Route::prefix('company')->group(function() {
   Route::get('list', 'company\controller\CompanyController@index')->name('company.index');
   Route::get('detail', 'company\controller\CompanyController@show')->name('company.show'); 
   Route::get('add', 'company\controller\CompanyController@create')->name('company.create');
   Route::get('edit/{id}', 'company\controller\CompanyController@edit')->name('company.edit');
   Route::post('insert', 'company\controller\CompanyController@store');
   Route::post('update/{id}', 'company\controller\CompanyController@update');
   Route::get('delete/{id}', 'company\controller\CompanyController@destroy');
   
});

Route::prefix('member')->group(function() {
	Route::get('list', 'member\controller\MemberController@index')->name('member.index');
	Route::get('detail', 'member\controller\MemberController@show')->name('member.show');
	Route::get('add', 'member\controller\MemberController@create')->name('member.create');
	Route::get('edit/{id}', 'member\controller\MemberController@edit')->name('member.edit');
	Route::post('insert', 'member\controller\MemberController@store');
	Route::post('update/{id}', 'member\controller\MemberController@update');
	Route::get('delete/{id}', 'member\controller\MemberController@destroy');
});

Route::prefix('division')->group(function() {
	Route::get('list', 'division\controller\DivisionController@index')->name('division.index');
	Route::get('add', 'division\controller\DivisionController@create')->name('division.create');	
	Route::get('edit/{id}', 'division\controller\DivisionController@edit')->name('division.edit');
	Route::post('insert', 'division\controller\DivisionController@store');
   	Route::post('update/{id}', 'division\controller\DivisionController@update');
    Route::get('delete/{id}', 'division\controller\DivisionController@destroy');
});

Route::prefix('role-division')->group(function() {
	Route::get('list', 'role_division\controller\RoleDivisionController@index');
	Route::get('add', 'role_division\controller\RoleDivisionController@create');
	Route::get('edit/{id}', 'role_division\controller\RoleDivisionController@edit');
	Route::post('insert', 'role_division\controller\RoleDivisionController@store');
   	Route::post('update/{id}', 'role_division\controller\RoleDivisionController@update');
    Route::get('delete/{id}', 'role_division\controller\RoleDivisionController@destroy');
});

Route::prefix('point_wallet')->group(function() {
	Route::get('list', 'point_wallet\controller\PointWalletController@index')->name('point_wallet.index');
	Route::get('add', 'point_wallet\controller\PointWalletController@create')->name('point_wallet.create');
	// Route::get('edit/{id}', 'role_division\controller\RoleDivisionController@edit')->name('role_division.edit');
	Route::post('insert', 'point_wallet\controller\PointWalletController@store');
 //   	Route::post('update/{id}', 'role_division\controller\RoleDivisionController@update')->name('role_division.update');
    Route::get('delete/{id}', 'point_wallet\controller\PointWalletController@destroy')->name('point_wallet.destroy');

    Route::get('check-member', 'point_wallet\controller\PointWalletController@checkMember')->name('point_wallet.check-member');
    Route::get('transaction', 'point_wallet\controller\PointWalletController@showTransaction')->name('point_wallet.transaction');

});

Route::prefix('wallet_level')->group(function() {
	Route::get('list', 'wallet_level\controller\WalletLevelController@index')->name('wallet_level.index');
	Route::get('add', 'wallet_level\controller\WalletLevelController@create')->name('wallet_level.create');
	Route::get('edit/{id}', 'wallet_level\controller\WalletLevelController@edit')->name('wallet_level.edit');
	Route::post('insert', 'wallet_level\controller\WalletLevelController@store');
   	Route::post('update/{id}', 'wallet_level\controller\WalletLevelController@update');
    Route::get('delete/{id}', 'wallet_level\controller\WalletLevelController@destroy');
});

Route::prefix('reward')->group(function() {
	Route::get('list', 'reward\controller\RewardController@index')->name('reward.index');
	Route::get('add', 'reward\controller\RewardController@create')->name('reward.create');
	Route::get('edit/{id}', 'reward\controller\RewardController@edit')->name('reward.edit');
	Route::post('insert', 'reward\controller\RewardController@store');
   	Route::post('update/{id}', 'reward\controller\RewardController@update');
    Route::get('delete/{id}', 'reward\controller\RewardController@destroy');
});

Route::prefix('point_type')->group(function() {
	Route::get('list', 'point_type\controller\PointTypeController@index')->name('point_type.index');
	Route::get('add', 'point_type\controller\PointTypeController@create')->name('point_type.create');
	Route::get('edit/{id}', 'point_type\controller\PointTypeController@edit')->name('point_type.edit');
	Route::post('insert', 'point_type\controller\PointTypeController@store');
	Route::post('update/{id}', 'point_type\controller\PointTypeController@update');
	Route::get('delete/{id}', 'point_type\controller\PointTypeController@destroy');
});

Route::resource('point-transaction', 'point_transaction\controller\PointTransactionController');


Route::prefix('usergroup')->group(function() {
	Route::get('list', 'Acl\Controller\AclController@index')->name("usergroup.index");
	Route::get('add', 'Acl\Controller\AclController@create')->name("usergroup.create");
	Route::post('insert', 'Acl\Controller\AclController@store');
	Route::get('edit/{id}', 'Acl\Controller\AclController@edit')->name("usergroup.edit");
	Route::put('update/{id}', 'Acl\Controller\AclController@update');
	Route::get('delete/{id}', 'Acl\Controller\AclController@destroy');
});

Route::prefix('membercard')->group(function() {
	Route::get('list', 'member_card\controller\MemberCardController@index')->name("membercard.index");
	Route::get('add', 'member_card\controller\MemberCardController@create')->name("membercard.create");
	Route::post('insert', 'member_card\controller\MemberCardController@store');
	Route::get('edit/{id}', 'member_card\controller\MemberCardController@edit')->name("membercard.edit");
	Route::post('update/{id}', 'member_card\controller\MemberCardController@update');
	Route::get('delete/{id}', 'member_card\controller\MemberCardController@destroy');
	Route::post('generate', 'member_card\controller\MemberCardController@generate')->name('membercard.generate');
});

Route::prefix('cms_home')->group(function() {
	Route::get('list', 'cms\cms_home\controller\CMSHomeController@index')->name('cms_home.index');
	Route::get('detail', 'cms\cms_home\controller\CMSHomeController@show')->name('cms_home.show');
	Route::get('add', 'cms\cms_home\controller\CMSHomeController@create')->name('cms_home.create');
	Route::get('edit/{id}', 'cms\cms_home\controller\CMSHomeController@edit')->name('cms_home.edit');
	Route::post('insert', 'cms\cms_home\controller\CMSHomeController@store');
   	Route::post('update/{id}', 'cms\cms_home\controller\CMSHomeController@update');
    Route::get('delete/{id}', 'cms\cms_home\controller\CMSHomeController@destroy');
});

Route::prefix('cms_banner')->group(function() {
	Route::get('list', 'cms\cms_banner\controller\CMSBannerController@index')->name('cms_banner.index');
	Route::get('add', 'cms\cms_banner\controller\CMSBannerController@create')->name('cms_banner.create');
	Route::get('edit/{id}', 'cms\cms_banner\controller\CMSBannerController@edit')->name('cms_banner.edit');
	Route::post('insert', 'cms\cms_banner\controller\CMSBannerController@store');
   	Route::post('update/{id}', 'cms\cms_banner\controller\CMSBannerController@update');
    Route::get('delete/{id}', 'cms\cms_banner\controller\CMSBannerController@destroy');
});

Route::prefix('cms_team')->group(function() {
	Route::get('list', 'cms\cms_team\controller\CMSTeamController@index')->name('cms_team.index');
	Route::get('add', 'cms\cms_team\controller\CMSTeamController@create')->name('cms_team.create');
	Route::get('edit/{id}', 'cms\cms_team\controller\CMSTeamController@edit')->name('cms_team.edit');
	Route::post('insert', 'cms\cms_team\controller\CMSTeamController@store');
   	Route::post('update/{id}', 'cms\cms_team\controller\CMSTeamController@update');
    Route::get('delete/{id}', 'cms\cms_team\controller\CMSTeamController@destroy');
});

Route::prefix('cms_news')->group(function() {
	Route::get('list', 'cms\cms_news\controller\CMSNewsController@index')->name('cms_news.index');
	Route::get('add', 'cms\cms_news\controller\CMSNewsController@create')->name('cms_news.create');
	Route::get('edit/{id}', 'cms\cms_news\controller\CMSNewsController@edit')->name('cms_news.edit');
	Route::post('insert', 'cms\cms_news\controller\CMSNewsController@store');
   	Route::post('update/{id}', 'cms\cms_news\controller\CMSNewsController@update');
    Route::get('delete/{id}', 'cms\cms_news\controller\CMSNewsController@destroy');
});

Route::prefix('cms_services')->group(function() {
	Route::get('list', 'cms\cms_services\controller\CMSServicesController@index')->name('cms_services.index');
	Route::get('add', 'cms\cms_services\controller\CMSServicesController@create')->name('cms_services.create');
	Route::get('edit/{id}', 'cms\cms_services\controller\CMSServicesController@edit')->name('cms_services.edit');
	Route::post('insert', 'cms\cms_services\controller\CMSServicesController@store');
   	Route::post('update/{id}', 'cms\cms_services\controller\CMSServicesController@update');
    Route::get('delete/{id}', 'cms\cms_services\controller\CMSServicesController@destroy');
});

Route::prefix('cms_location')->group(function() {
	Route::get('list', 'cms\cms_location\controller\CMSLocationController@index')->name('cms_location.index');
	Route::get('add', 'cms\cms_location\controller\CMSLocationController@create')->name('cms_location.create');
	Route::get('edit/{id}', 'cms\cms_location\controller\CMSLocationController@edit')->name('cms_services.edit');
	Route::post('insert', 'cms\cms_location\controller\CMSLocationController@store');
   	Route::post('update/{id}', 'cms\cms_location\controller\CMSLocationController@update');
    Route::get('delete/{id}', 'cms\cms_location\controller\CMSLocationController@destroy');
});


//=====================get route================================//
use App\Models\RouteList;

Route::get('roles',function(){
	$data = Route::getRoutes();
	foreach ($data as $index => $value) {
		# code...
		$exist = RouteList::where('route_name','=',$value->getName())->count();
		if ($exist == 0){
			if ($value->getName() != ""){
				$save = new RouteList;
				$save->route_name = $value->getName();
				$save->description = $value->getName();
				$save->modul = $value->getName();
				$save->status = 1;
				$save->save();
				echo "success add data ".$index." '<br>'";
			}
				
		}
			// echo "failed";
		
	}
});