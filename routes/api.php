<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::group(['middleware' => 'auth:api'], function(){
	
	Route::prefix('member')->group(function() {
		Route::get('list', 'member\service\MemberService@index');
	});

	Route::prefix('company')->group(function() {
		Route::get('list', 'company\service\CompanyService@index');
	});

	Route::prefix('reward')->group(function() {
		Route::get('list', 'reward\service\RewardService@index');
	});

	Route::prefix('cms_home')->group(function() {
		Route::post('list', 'cms\cms_home\service\CMSHomeService@index');
	});
	
	Route::prefix('cms_banner')->group(function() {
		Route::get('list', 'cms\cms_banner\service\CMSBannerService@index');
	});

	Route::prefix('cms_news')->group(function() {
		Route::post('list', 'cms\cms_news\service\CMSNewsService@index');
	});

	Route::prefix('cms_team')->group(function() {
		Route::post('list', 'cms\cms_team\service\CMSTeamService@index');
	});

	Route::prefix('cms_services')->group(function() {
		Route::post('list', 'cms\cms_services\service\CMSServicesService@index');
	});


});
